Vue.component('search-box', {
		template: '#searchbox-tpl',
		data: function() {
			return {
				noRes: 0,
				brands: [],
				models: [],
				filter: {
					brand: null,
					model: null,
					price: null,
					km: null,
					anno: null
				}
			}
		},
		ready: function() {
			this.init_load();
		},
		watch: {
			'filter': {
				handler: function() {
					this.tmp_search();
				},
				deep: 1
			},
			'filter.brand': function() {

				this.filter.price = null;
				this.filter.km = null;
				this.filter.anno = null;
				
				this.$http.post('/api/federico.models', { brand: this.filter.brand }).then(function(a) {
					this.models = a.data;
				});

			}
		},
		methods: {
			init_load: function() {
				// This is necessary only to load all the brands..
				this.$http.get('/api/federico.brands').then(function(a) {
					this.brands = a.data;
				})
			},
			tmp_search: function() {
				this.$http.post('/api/federico.search', { filters: this.filter }).then(function(data) {
					this.noRes = data.data.res;
					console.log(data);
				})
			}
		}
	});

	new Vue({
		el: '#app'
	})