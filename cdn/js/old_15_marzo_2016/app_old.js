$(document).ready(function() {
  
    $('.fancybox').fancybox({
        'transitionIn'  :   'none',
        'transitionOut' :   'none',
        'speedIn'       :   0, 
        'speedOut'      :   0,
        'nextEffect'    : 'none',
        'prevEffect'    : 'none'
    });
     
     if ($('.email-message').length) {
        setTimeout(function(){ 
            $('.email-message').fadeOut(); 
        }, 5000);
     }
      
     if ($('#equipaggiamento').length) {
        var a=$('#equipaggiamento').html();
        var res = a.replace(/;/g, ";<br>");
        $('#equipaggiamento').html(res);
     }

    if ($('#vat-map').length) {
        var address = centerMap = {
            lat: 41.621529,
            lng: 13.3108486
        };
        var map = new google.maps.Map(document.getElementById('vat-map'), {
            zoom: 18,
            center: centerMap,
            disableDefaultUI: true,
            disableDefaultUI: true,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false,
            scrollwheel: false
           });
        var marker = new google.maps.Marker({
            position: address,
            map: map
        });
        google.maps.event.addDomListener(window, 'resize', function() {
            map.setCenter(centerMap);
        });
    }

    $('#richiedi-informazioni-btn').bind('click',function(){
        scrollToElement('contatta-venditore');
    });

    scrollToElement = function(elem) {
        var top = $("#" + elem).offset().top;
        $('html, body').animate({
            scrollTop: top
        }, 500, function() {
        });
    }

    $('#vieniatrovarci-btn').bind('click', function() {
        $('#listapreferitiarea').addClass("closed");
        $('#concessionariarea').addClass("closed");
        var aa = $('#vieniatrovarciarea').hasClass("closed");
        if (aa)
            $('#vieniatrovarciarea').removeClass("closed");
        else
            $('#vieniatrovarciarea').addClass("closed");
      });



    $('#listapreferiti-btn').bind('click', function() {
        $('#vieniatrovarciarea').addClass("closed");
        $('#concessionariarea').addClass("closed");
        var aa = $('#listapreferitiarea').hasClass("closed");
        if (aa)
            $('#listapreferitiarea').removeClass("closed");
        else
            $('#listapreferitiarea').addClass("closed");
    });


    $('#concessionari-btn').bind('click', function() {
        $('#listapreferitiarea').addClass("closed");
        $('#vieniatrovarciarea').addClass("closed");
        var aa = $('#concessionariarea').hasClass("closed");
        if (aa)
            $('#concessionariarea').removeClass("closed");
        else
            $('#concessionariarea').addClass("closed");
    });


    $('#vieniatrovarci-btn-menu').bind('click', function() {
        scrollToElement('vieniatrovarci-btn');
        var aa = $('#vieniatrovarciarea').hasClass("closed");
        if (aa)
            $('#vieniatrovarciarea').removeClass("closed");
        else
            $('#vieniatrovarciarea').addClass("closed");
    });


     $(document).on('click','.preferiti-btn',function(e){
        e.preventDefault();
        $.post('/api/wishlists.add', {"prod":$(this).data('car')}, function(a){
            location.reload();
        });
     });

     $(window).bind('scroll', function() {
        
        var offset = 50 + $("#endheader").offset().top;
        var docPos = $(window).scrollTop();
        var wMiddleH = $(window).height();
        if(docPos){
            var diff=offset-docPos;
            if (diff<200) {
                $('.area-selector').addClass('position-fixed');
            } else{
                $('.area-selector').removeClass('position-fixed');
            }
        }
        if (docPos > offset) {
            console.log('2');
            $('.vieniatrovarci.useraction').css("position", "fixed");
            $('.vieniatrovarci.useraction').css("top", "40%");
            $('.vieniatrovarci.useraction').css("marginTop", "-200px");
            $('.listapreferiti.useraction').css("position", "fixed");
            $('.listapreferiti.useraction').css("top", "40%");
            $('.concessionari.useraction').css("position", "fixed");
            $('.concessionari.useraction').css("top", "40%");
            $('.concessionari.useraction').css("marginTop", "188px");
        } else {
            console.log('3');
            $('.vieniatrovarci.useraction').css("position", "absolute");
            $('.vieniatrovarci.useraction').css("top", offset + "px");
            $('.vieniatrovarci.useraction').css("marginTop", "0px");
            $('.listapreferiti.useraction').css("position", "absolute");
            $('.listapreferiti.useraction').css("top", (offset + 199) + "px");
            $('.concessionari.useraction').css("position", "absolute");
            $('.concessionari.useraction').css("top", (offset + 195) + "px");
        }
        if (docPos < 5) {
            console.log('4');
            $('.vieniatrovarci.useraction').css("position", "absolute");
            $('.vieniatrovarci.useraction').css("top", offset + "px");
            $('.vieniatrovarci.useraction').css("marginTop", "0px");
            $('.listapreferiti.useraction').css("position", "absolute");
            $('.listapreferiti.useraction').css("top", (offset + 199) + "px");
            $('.concessionari.useraction').css("position", "absolute");
            $('.concessionari.useraction').css("top", (offset + 195) + "px");
        }

    });

    var docPos = $(window).scrollTop();
    var offset = 50 + $("#endheader").offset().top;
    var diff=offset-docPos;

    if (diff<200) {
        $('.area-selector').addClass('position-fixed');
    } else{
        $('.area-selector').removeClass('position-fixed');
    }

    $('.vieniatrovarci.useraction').css("position", "absolute");
    $('.vieniatrovarci.useraction').css("top", offset + "px");
    $('.listapreferiti.useraction').css("position", "absolute");
    $('.listapreferiti.useraction').css("top", (offset + 199) + "px");
    $('.concessionari.useraction').css("position", "fixed");
    $('.concessionari.useraction').css("top", "50%");
    $('.concessionari.useraction').css("marginTop", "190px");

    $(document).scrollTop(10);

    $("#openFormMobile").click(function(){
        var st = $('.dimension-form').hasClass('close');
        if(st){
            $('.dimension-form').removeClass('close');
            $(this).removeClass('close');
        }
        else{
            $('.dimension-form').addClass('close');
            $(this).addClass('close');
        }
    });
 

});