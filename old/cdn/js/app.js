$(document).ready(function() {

    /* MAPPE */

    if ($('#vat-map').length) {

        var address = centerMap = {
            lat: 41.621333,
            lng: 13.311194
        };

        var map = new google.maps.Map(document.getElementById('vat-map'), {
            zoom: 15,
            center: centerMap,
            disableDefaultUI: true,
            disableDefaultUI: true,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false,
            scrollwheel: false
        });

        var marker = new google.maps.Marker({
            position: address,
            map: map
        });

        google.maps.event.addDomListener(window, 'resize', function() {
            map.setCenter(centerMap);
        });

    }

    


    /* SCROLL DOCUMENT */

    scrollToElement = function(elem) {
        console.log(elem);
        var top = $("#" + elem).offset().top;
        $('html, body').animate({
            scrollTop: top
        }, 500, function() {

        });
    }


    /* BOTTONI */

    $('#vieniatrovarci-btn').bind('click', function() {
        var aa = $('#vieniatrovarciarea').hasClass("closed");
        if (aa)
            $('#vieniatrovarciarea').removeClass("closed");
        else
            $('#vieniatrovarciarea').addClass("closed");
    });

    $('#listapreferiti-btn').bind('click', function() {
        var aa = $('#listapreferitiarea').hasClass("closed");
        if (aa)
            $('#listapreferitiarea').removeClass("closed");
        else
            $('#listapreferitiarea').addClass("closed");
    });




    $('#vieniatrovarci-btn-menu').bind('click', function() {
          scrollToElement('vieniatrovarci-btn');
        var aa = $('#vieniatrovarciarea').hasClass("closed");
        if (aa)
            $('#vieniatrovarciarea').removeClass("closed");
        else
            $('#vieniatrovarciarea').addClass("closed");
    });




    /* */

    $(window).bind('scroll', function() {
        var offset = 50 + $("#endheader").offset().top;
        var docPos = $(window).scrollTop();
        var wMiddleH = $(window).height();
        if (docPos > offset) {
            $('.vieniatrovarci.useraction').css("position", "fixed");
            $('.vieniatrovarci.useraction').css("top", "50%");
            $('.vieniatrovarci.useraction').css("marginTop", "-200px");
            $('.listapreferiti.useraction').css("position", "fixed");
            $('.listapreferiti.useraction').css("top", "50%");
        } else {
            $('.vieniatrovarci.useraction').css("position", "absolute");
            $('.vieniatrovarci.useraction').css("top", offset + "px");
              $('.vieniatrovarci.useraction').css("marginTop", "0px");
            $('.listapreferiti.useraction ').css("position", "absolute");
            $('.listapreferiti.useraction ').css("top", (offset + 199) + "px");
        }

        if (docPos < 5) {
            $('.vieniatrovarci.useraction').css("position", "absolute");
            $('.vieniatrovarci.useraction').css("top", offset + "px");
            $('.vieniatrovarci.useraction').css("marginTop", "0px");
            $('.listapreferiti.useraction ').css("position", "absolute");
            $('.listapreferiti.useraction ').css("top", (offset + 199) + "px");
        }

    });


    
    var offset = 50 + $("#endheader").offset().top;
    $('.vieniatrovarci.useraction').css("position", "absolute");
    $('.vieniatrovarci.useraction').css("top", offset + "px");
    $('.listapreferiti.useraction ').css("position", "absolute");
    $('.listapreferiti.useraction ').css("top", (offset + 199) + "px"); 



});