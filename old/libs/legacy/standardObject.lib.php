<?php

/*

-----------
Gyural 1.10
-----------

Filename: /libs/standardObject.lib.php
 Version: 1.9.1
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 04/11/2014

----
Desc
----

MySQL base Object.

*/

class standardObject {

	var $gyu_id = 'id';

	function __construct() {}

	// Filtering the database.
	public function filter() {

		$args = func_get_args();
		return $this->filterExecute($args);

	}

	public function filter_array($args) {

		return $this->filterExecute($args);

	}

	public function filterExecute($args) {

		$whereC = array();
		$endQ = array();
		$isArray = 1;
		$limitIndication = ' LIMIT 0,100';
		$constructArgs = null;
		$className = get_class($this);

		foreach($args as $a) {
			$pT = true;

			if(is_array($a)) {
				if(count($a) == 2) {
					list($key, $value) = $a;
					if($key == 'LIMIT') {
						$pT = false;
						$limitIndication = 'LIMIT ' . $a[1];
					} else if($key == '#collection') {
						$pT = false;
						$collection = $value;
					} else {
						$operator = '=';
					}
				} else {
					if($a[0] == 'LIMIT') {
						$pT = false;
						$limitIndication = 'LIMIT ' . $a[1] . ',' . $a[2];
					} else {
						$pT = true;
						list($key, $operator, $value) = $a;
					}
				}

				if($pT) {
					if($operator == 'LIKE')
						$whereC[] = "`".$key."` " . $operator . " '%" . $value . "%'";
					else
						$whereC[] = "`".$key."` " . $operator . " '" . $value . "'";
				}

			} else if(is_object($a)) {
				$constructArgs[] = $a;
			} else {
				if($a == 'ONE') {
					if($limitIndication != 'LIMIT 1') {
						$limitIndication = ' LIMIT 1';
					}
					$isArray = 0;
				}
				else if($a == 'COUNT') {
					$isArray = 3;
				}
				else if($a == '#collection') {
					$collection = 'collectionsObject';
				}
				else
					$endQ[] = $a;
			}
		}

		$label = $whereC ? ' WHERE ' : '';

		if($isArray == 3){
			$query = "SELECT COUNT(*) AS gyu_tot FROM `".$this->gyu_table."` " . $label . implode(' AND ', $whereC) . ' ' . implode(" ", $endQ) . ' ';
			$size = FetchObject(Database()->query($query))->gyu_tot;
			return $size;
		} else {
			$query = "SELECT * FROM `".$this->gyu_table."` " . $label . implode(' AND ', $whereC) . ' ' . implode(" ", $endQ) . ' ' . $limitIndication;
			$res = FetchObject(Database()->query($query), $isArray, $className, $constructArgs, $query);
			if($collection) {
				return LoadClass($collection, 1)->popolate($res);
			} else
				return $res;
		}

	}


	// Get!
	public function get($index, $constructArgs) {

		return $this->filter(array($this->gyu_id, $index), array('LIMIT', 1), 'ONE', $constructArgs);

	}

	//	Render the $name from the object { try to call rendername(); }
	public function render($name, $args) {

		if(method_exists($this, 'render' . ucFirst($name)))
			return call_user_func(array($this, 'render' . ucFirst($name)), $args);
		else
			return stripslashes($this->$name);

	}

	// Not null
	public function notNull($what) {

		if(strlen($this->getAttr($what)) > 0 && $this->getAttr($what) != 0)
			return true;
		else
			return false;

	}

	###########################
	## Manipulate the Object ##
	###########################

	//	Get the $name attr from the object. { try to call getname(); }
	public function getAttr($name, $args = null) {

		if(method_exists($this, 'get' . ucFirst($name)))
			return call_user_func(array($this, 'get' . ucFirst($name)), $args);
		else
			return stripslashes($this->$name);

	}

	public function verb($name) {

		if(method_exists($this, 'verb' . ucFirst($name)))
			return call_user_func(array($this, 'verb' . ucFirst($name)));
		else
			return stripslashes($this->$name);

	}

	//	Set the $key attribute with the $vValue value
	public function setAttr($key, $vValue) {

		if(method_exists($this, 'set' . ucFirst($key)))
			return call_user_func(array($this, 'set' . ucFirst($key)), $vValue);
		else
			return $this->$key = $vValue;

	}

	//	Refill the object with the $vArray values.
	public function refill($vArray = null) {

		if(!$vArray)
			return false;

		foreach($vArray as $key=>$vValue)
			$this->setAttr($key, $vValue);

		return $this;

	}

	##############################
	## Insert an object into db ##
	##############################

	// $object->hang() ==> prepare the query to insert the object in the database.
	public function hang($vArray = null) {

		if($vArray)
			$this->refill($vArray);

		return CreateQuery('I', $this->gyu_table, $this);

	}

	// $object->hangExecute() ==> execute the query, prepared by ->hang();
	public function hangExecute($vArray = null) {

		if($query = $this->hang($vArray)) {
			Database()->query($query);
			$this->{$this->gyu_id} = Database()->insert_id;
			return $this->{$this->gyu_id};
		}

	}

	##############################
	## Update an Object into db ##
	##############################

	// $object->put() ==> prepare the query to update the object in the database.
	public function put($vArray = null, $id = null) {

		if(isset($vArray) && !is_array($vArray))
			$id = $vArray;
		else if(isset($vArray))
			$this->refill($vArray);

		if($id == null)
			$id = $this->gyu_id . ' = ' . $this->getAttr($this->gyu_id);

		return CreateQuery('U', $this->gyu_table, $this, $id);

	}

	// $object->putExecute() ==> execute the query, prepared by ->put();
	public function putExecute($vArray = null, $id = null) {

		if($query = $this->put($vArray, $id))
			return Database()->query($query);

	}

	##############################
	## Delete an object from db ##
	##############################

	// $object->delete() ==> prepare the query to delete the object in the database.
	public function delete($id = null) {

		if(!$id)
			$id = $this->gyu_id . ' = ' . $this->getAttr($this->gyu_id);

		return "DELETE FROM `" .$this->gyu_table . "` WHERE " . $id;

	}

	// $object->deleteExecute() ==> execute the query, prepared by ->delete();
	public function deleteExecute($id = null) {

		if($query = $this->delete($id))
			return Database()->query($query);

	}

	############
	## Magic! ##
	############
	# 1.1 #

	public function gyu_tablesPrefix() {

		if(isset($this->gyu_table)) {
			if($this->gyu_table[0] != '*')
				$this->gyu_table = tablesPrefix . $this->gyu_table;
			else
				$this->gyu_table = str_replace('*', '', $this->gyu_table);
		}

	}

	public function __call($name, $arguments) {

		echo "'$name' method does not exists.\n";

	}

	## Next to be removed into 1.8 ##
	public function totRecords() {

		return FetchObject(Database()->query("SELECT COUNT(*) AS a FROM `".$this->gyu_table."`"), 0)->a;

	}

}

?>