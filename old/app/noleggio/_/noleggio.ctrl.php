<?php

class noleggioCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> 'NOLEGGIO',
			'subTitle'		=> '',
			'voceMenu' 		=> '',
			'header-bkg'	=> 'header-bkg-1.jpg',
			'breadcrumbs'	=> Array(
				['name'=>'Noleggio','current'=>'current'],
			)
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/noleggio');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

	}

	
}

