<?php

class frontendCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->categorie);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> 'LA NOSTRA PASSIONE, LE VOSTRE AUTOMOBILI',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> 'home',
			'header-bkg'	=> 'header-bkg-1.jpg'
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/home');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

		//$this->frontend->vieniatrovarci();
		//$this->frontend->listapreferiti();

	}

	

}

