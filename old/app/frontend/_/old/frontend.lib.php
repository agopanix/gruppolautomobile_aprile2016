<?php
class frontend {

	var $config = [
		'title' => siteName,
		'description' => '',
		'logo' => '//placehold.it/150x150',
		'showHero' => true
	];

	var $catsel=0;



	var $marche = array(
		['id'=>'1','marca'=>'marca 1'],
		['id'=>'2','marca'=>'marca 2']
	);

	var $categorie = array(
		['id'=>'1','name'=>'Neopatentati','img'=>'/upl/neopatentati.png', 'text' =>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'2','name'=>'IvaEsposta','img'=>'/upl/ivaesposta.png','text'=>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'3','name'=>'Aziendali Km0','img'=>'/upl/aziendalikmzero.png','text'=>'Vendita auto km0 dai concessionari di Gruppo L&rsquo;Automobile per assicurarti l&rsquo;auto che hai sempre desiderato al prezzo inaspettato'],
		['id'=>'4','name'=>'Famiglia','img'=>'/upl/famiglie.png','text'=>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'5','name'=>'City Car','img'=>'/upl/citycar.png','text'=>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'6','name'=>'Suv & Fuori Strada','img'=>'/upl/furiostrada.png','text'=>'L&rsquo;incanto sar&agrave; godersi la strada']
	);

	var $autoinofferta = array(
		['id'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
	);

	var $autopreferite = array(
		['id'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'3','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'4','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'5','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'6','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
	);

	var $serviziaggiuntivi = array(
		['id'=>'1','titolo'=>'Non perdere l&lsquo;occasione','descrizione'=>'Salva nella tua “Whis List” tutti i modelli che preferisci','img'=>'/upl/servizio-1.png'],
		['id'=>'2','titolo'=>'Vieni a trovarci','descrizione'=>'Un nostro collaboratore sarà al tuo servizio per l’intera giornata.','img'=>'/upl/servizio-2.png'],
	);

	var $accessori = array(
		['id'=>'1','titolo'=>'ABS'],
		['id'=>'2','titolo'=>'Airbag passeggero'],
		['id'=>'3','titolo'=>'Cambio Automatico'],
		['id'=>'4','titolo'=>'Climatizzatore'],
		['id'=>'5','titolo'=>'Interni in materiale pregiato'],
		['id'=>'6','titolo'=>'Servosterzo'],
		['id'=>'7','titolo'=>'Airbag lato guida'],
		['id'=>'8','titolo'=>'Airbag laterali'],
		['id'=>'9','titolo'=>'Cerchi in lega'],
		['id'=>'10','titolo'=>'Controllo elettr. trazione'],
		['id'=>'11','titolo'=>'Navigatore satellitare'],
		['id'=>'12','titolo'=>'Sospensioni autoregolabili'],
	);

	var $risultato_ricerca = array(
		['id'=>'1','marca'=>'Audi','modello'=>'A4', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4],'immatricolazione'=>'24/05/2007','descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','marca'=>'BMW','modello'=>'316d', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9],'immatricolazione'=>'24/05/2007', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'3','marca'=>'Audi','modello'=>'A4', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4],'immatricolazione'=>'24/05/2007','descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'4','marca'=>'BMW','modello'=>'316d', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9],'immatricolazione'=>'24/05/2007', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'5','marca'=>'Audi','modello'=>'A4', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4],'immatricolazione'=>'24/05/2007','descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'6','marca'=>'BMW','modello'=>'316d', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9],'immatricolazione'=>'24/05/2007', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
	);

	var $dettaglio_veicolo = array(
		['id'=>'1','codice'=>'801117','marca'=>'BMW','modello'=>'318d 2.0 143CV cat Touring Attiva', 'normativa'=>'Euro5 (715/2007-692/2008)','colore'=>'Blue','alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9,10,11,12],'luogo'=>'Via Enrico Fermi, 27 - 03100 Frosinone','immatricolazione'=>'24/05/2007','km'=>'106.205','descrizione'=>'', 'motore' =>'2.0 TDI 143CV F.AP. Advanced' ,'cilindrata'=>'1995','kw'=>'135','cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
	);

	var $auto_interesse = array(
		['id'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','marca'=>'BMW','modello'=>'316d','importo'=>'22900' , 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'2','marca'=>'BMW','modello'=>'316d', 'importo'=>'17800' ,'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		);


	
	function _config($config) {
		$config = array_merge($this->config, $config);
		$this->config = $config;
	}

		
	function header() {
		Application('frontend/_v/commons/header', null, $this);
	}
	
	function footer() {
		Application('frontend/_v/commons/footer', null, $this);
	}
	
	function menu() {
		Application('frontend/_v/commons/menu', null, $this);
	}

	function vieniatrovarci() {
		Application('frontend/_v/commons/lista-preferiti', null, $this);
	}

	function listapreferiti() {
		Application('frontend/_v/commons/vieni-a-trovarci', null, $this);
	}

}