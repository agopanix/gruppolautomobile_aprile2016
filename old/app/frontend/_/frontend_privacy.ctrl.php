<?php

class frontend_privacyCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile',
			'description' 	=> 'Gruppo L&lsquo;Automobile - Vieni a Trovarci',
			'titleHeader' 	=> 'NOTE LEGALI &amp; PRIVACY',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> '',
			'header-bkg'	=> 'header-bkg-1.jpg',
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/privacy');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

	}

	
}

