<?php
class frontend {

	var $config = [
		'title' => siteName,
		'description' => '',
		'logo' => '//placehold.it/150x150',
		'showHero' => true
	];

	var $catsel=0;



	var $marche = array(
		['id'=>'1','marca'=>'marca 1'],
		['id'=>'2','marca'=>'marca 2']
	);

	var $categorie = array(
		['id'=>'1','name'=>'Neopatentati','img'=>'/upl/neopatentati.png', 'text' =>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'2','name'=>'Iva Esposta','img'=>'/upl/ivaesposta.png','text'=>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'3','name'=>'Aziendali Km0','img'=>'/upl/aziendalikmzero.png','text'=>'Vendita auto km0 dai concessionari di Gruppo L&rsquo;Automobile per assicurarti l&rsquo;auto che hai sempre desiderato al prezzo inaspettato'],
		['id'=>'4','name'=>'Famiglia','img'=>'/upl/famiglie.png','text'=>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'5','name'=>'City Car','img'=>'/upl/citycar.png','text'=>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'6','name'=>'Suv & Fuori Strada','img'=>'/upl/furiostrada.png','text'=>'L&rsquo;incanto sar&agrave; godersi la strada']
	);

	var $autoinofferta = array(
		['id'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
	);

	var $autopreferite = array(
		['id'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'3','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'4','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'5','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'6','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
	);

	var $serviziaggiuntivi = array(
		['id'=>'1','titolo'=>'Noleggio a breve e lungo termine','descrizione'=>'Noleggia le auto che preferisci, scegliendo tra una vasta gamma di opportunità','img'=>'/upl/servizio-1.png','link'=>'/frontend/noleggio/'],
		['id'=>'2','titolo'=>'Carrozzeria','descrizione'=>'Per garantirti un&rsquo;esperienza di mobilità assolutamente sicura, al riparo da qualsiasi rischio ... ','img'=>'/upl/servizio-1.png','link'=>'/frontend/carrozzeria/'],
		['id'=>'3','titolo'=>'Convenzioni','descrizione'=>'Area Riservata','img'=>'/upl/servizio-1.png','link'=>'/frontend/convenzioni/'],
		//['id'=>'2','titolo'=>'Vieni a trovarci','descrizione'=>'Un nostro collaboratore sarà al tuo servizio per l’intera giornata.','img'=>'/upl/servizio-2.png', 'link'=>'contatti/'],
	);

	var $accessori = array(
		['id'=>'1','titolo'=>'ABS'],
		['id'=>'2','titolo'=>'Airbag passeggero'],
		['id'=>'3','titolo'=>'Cambio Automatico'],
		['id'=>'4','titolo'=>'Climatizzatore'],
		['id'=>'5','titolo'=>'Interni in materiale pregiato'],
		['id'=>'6','titolo'=>'Servosterzo'],
		['id'=>'7','titolo'=>'Airbag lato guida'],
		['id'=>'8','titolo'=>'Airbag laterali'],
		['id'=>'9','titolo'=>'Cerchi in lega'],
		['id'=>'10','titolo'=>'Controllo elettr. trazione'],
		['id'=>'11','titolo'=>'Navigatore satellitare'],
		['id'=>'12','titolo'=>'Sospensioni autoregolabili'],
	);

	var $risultato_ricerca = array(
		['id'=>'1','idcategoria'=>'1','marca'=>'Audi','modello'=>'A4', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4],'immatricolazione'=>'24/05/2007','descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','idcategoria'=>'1','marca'=>'BMW','modello'=>'316d', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9],'immatricolazione'=>'24/05/2007', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'3','idcategoria'=>'1','marca'=>'Audi','modello'=>'A4', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4],'immatricolazione'=>'24/05/2007','descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'4','idcategoria'=>'1','marca'=>'BMW','modello'=>'316d', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9],'immatricolazione'=>'24/05/2007', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'5','idcategoria'=>'1','marca'=>'Audi','modello'=>'A4', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4],'immatricolazione'=>'24/05/2007','descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'6','idcategoria'=>'1','marca'=>'BMW','modello'=>'316d', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9],'immatricolazione'=>'24/05/2007', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
	);

	var $dettaglio_veicolo = array(
		['id'=>'1','idcategoria'=>'1','codice'=>'801117','marca'=>'BMW','modello'=>'318d 2.0 143CV cat Touring Attiva', 'normativa'=>'Euro5 (715/2007-692/2008)','colore'=>'Blue','alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9,10,11,12],'luogo'=>'Via Enrico Fermi, 27 - 03100 Frosinone','immatricolazione'=>'24/05/2007','km'=>'106.205','descrizione'=>'', 'motore' =>'2.0 TDI 143CV F.AP. Advanced' ,'cilindrata'=>'1995','kw'=>'135','cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
	);

	var $auto_interesse = array(
		['id'=>'1','idcategoria'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','idcategoria'=>'1','marca'=>'BMW','modello'=>'316d','importo'=>'22900' , 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'2','idcategoria'=>'1','marca'=>'BMW','modello'=>'316d', 'importo'=>'17800' ,'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		);

	var $concessionari = array(
			['id'=>'1',
			'marchi' =>['BMW', 'MINI'],
			'concessionaria'=>'L&rsquo;Automobile S.r.l.',
			'indirizzo'=>'Via Enrico Fermi, 27', 
			'cap'=>'03100',
			'citta'=>'Frosinone',
			'capoluogo'=>'Frosinone',
			'sigla'=>'FR',
			'email'=>'info1@gruppolautomobile.it',
			'telefono'=>'0775/88.50.200',
			'fax'=>'0775.88.50.220',
			'lat'=>'41.621333',
			'long'=>'13.311194'],

			['id'=>'2',
			'marchi' =>['MERCEDES BENZ', 'SMART'],
			'concessionaria'=>"L'Auto",
			'indirizzo'=>'Via Enrico Fermi, 27', 
			'cap'=>'03100',
			'citta'=>'Frosinone',
			'capoluogo'=>'Frosinone',
			'sigla'=>'FR',
			'email'=>'info2@gruppolautomobile.it',
			'telefono'=>'0775/88.50.200',
			'fax'=>'0775.88.50.220',
			'lat'=>'41.621333',
			'long'=>'13.311194'],

			['id'=>'3',
			'marchi' =>['BMW'],
			'concessionaria'=>"L'Automobile S.r.l.",
			'indirizzo'=>'S.R.630 AUSONIA FORMIA-CASSINO', 
			'cap'=>'04023',
			'citta'=>'Formia',
			'capoluogo'=>'Latina',
			'sigla'=>'FR',
			'email'=>'info3@gruppolautomobile.it',
			'telefono'=>'0771.21.434',
			'fax'=>'0771.77.13.91',
			'lat'=>'41.370783',
			'long'=>'13.741623'],

			['id'=>'4',
			'marchi' => ['MERCEDES BENZ','SMART'],
			'concessionaria'=>"L'Auto",
			'indirizzo'=>'Via Epitaffio 74/76', 
			'cap'=>'04100',
			'citta'=>'Latina',
			'capoluogo'=>'Latina',
			'sigla'=>'LT',
			'email'=>'info4@gruppolautomobile.it',
			'telefono'=>'0773.19.90.500',
			'fax'=>null,
			'lat'=>'41.486194',
			'long'=>'12.916413'],

			['id'=>'5',
			'marchi' => ['MERCEDES BENZ','SMART'],
			'concessionaria'=>"L'Auto",
			'indirizzo'=>'Via Sandro Pertini, 167 ', 
			'cap'=>'67051',
			'citta'=>'Avezzano',
			'capoluogo'=>"L'Aquila",
			'sigla'=>'AQ',
			'email'=>'info5@gruppolautomobile.it',
			'telefono'=>'0863.01.50.000',
			'fax'=>null,
			'lat'=>'42.014493',
			'long'=>'13.428692'],

			['id'=>'6',
			'marchi' => ['PORCHE'],
			'concessionaria'=>"L'Auto Sport",
			'indirizzo'=>'Via Epitaffio, 144', 
			'cap'=>'04100',
			'citta'=>'Latina',
			'capoluogo'=>'Latina',
			'sigla'=>'LT',
			'email'=>'info5@gruppolautomobile.it',
			'telefono'=>'0773.19.03.109',
			'fax'=>'0773.1903098',
			'lat'=>'41.529332',
			'long'=>'12.945100'],

			['id'=>'7',
			'marchi' => ['SUZUKI','CITROEN'],
			'concessionaria'=>"L'Automotive",
			'indirizzo'=>'VIA MONTI LEPINI, KM.5,700 SNC', 
			'cap'=>'03023',
			'citta'=>'Ceccano',
			'capoluogo'=>'Frosinone',
			'sigla'=>'FR',
			'email'=>'info6@gruppolautomobile.it',
			'telefono'=>'0775.64.06.84',
			'fax'=>null,
			'lat'=>'41.639601',
			'long'=>'13.342634'],

			['id'=>'8',
			'marchi' => [],
			'concessionaria'=>"ALD AUTOMOTIVE ITALIA",
			'indirizzo'=>'Viale A. Gustave Eiffel, 15', 
			'cap'=>'00148',
			'citta'=>'Roma',
			'capoluogo'=>'Roma',
			'sigla'=>'RM',
			'email'=>'info7@gruppolautomobile.it',
			'telefono'=>'06.65.68.51',
			'fax'=>null,
			'lat'=>'41.811600',
			'long'=>'12.332970']
	
		);

	
	function _config($config) {
		$config = array_merge($this->config, $config);
		$this->config = $config;
	}

		
	function header() {
		Application('frontend/_v/commons/header', null, $this);
	}
	
	function footer() {
		Application('frontend/_v/commons/footer', null, $this);
	}
	
	function menu() {
		Application('frontend/_v/commons/menu', null, $this);
	}

	function vieniatrovarci() {
		Application('frontend/_v/commons/lista-preferiti', null, $this);
	}

	function listapreferiti() {
		Application('frontend/_v/commons/vieni-a-trovarci', null, $this);
	}

}