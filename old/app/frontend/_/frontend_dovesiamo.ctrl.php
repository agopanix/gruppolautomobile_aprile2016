<?php

class frontend_dovesiamoCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile - Dove Siamo',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> 'DOVE SIAMO',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> 'dovesiamo',
			'header-bkg'	=> 'header-bkg-1.jpg',
			'breadcrumbs'	=> Array(
				['name'=>'Dove Siamo','current'=>'current'],
			)
		];
	
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/dovesiamo');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

	}

	
}

