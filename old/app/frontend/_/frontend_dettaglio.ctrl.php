<?php

class frontend_dettaglioCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		foreach ($this->frontend->categorie as $categoria) {
			if($categoria["id"]==$_REQUEST["cat"]){
				$categoria_selezionata = $categoria["name"];
				$testo_categoria = $categoria["text"];
			}
		}
		
		$config = [
			'title' => 'Gruppo L&lsquo;Automobile',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> '',
			'subTitle'		=> '',
			'voceMenu' 		=> '',
			'little-header' => true,
			'header-opaque' => true,
			'header-bkg'	=> 'header-bkg-1.jpg',
			'breadcrumbs'	=> Array(
				['name'=>'Home','current'=>'', 'url'=>'/'],
				['name'=> $categoria_selezionata,'current'=>'','url'=>'/categoria/' . $_REQUEST["cat"]],
				['name'=> $this->frontend->dettaglio_veicolo[0]['marca'] . $this->frontend->dettaglio_veicolo[0]['modello'] ,'current'=>'current']
			)
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/dettaglio');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

	}

	
}

