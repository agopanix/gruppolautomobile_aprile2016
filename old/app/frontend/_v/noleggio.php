<div class="row">
	<div class="small-12 columns padding-top-10 font-size-15 font-bold">
		NOLEGGIA LA TUA AUTO PREFERITA - NOLEGGIO A BREVE E A LUNGO TERMINE
	</div>
</div>
<div class="row">
	<div class="small-12 columns font-size-12  padding-top-10 color-7 font-bold">
		Noleggio auto breve e lungo termine su GruppoLAutomobile.it con le offerte d'eccezione cui non potrai resistere
	</div>
</div>
<div class="row">
	<div class="medium-12 columns padding-top-10 color-7">
		<div class="row">
			<div class="medium-12 columns">
				<img class="fullWidth" src="http://www.gruppolautomobile.it/cdn/images/noleggiobig.png" title="">
			</div>
			<div class="medium-12 columns padding-top-10">
				&quot;Autorent&quot; opera nel mercato del noleggio auto a lungo termine ed a breve termine per offrire la risposta migliore a qualsiasi esigenza di mobilit&agrave;. I nostri servizi offrono soluzioni flessibili dal punto di vista economico e ambientale formulate grazie alle competenze garantite dalla partnership con ALD - azienda leader nel settore Automotive - che in Italia opera nel settore del noleggio LT e nella gestione delle flotte aziendali.
				Abbiamo gestito un parco auto di oltre 17.000 clienti assicurando la mobilit&agrave; quotidiana dei loro utenti.
				Qualsiasi Cliente, dal singolo professionista alla struttura imprenditoriale di piccole e medie dimensioni , fruisce cos&igrave; di un servizio strutturato, efficiente e tempestivo erogato capillarmente su tutto il territorio.
			</div>
		</div>
	</div>
	<div class="medium-12 columns padding-top-20 color-7">
		<b>Con la consulenza specializzata della nostra Azienda potrai:</b><br>
		<ul>
			<li>Scegliere il modello pi&ugrave; in linea alla tua attivit&agrave; professionale, al budget disponibile e al tuo stile di guida;</li>
			<li>Avere una proposta personalizzata di Noleggio con i soli servizi di cui hai realmente bisogno;</li>
			<li>In caso di permuta ottenere una veloce valutazione del tuo usato;</li>
			<li>Contare su un consulente in grado di seguire la tua pratica di noleggio passo dopo passo, dall&rsquo;istruttoria fino alla consegna;</li>
			<li>Ricevere supporto costante durante tutta la durata del contratto, da semplici informazioni alla verifica di eventuali variazioni da apportare ai parametri e ai servizi inclusi.</li>
		</ul>
	</div>
	<div class="medium-12 columns padding-top-5 color-7">
		La nostra ambizione &egrave; divenire il Partner ideale per &ldquo;muovere&rdquo; il tuo business, fornendo servizi personalizzati, omnicomprensivi e rispondenti alle singole esigenze professionali, finanziarie, tecniche e ambientali, con l&rsquo;obiettivo di migliorare costantemente le proprie attivit&agrave; e le proprie aree di business.
	</div>
	<div class="medium-12 columns padding-top-20 color-7">
		<b>MOTIVI PER SCEGLIERE IL NOSTRO NOLEGGIO A LUNGO TERMINE:</b><br>
		<ul>
			<li>Costi certi e pianificabili, che includono l’assicurazione e tutti i servizi per garantire la tua mobilità quotidiana;</li>
			<li>Nessun immobilizzo di capitale, poiché il canone mensile comprende anche l’intero finanziamento del veicolo;</li>
			<li>Consulenza su tutti i processi di gestione e ottimizzazione della tua mobilità o della tua flotta e dei suoi costi;</li>
			<li>Azzeramento di tempo e risorse per la gestione del veicolo e delle pratiche amministrative;</li>
			<li>Nessun impegno e preoccupazione per vendere il tuo usato grazie ad ALD Permuta;</li>
			<li>Possibilità di acquisto del veicolo, al termine del noleggio, a un prezzo vantaggioso per te e la tua famiglia;</li>
			<li>Risposte certe ad ogni tua necessità, grazie a un Customer Service dedicato.</li>
		</ul>
	</div>
	<div class="medium-12 columns padding-top-10 color-7">
		<b>ALCUNI DEI NOSTRI SERVIZI:</b><br>
		<ul>
			<li>Assistenza stradale: siamo sempre al tuo fianco;</li>
			<li>Manutenzione: sia ordinaria sia straordinaria;</li>
			<li>Carta carburante: semplifica la gestione dei rifornimenti;</li>
			<li>Veicolo sostitutivo: la mobilità è la nostra priorità;</li>
			<li>Sostituzione pneumatici: la sicurezza su strada garantita;</li>
			<li>Servizi Telematici: in linea con le tue esigenze;</li>
		</ul>
	</div>
	<div class="medium-12 columns padding-top-10 color-7">
		<a href="http://www.autorent-ald.com/" target="_blank"></a>
	</div>
</div>
<div class="row">
	<div class="small-12 columns padding-top-10">
		<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">COMPILA IL MODULO PER RICHIEDERE INFORMAZIONI SUL NOLEGGIO</div>
	</div>
</div>
<form action="" method="POST">
<div class="row">
	<div class="medium-12 columns">
		<div class="row">
			<div class="medium-12 columns">
				<input type="text"  class="big" placeholder="NOME E COGNOME*"  tabindex="1" required/>
			</div>
			<div class="medium-6 columns">
				<input type="number"  class="big" placeholder="CELLULARE O TELEFONO*"  tabindex="2" required/>
			</div>
			<div class="medium-6 columns">
				<input type="email"  class="big" placeholder="EMAIL*"  tabindex="2" required/>
			</div>
			<div class="medium-6 columns">
				<input type="text"  name="marca" class="big" placeholder="MARCA DI INTERESSE*"  tabindex="3" required/>
			</div>
			<div class="medium-6 columns">
				<input type="text" name="modello"  class="big" placeholder="MODELLO DI INTERESSE*"  tabindex="4" required/>
			</div>
			<div class="medium-6 columns">
				<input type="text" name="limitekm"  class="big" placeholder="MAX KM/ANNO (Limite Km in un anno)*"  tabindex="5" required/>
			</div>
			<div class="medium-6 columns">
				<input type="number" name="duratanoleggio"  class="big" placeholder="DURATA DEL NOLEGGIO IN ANNI*"  tabindex="6" required/>
			</div>
			<div class="medium-6 columns">
				<input type="number" name="partitaiva"  class="big" placeholder="PARTITA IVA*"  tabindex="7" required />
			</div>
			<div class="medium-6 columns">
				<input type="text" name="ragionesociale"  class="big" placeholder="RAGIONE SOCIALE*"  tabindex="8" requyired />
			</div>
			<div class="medium-12 columns">
				<textarea rows="4" cols="30"  name="messaggio" tabindex="5" placeholder="MESSAGGIO" tabindex="9" ></textarea>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns padding-top-10">
				Tutti i campi contrassegnati con * sono obbligatori.
			</div>
		</div>
		<div class="row">
			<div class="small-12 padding-top-10 columns">
				Preferenza sulla modalità di contatto.  <input type="checkbox" name="ricontattotel" value="1"> Cellulare - <input type="checkbox" name="ricontattoemail" value="1"> Email
			</div>
		</div>
		<div class="row">
			<div class="medium-12 columns margin-top-1">
				<input id="authorize-action" tabindex="6" type="checkbox" required><label class="authorize-action-label" for="authorize-action">Acconsento al trattamento dei dati ai sensi e per gli effetti di cui all'articolo 13, D.Lgs 30/06/2003.</label>
			</div>
		</div>
		<div class="row padding-top-20 padding-bottom-20">
			<div class="medium-12 columns text-right ">
				<button type="submit" tabindex="7" >Invia</button>
			</div>
		</div>
	</div>
</div>
</form>