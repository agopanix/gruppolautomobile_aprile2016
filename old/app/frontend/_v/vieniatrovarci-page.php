<div class="row ">
	<div class="font-size-13">
		<div class="row">
			<div class="small-12 columns padding-top-10">
				<div class="map" id="vat-map-2"></div>
				<div class="font-size-11 padding-top-10"> <i class="color-13 fa fa-map-marker"></i> Via Enrico Fermi, 27 - 03100 - Frosinone</div>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<hr>
			</div>
			<div class="small-7 columns padding-bottom-10">
				<div class="font-size-08 color-4">Hai bisogno di un appuntamento specifico?</div>
				<div class="font-size-1 color-4 font-bold">Tel. 0775.8850200</div>
			</div>
			<div class="small-5 columns text-right padding-bottom-10">
				<a class="button font-size-07">Prenota un appuntamento</a>
			</div>
		</div>
	</div>
</div>