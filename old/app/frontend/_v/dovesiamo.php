<div class="row">
	<div class="small-12 columns padding-top-10 font-size-15 font-bold">
		VIENI A TROVARCI IN SEDE
	</div>
</div>

<div class="row">
	<div class="medium-12 columns padding-top-20 padding-bottom-20 font-size-12 color-7">
		<div class="map" id="vat-map-2"></div>
	</div>
</div>

<div class="row">
	<?php foreach ($this->frontend->concessionari as $concessionario) { ?>
	<div class="medium-6 columns">
		<div class="row">
			<div class="small-12 columns padding-top-10">
				<?php 
					$index = 0;
					foreach ( $concessionario["marchi"] as $marchio) { 
						echo '<span class="font-bold">' . $marchio . ' </span>'; 
						$index ++;
						if($index < count($concessionario["marchi"]))
								echo ' - ';
					} 
				?><br>
				<span  <?php if(count($concessionario["marchi"])==0){ echo 'class="font-bold"'; } ?> ><?php echo $concessionario['concessionaria']; ?></span><br>
				<?php echo $concessionario['indirizzo']; ?><br>
				 <?php if($concessionario['citta'] != $concessionario['capoluogo']) echo $concessionario['cap'] .' ' . $concessionario['citta'];?> 
				<?php echo $concessionario['capoluogo']; ?> (<?php echo $concessionario['sigla']; ?>)<br>
				<?php if(isset($concessionario['telefono'])){ echo 'Tel: ' . $concessionario['telefono']; } ?> <br>
				<?php if(isset($concessionario['fax'])){ echo 'Fax: ' . $concessionario['fax']; } ?> <br>
			</div>
		</div>
	</div>
	<?php }?>
</div>

<div class="row">
	<div class="medium-12 columns padding-top-20  color-7">
		<span class="font-bold">SE HAI NECESSITÀ RICHIEDI UN APPUNTAMENTO</span><br>
		<span class="font-size-08">Un nostro responsabile la ricontatter&agrave; per fissare un appuntamento.<br></span>
		<br>
		<form action="#">
			<div class="row ">
				<div class="large-12 columns">
					<input type="text"  class="big" placeholder="NOME E COGNOME"  tabindex="1" required/>
					<input type="email" class="big" placeholder="EMAIL"  tabindex="3" required/>
				</div>
				<div class="large-12 columns">
					<input type="number" class="big" placeholder="TELEFONO"  tabindex="4" required/>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<textarea rows="4" cols="30" class="form-control" name="message" tabindex="5" placeholder="MESSAGGIO" required=""></textarea>
				</div>
			</div>
			<div class="row">
				<div class="large-12 columns margin-top-1">
					<input id="authorize-action" tabindex="6" type="checkbox"><label class="authorize-action-label" for="authorize-action">Autorizzo il trattamento dei dati personali Dlgs 196/2003</label>
				</div>
				<div class="large-12 columns text-right padding-top-13">
					<button type="submit" tabindex="7" >Prenota</button>
				</div>
			</div>
		</form>
		<br>
	</div>
</div>