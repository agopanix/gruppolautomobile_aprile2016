<div class="row margin-top-20">
	<div class="large-9 large-push-3 columns">
		<div class="row collapse">
			<div class="large-6 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-left margin-bottom-10">
				<div class="padding-left-10">RISULTATO RICERCA</div>
			</div>
			<div class="large-6 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-right margin-bottom-10">
				TORNA A: <span class="font-bold margin-right-10">Usato a Km0</span>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<ul class="category-list">
					<?php foreach ($this->frontend->categorie as $categoria) { ?>
					<li>
						<input type="checkbox"  name="ctagorie_group[]" <?php if($categoria['id']==$this->frontend->config["catsel"]){?>checked<? } ?> value="<?php echo $categoria['id'];?>">
						<?php echo $categoria['name'];?>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<?php foreach ($this->frontend->dettaglio_veicolo as $auto) { ?>
				<div class="row car-schedule">
					<div class="medium-8 columns padding-top-10">
						<div class="row ">
							<div class="small-12 columns button-action-landing detail-page">
								<img
								data-interchange="
								[/timthumb.php?src=<?php echo $auto['img'];?>&w=600&h=420&zc=1, (small)],
								[/timthumb.php?src=<?php echo $auto['img'];?>&w=600&h=420&zc=1, (medium)],
								[/timthumb.php?src=<?php echo $auto['img'];?>&w=600&h=420&zc=1, (large)]"
								src="" >
								<a href="#" class="button button-gray-2 font-size-1 font-bold aggiungi-preferiti-scroller"><i class="fa fa-car"></i> Aggiungi ai preferiti </a>
								<div class="social-area">
									<a class="social-icon button button-gray font-bold" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><i class="fa fa-facebook"></i></a>
									<a class="social-icon button button-gray font-bold" target="_blank" href="https://plus.google.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><i class="fa fa-google-plus"></i></a>
									<a class="social-icon button button-gray font-bold" target="_blank"
									href="https://twitter.com/home?status= <?php echo $auto['descrizione']; ?> <?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/' . $auto['idcategoria'] .'/'. $auto['id'] )[1];  ?>"><i class="fa fa-twitter"></i></a>
									<a class="social-icon button button-gray font-bold" target="_blank"
									href="https://twitter.com/home?status= <?php echo $auto['descrizione']; ?> <?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/' . $auto['idcategoria'] .'/'. $auto['id'] )[1];  ?>"><i class="fa fa-youtube"></i></a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="gallery-dettaglio small-12 columns color-7 padding-top-10">
								<ul class="example-orbit" data-orbit  data-options="animation:slide; animation_speed:500;  timer:false; navigation_arrows:true; bullets:false; slide_number: false; resume_on_mouseout: false;">
									<li>
										<div style="margin:0px 30px 30px 30px; height:100%;">
											
											<a class="fancybox" href="<?php echo $auto['img'];?>" data-fancybox-group="gallery" title="Descrizione Modello">
												<img style="width:33.33%; float:left;" src="/timthumb.php?src=<?php echo $auto['img'];?>&w=600&zc=1" alt="slide 1" />
											</a>
											<a class="fancybox" href="<?php echo $auto['img'];?>" data-fancybox-group="gallery" title="Descrizione Modello">
												<img style="width:33.33%; float:left;" src="/timthumb.php?src=<?php echo $auto['img'];?>&w=600&zc=1" alt="slide 1" />
											</a>
											<a class="fancybox" href="<?php echo $auto['img'];?>" data-fancybox-group="gallery" title="Descrizione Modello">
												<img style="width:33.33%; float:left;" src="/timthumb.php?src=<?php echo $auto['img'];?>&w=600&zc=1" alt="slide 1" />
											</a>
											
										</div>
									</li>
									
									<li>
										<div style="margin:0px 30px 30px 30px; height:100%;">
											
											<a class="fancybox" href="<?php echo $auto['img'];?>" data-fancybox-group="gallery" title="Descrizione Modello">
												<img style="width:33.33%; float:left;" src="/timthumb.php?src=<?php echo $auto['img'];?>&w=200&zc=1" alt="slide 1" />
											</a>
											<a class="fancybox" href="<?php echo $auto['img'];?>" data-fancybox-group="gallery" title="Descrizione Modello">
												<img style="width:33.33%; float:left;" src="/timthumb.php?src=<?php echo $auto['img'];?>&w=200&zc=1" alt="slide 1" />
											</a>
											<a class="fancybox" href="<?php echo $auto['img'];?>" data-fancybox-group="gallery" title="Descrizione Modello">
												<img style="width:33.33%; float:left;" src="/timthumb.php?src=<?php echo $auto['img'];?>&w=200&zc=1" alt="slide 1" />
											</a>
											
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="small-12 columns color-7 padding-top-10 font-size-10">
								<i class="color-13 fa fa-map-marker"></i> <span class="font-bold">Disponibile in sede: </span> <?php echo $auto['luogo'];?>
							</div>
						</div>
					</div>
					<div class="medium-4 columns padding-top-10 color-7">
						<div class="row">
							<div class="medium-12 columns font-bold "><?php echo $auto['marca'];?> <?php echo $auto['modello'];?></div>
							<div class="medium-12 columns font-size-14"><?php echo callFunction('frontend','price',$auto['importo'],'€');?></div>
						</div>
						<!--<div class="row padding-top-10">
							<div class="small-12 columns  border-bottom-gray">
								<div class="font-bold">Codice Auto</div>
								<div><?php echo $auto['codice']; ?></div>
							</div>
						</div>-->
						<div class="row padding-top-10">
							<div class="small-12 columns  border-bottom-gray">
								<!--<div class="font-bold">Carburante</div>-->
								<div><?php echo $auto['alimentazione']; ?></div>
							</div>
						</div>
						<div class="row padding-top-10">
							<div class="small-12 columns  border-bottom-gray">
								<!-- <div class="font-bold">Colore</div>-->
								<div><?php echo $auto['colore']; ?></div>
							</div>
						</div>
						<div class="row padding-top-10">
							<div class="small-12 columns  border-bottom-gray">
								<!-- <div class="font-bold">Cilindrata</div>-->
								<div><?php echo $auto['cilindrata']; ?> cc</div>
							</div>
						</div>
						<div class="row padding-top-10">
							<div class="small-12 columns  border-bottom-gray">
								<!-- <div class="font-bold">Potenza Cav / Kw</div> -->
								<div><?php echo $auto['cavalli']; ?> cv / <?php echo $auto['kw']; ?> kw</div>
							</div>
						</div>
						<div class="row padding-top-10">
							<div class="small-12 columns  border-bottom-gray">
								<!-- <div class="font-bold">Km percorsi</div>-->
								<div><?php echo $auto['km']; ?> km</div>
							</div>
						</div>
						<div class="row padding-top-10">
							<div class="small-12 columns  border-bottom-gray">
								<div><?php echo $auto['normativa']; ?></div>
							</div>
						</div>
						<div class="row padding-top-10">
							<div class="small-12 columns  border-bottom-gray">
								<div class="font-bold">Prima Immatricolazione</div>
								<div><?php echo $auto['immatricolazione']; ?></div>
							</div>
						</div>
						
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<div class="row button-action-landing">
			<div class="medium-4 columns padding-top-10">
				<a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>"class="button fullWidth font-size-08 font-bold"><i class="fa fa-calendar"></i> VIENI A TROVARCI</a>
			</div>
			<div class="medium-4 columns padding-top-10">
				<a href="<? echo createUrl('frontend/contatti/GetIndex')[1]; ?>"class="button fullWidth button-gray-2 font-size-08 font-bold"><i class="fa fa-envelope"></i> RICHIEDI INFORMAZIONI</a>
			</div>
			<div class="medium-4 columns padding-top-10">
				<a href="<? echo createUrl('frontend/contatti/GetIndex')[1]; ?>"class="button fullWidth button-gray-2 font-size-08 font-bold"><i class="fa fa-envelope"></i> SCARICA  SCHEDA</a>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns padding-top-10">
				<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">EQUIPAGGIAMENTO </div>
			</div>
		</div>
		<div class="row border-bottom-gray color-7" data-equalizer data-equalizer-mq="medium-up">
			<div class="medium-6 columns" data-equalizer-watch>
				<span class="font-bold centered-text padding-top-10">Equipaggiamento del veicolo</span>
			</div>
			<div class="medium-6 columns padding-bottom-10" data-equalizer-watch>
				<?php
					foreach ($auto['accessori'] as $accessorio) {
						echo ' ' . callFunction('frontend','getAccessorio', $accessorio, $this->frontend->accessori) . '<br>';
					}
				?>
			</div>
		</div>
		<div class="row border-bottom-gray color-7" data-equalizer data-equalizer-mq="medium-up">
			<div class="medium-6 columns padding-top-10 padding-bottom-10" data-equalizer-watch>
				<span class="font-bold centered-text ">Ulteriori specifiche del veicolo</span>
			</div>
			<div class="medium-6 columns padding-top-10 padding-bottom-10" data-equalizer-watch>
				<span class="font-bold">Tipo di cambio:</span> meccanico<br>
				<span class="font-bold">Prossima revisione:</span> 03/03/2016<br>
				<span class="font-bold">Ultima revisione:</span> 03/03/2014<br>
			</div>
		</div>
		<div class="row color-7">
			<div class="small-6 columns padding-top-10">
				<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">INFO RIVENDITORE</div>
				<div class="row">
					<div class="small-12 columns">
						<div class="map-dettaglio margin-bottom-10" id="vat-map-2"></div>
						<span class="font-bold">GRUPPO L’AUTOMOBILE</span><br>
						Via Enrico Fermi, 27 - 03100 Frosinone<br>
						Tel.: +39 - 0775 - 88 50 222<br>
						Mob.: +39 - 345 - 39 70 670<br>
						Fax: +39 - 0775 - 88 50 235<br>
					</div>
				</div>
			</div>
			<div class="small-6 columns padding-top-10">
				<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">CONTATTA IL RIVENDITORE</div>
				<div class="row">
					<div class="small-12 columns">
						<form action="#">
							<div class="row ">
								<div class="large-12 columns">
									<input type="text"  class="big" placeholder="NOME E COGNOME"  tabindex="1" required/>
									<input type="email" class="big" placeholder="EMAIL O TELEFONO"  tabindex="3" required/>
								</div>
							</div>
							<div class="row">
								<div class="small-12 columns">
									<textarea rows="4" cols="30" class="form-control" name="message" tabindex="5" placeholder="MESSAGGIO" required=""></textarea>
								</div>
							</div>
							<div class="row">
								<div class="large-12 columns margin-top-1">
									<input id="authorize-action" tabindex="6" type="checkbox"><label class="authorize-action-label" for="authorize-action">Autorizzo il trattamento dei dati personali Dlgs 196/2003</label>
								</div>
								<div class="large-12 columns text-left padding-top-20">
									<button type="submit" tabindex="7" class="fullWidth">RICHIEDI INFORMAZIONI</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">ALTRE VETTURE CHE POTREBBERO INTERESSARTI</div>
			</div>
		</div>
		<div class="row padding-bottom-20">
			<?php foreach ($this->frontend->auto_interesse as $auto) { ?>
			<div class="medium-4 columns padding-top-10">
				<a href="<? echo createUrl('frontend/dettaglio/' . $auto['id'])[1]; ?>">
					<img data-interchange="
					[/timthumb.php?src=<?php echo $auto['img'];?>&w=900&h=300zc=1, (small)],
					[/timthumb.php?src=<?php echo $auto['img'];?>&w=900&h=300zc=1, (medium)],
					[/timthumb.php?src=<?php echo $auto['img'];?>&w=600&h=600&zc=1, (large)]"
					src="" >
					<div class="font-bold color-7"><?php echo $auto["marca"];?> <?php echo $auto["modello"];?></div>
					<div class="font-bold color-8"><?php echo callFunction('frontend','price',$auto['importo'],'€');?></div>
				</a>
			</div>
			<?php } ?>
		</div>
	</div>
	<div class="large-3 large-pull-9 columns left">
		<form action="#" method="post">
			<div class="row">
				<div class="small-12 columns">
					<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">RICERCA AVANZATA</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Marchio
						<select name="marchio">
							<option value="-">-</option>
						</select>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Modello
						<select name="modello">
							<option value="-">-</option>
						</select>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Alimentazione
						<select name="alimentazione">
							<option value="-">-</option>
						</select>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Colore
						<select name="colore">
							<option value="-">-</option>
						</select>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Da Km percorsi
						<input type="number"name="kmpercorsi">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Da Km percorsi
						<input type="number"name="kmpercorsi">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Da anno immatric. (es.:2006)
						<input type="number" name="daannoimm">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Da Km percorsi
						<input type="number"name="kmpercorsi">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Ad anno immatric. (es.:2006)
						<input type="number" name="adannoimm">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Da prezzo (senza simbolo € )
						<input type="number"name="daprezzo">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>A prezzo (senza simbolo € )
						<input type="number"name="daprezzo">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<button class="button fullWidth" type="submit">FILTRA RICERCA</button>
				</div>
			</div>
		</form>
		<form action="#" method="post">
			<div class="row">
				<div class="small-12 columns">
					<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">ACCESSORI</div>
				</div>
			</div>
			<?php foreach ($this->frontend->accessori as $accessorio) { ?>
			<div class="row">
				<div class="small-12 columns">
					<input type="checkbox"  name="accessori_group[]" value="<?php echo $accessorio['id'];?>">
					<?php echo $accessorio['titolo'];?>
				</div>
			</div>
			<? } ?>
			<div class="row">
				<div class="small-12 columns">
					<button class="button fullWidth" type="submit">FILTRA RICERCA</button>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="fullWidth padding-for-small bkg-color-0 padding-bottom-20 padding-top-20">
	<div class="row">
		<div class="small-12 columns text-center">
			<div class="titolo-sezione font-bold color-6">Servizi Aggiuntivi</div>
			<a href="" class="color-6" >Solo il meglio per inostri clienti</a>
		</div>
	</div>
	<div class="row">
		<?php foreach ($this->frontend->serviziaggiuntivi as $i=>$servizio) { ?>
			<!-- <div class="<?= ( $i & 1 ) ? 'medium-8' : 'medium-4' ; ?> columns relative padding-top-10">-->
			<div class="medium-4 columns relative padding-top-10">
				<div class="servizi-aggiuntivi card relative">
					<a href="<?php echo $servizio['link'];?>">
						<img
						data-interchange="
						[/timthumb.php?src=<?php echo $servizio['img'];?>&h=700&w=900&zc=1, (small)],
						[/timthumb.php?src=<?php echo $servizio['img'];?>&h=455&zc=1, (medium)],
						[/timthumb.php?src=<?php echo $servizio['img'];?>&h=900&zc=1, (large)]"
						src="/timthumb.php?src=<?php echo $servizio['img'];?>&h=908&zc=1" >
						<div class="description-2 padding-top-05 padding-bottom-05">
							<div class="<?= ( $i & 1 ) ? 'text2' : 'text' ; ?>  text-center centered-text">
								<span class="titolo titolo-txt line-height-10 font-bold"><?php echo $servizio['titolo'];?></span><br>
								<span class="descrizione descrizione-txt"><?php echo $servizio['descrizione'];?></span>
							</div>
						</div>
					</a>
				</div>
			</div>
			<?php } ?>
	</div>
</div>