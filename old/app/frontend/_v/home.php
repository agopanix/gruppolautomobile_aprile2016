<?php
//var_dump($this->frontend);
?>
<div class="row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 text-center padding-top-20">
				<h1 class="h1 titolo-sezione font-bold color-6">Scegli la tua auto usata garantita</h1>
				<p>
					<span class="content font-bold color-6">Gruppo L'Automobile è il portale delle auto usate garantite dai concessionari uffciali con offerte su oltre cinquecento vetture.</span><br>
					<span class="content color-7">Gruppo L'Automobile: portale delle auto usate racchiude in sé la storia di oltre 50 anni di esperienza nella vendita auto. Vi invitiamo a scoprire le
					nostre offerte su oltre 500 vetture selezionate da ben sette concessionari ufficiali per i più prestigiosi marchi: BMW, Mercedes-Benz, Smart, Porsche, MINI, Citroen e Suzuki. </span>
					<!--<a href="">Visualizza Altro <i class="fa fa-sort-desc"></i> </a>-->
				</p>
			</div>
		</div>
		<div class="row">
			<?php foreach ($this->frontend->categorie as $cat) { ?>
			<div class="small-6 medium-4 columns text-center relative padding-top-10">
				<a class="category-link" href="<? echo createUrl('frontend/categoria/' . $cat['id'] )[1]; ?>">
					<div class="box-background relative">
						<img class="categoryimg" src="/timthumb.php?src=<?php echo $cat['img'];?>&w=900" >
						<div class="titolo-categoria centered-text color-0 font-bold font-size-20"><?php echo $cat['name'];?></div>
					</div>
				</a>
			</div>
			<?php } ?>
		</div>
		<!-- AUTO IN OFFERTA -->
		<div class="row padding-top-20 padding-bottom-20">
			<div class="small-12 text-center ">
				<div class="titolo-sezione font-bold color-6">Scopri le offerte della settimana</div>
				<div>
					<a href="<? echo createUrl('frontend/vieniatrovarci/GetIndex/1')[1]; ?>" class="color-6" >Visualizza tutti i migliori modelli di usato garantito »</a>
				</div>
			</div>
		</div>
		<div class="row padding-bottom-40">
			<?php foreach ($this->frontend->autoinofferta as $i=>$auto) { ?>
			<div class="<?= ( $i & 1 ) ? 'medium-4' : 'medium-8' ; ?> columns text-center relative padding-top-10">
				<div class="card relative">
					<a href="">
						<img data-interchange="
						[/timthumb.php?src=<?php echo $auto['img'];?>&h=700&w=900&zc=1, (small)],
						[/timthumb.php?src=<?php echo $auto['img'];?>&h=455&zc=1, (medium)],
						[/timthumb.php?src=<?php echo $auto['img'];?>&h=455&zc=1, (large)]"
						src="/timthumb.php?src=<?php echo $auto['img'];?>&h=455&zc=1" >
						<div class="description padding-top-05 padding-bottom-05 text-left font-bold">
							<div class="small-4 medium-8 columns truncate">
								<?php echo $auto['descrizione'];?>
							</div>
							<div class="small-8 medium-4 columns color-8 text-right">
								<?php echo callFunction('frontend','price',$auto['importo'],'€');?>
							</div>
						</div>
					</a>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>

<div class="fullWidth padding-for-small bkg-color-0 padding-bottom-20">
	<div class="row">
		<div class="small-12 columns text-center">
				<div class="titolo-sezione font-bold color-6 padding-top-10">Servizi Aggiuntivi</div>
				<a href="" class="color-6" >Solo il meglio per inostri clienti</a>
		</div>
	</div>
	<div class="row">
		<?php foreach ($this->frontend->serviziaggiuntivi as $i=>$servizio) { ?>
			<!-- <div class="<?= ( $i & 1 ) ? 'medium-8' : 'medium-4' ; ?> columns relative padding-top-10">-->
			<div class="medium-4 columns relative padding-top-10">
				<div class="servizi-aggiuntivi card relative">
					<a href="<?php echo $servizio['link'];?>">
						<img
						data-interchange="
						[/timthumb.php?src=<?php echo $servizio['img'];?>&h=700&w=900&zc=1, (small)],
						[/timthumb.php?src=<?php echo $servizio['img'];?>&h=455&zc=1, (medium)],
						[/timthumb.php?src=<?php echo $servizio['img'];?>&h=900&zc=1, (large)]"
						src="/timthumb.php?src=<?php echo $servizio['img'];?>&h=908&zc=1" >
						<div class="description-2 padding-top-05 padding-bottom-05">
							<div class="<?= ( $i & 1 ) ? 'text2' : 'text' ; ?>  text-center centered-text">
								<span class="titolo titolo-txt line-height-10 font-bold"><?php echo $servizio['titolo'];?></span><br>
								<span class="descrizione descrizione-txt"><?php echo $servizio['descrizione'];?></span>
							</div>
						</div>
					</a>
				</div>
			</div>
			<?php } ?>
	</div>
</div>
