
<div class="row ">
	<div class="large-4 columns padding-top-20">
		<div class="font-bold font-size-12">CONTATTI</div>
		<span class="bold underline"><span class="italic">Gruppo</span> L'Automobile</span><br>
		<span>Via Enrico Fermi, 27<br>
			Telefono: +39 0775.8850200<br>
			Fax: +39 0775.8850200<br>
			Mail: <a href="mailto:info@gruppolautomobile.it">info@gruppolautomobile.it</a><br>
		</span>
		

	</div>
	<div class="large-8 columns">
		<div class="row ">
			<div class="large-12 columns">
			<div class="padding-top-20 padding-bottom-10 font-bold font-size-12">RICHIEDI INFORMAZIONI</div>
				<form action="#">
					<div class="row ">
						<div class="large-6 columns">
							<input type="text"  class="big" placeholder="NOME"  tabindex="1" required/>
							<input type="email" class="big" placeholder="EMAIL"  tabindex="3" required/>
						</div>
						<div class="large-6 columns">
							<input type="text"  class="big" placeholder="COGNOME"  tabindex="2" required/>
							<input type="number" class="big" placeholder="TELEFONO"  tabindex="4" required/>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<textarea rows="4" cols="30" class="form-control" name="message" tabindex="5" placeholder="MESSAGGIO" required=""></textarea>
						</div>
					</div>
					<div class="row">
						<div class="large-9 columns margin-top-1">
							<input id="authorize-action" tabindex="6" type="checkbox"><label class="authorize-action-label" for="authorize-action">Autorizzo il trattamento dei dati personali Dlgs 196/2003</label>
						</div>
						<div class="large-3 columns text-right margin-top-1">
							<button type="submit" tabindex="7" >Invia</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>