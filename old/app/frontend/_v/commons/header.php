<?
$frontend = $app_data->config;
?>
<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><? echo $frontend["title"]; ?></title>
		<meta name="description" value="" />
		<script src="//fast.eager.io/6iWfr1UhwG.js"></script>
		<link rel="stylesheet" href="/cdn/css/foundation.css" />
		<script src="/cdn/js/vendor/modernizr.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js"></script>
		<link rel="stylesheet" href="/cdn/css/gruppolautomobile.css" />
		<link rel="stylesheet" href="/cdn/css/font-awesome/css/font-awesome.min.css" />
		<!-- socials -->
		<meta property="og:title" content="<?php echo $frontend["titleHeader"]; ?>" />
		<meta property="og:url" content="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" />
		<meta property="og:description" content="<?php echo $frontend["description"]; ?>"/>
		<meta property="og:site_name" content="<? echo $seo["title"]; ?>">
		<meta property="og:type" content="website" />
		<meta property="og:updated_time" content="<? echo time(); ?>" />
		
		<meta name="twitter:card" content="photo">
		<meta name="twitter:site" content="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
		<meta name="twitter:creator" content="@Gruppolautomobile">
		<meta name="twitter:url" content="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
		<meta name="twitter:description" content="<?php echo $frontend["description"]; ?>">
		<?php if(isset($app_data->dettaglio_veicolo)){?>
		<meta name="twitter:title" content="<?php echo $app_data->dettaglio_veicolo[0]["marca"] . " " . $app_data->dettaglio_veicolo[0]["modello"] ?>">
		<meta property="og:image" itemprop="image" content="<?php echo 'http://' . $_SERVER['HTTP_HOST'] .  $app_data->dettaglio_veicolo[0]["img"];?>" />
		<meta name="twitter:image" content="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $app_data->dettaglio_veicolo[0]["img"];?>" />
		<?php } ?>
	</head>
	<body class="bkg-color-2">
		<div class="row bkg-color-0 text-center fullWidth relative hidden-for-small">
			<a href="/">
				<img class="logo margin-bottom-10 margin-top-10" src="/cdn/images/main-logo.png">
			</a>
		</div>
		<div class="header fullWidth relative <?php if(isset($frontend["little-header"])){ echo 'little-header'; } ?>">
			<nav class="mainmenu top-bar relative" data-topbar role="navigation">
				<ul class="title-area">
					<li class="name show-for-small-down">
						<h1><a href="#" class="underline"><span class="italic">Gruppo</span> L'Automobile</a></h1>
					</li>
					<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
				</ul>
				<section class="top-bar-section">
					<ul class="right">
						<?php if(isset($frontend["showHomebtn"])){?>
						<li><a class="shadow" href="/"> <i class="fa fa-home"></i> Torna al Sito</a></li>
						<?php } ?>
						<li class="has-dropdown">
							<a class="shadow" href="#"> <i class="fa fa-car"></i> Scegli la tua auto</a>
							<ul class="dropdown">
								<?php foreach ($app_data->categorie as $cat) { ?>
								<li><a href="<? echo createUrl('frontend/categoria/' . $cat['id'] )[1]; ?>"><?php echo $cat['name'];?></a></li>
								<?php } ?>
							</ul>
						</li>
						<li><a class="shadow" href="#" id="vieniatrovarci-btn-menu"> <i class="fa fa-calendar"></i> Vieni a tovarci</a></li>
						<li <? echo $frontend["voceMenu"] == 'preferiti' ? 'class="active"' : ''; ?>><a  class="shadow" href="<? echo createUrl('frontend/preferiti/GetIndex')[1]; ?>"> <i class="fa fa-car"></i> Lista dei preferiti</a></li>
						<li <? echo $frontend["voceMenu"] == 'chisiamo' ? 'class="active"' : ''; ?>><a class="shadow" href="<? echo createUrl('frontend/chisiamo/GetIndex')[1]; ?>"> <i class="fa fa-heart"></i> Chi Siamo</a></li>
						<li <? echo $frontend["voceMenu"] == 'dovesiamo' ? 'class="active"' : ''; ?>><a class="shadow" href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>"> <i class="fa fa-map-marker"></i> Dove Siamo</a></li>
						<li <? echo $frontend["voceMenu"] == 'contatti' ? 'class="active"' : ''; ?>><a class="shadow" href="<? echo createUrl('frontend/contatti/GetIndex')[1]; ?>"> <i class="fa fa-envelope"></i> Contatti</a></li>
					</ul>
				</section>
			</nav>
			<img class="header-image fullWidthImg relative hidden-for-small-down" src="/cdn/images/<? echo $frontend["header-bkg"]; ?>">
			<div class="title-page centerer text-center color-0">
				<span class="text1 font-bold shadow"><?php echo $frontend["titleHeader"]; ?></span><br>
				<span class="text2 shadow"><?php echo $frontend["subTitle"]; ?></span>
			</div>
			<?php if(!isset($frontend["header-opaque"])) { ?>
			<div class="area-selector text-center padding-top-14 hidden-for-medium" >
				<div class="row">
					<div class="small-12 columns">
						<div class="row collapse">
							<div class="medium-2 columns">
								<select name="marca" required>
									<option value="-">Marca</option>
								</select>
							</div>
							<div class="medium-2 columns">
								<select name="modello" required>
									<option value="-">Modello</option>
								</select>
							</div>
							<div class="medium-2 columns">
								<select name="valorefinoa" required>
									<option value="-">Prezzo fino a</option>
									<option value="5000">    5.000 €</option>
									<option value="10000">  10.000 €</option>
									<option value="15000">  15.000 €</option>
									<option value="20000">  20.000 €</option>
									<option value="30000">  30.000 €</option>
									<option value="50000">  50.000 €</option>
								</select>
							</div>
							<div class="medium-2 columns">
								<select name="kmfinoa" required>
									<option value="-">	   KM fino a</option>
									<option value="5000">    5.000 KM</option>
									<option value="10000">  10.000 KM</option>
									<option value="15000">  15.000 KM</option>
									<option value="20000">  20.000 KM</option>
									<option value="30000">  30.000 KM</option>
									<option value="50000">  50.000 KM</option>
									<option value="100000">100.000 KM</option>
								</select>
							</div>
							<div class="medium-2 columns">
								<select name="annoda" required>
									<option value="-">Anno da</option>
									<option value="2014">2014</option>
									<option value="2013">2013</option>
									<option value="2012">2012</option>
									<option value="2010">2011</option>
									<option value="2009">2010</option>
									<option value="2008">2009</option>
									<option value="2007">2008</option>
									<option value="2006">2007</option>
									<option value="2005">2005</option>
									<option value="2004">2004</option>
								</select>
							</div>
							
							<div class="medium-2 columns small text-left">
								<button class="button submit width100 heightHowInput">Ricerca</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if(isset($frontend["header-opaque"])) { ?>
			<div class="header-opaque"></div>
			<?php } ?>
			<?php if(isset($frontend["landingOfferta"])) { ?>
			<div class="area-selector text-center padding-top-14 hidden-for-medium" style="">
				<div class="row button-action-landing" style="max-width:600px;">
					<div class="medium-6 columns text-left">
						<a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>"class="button fullWidth font-size-08 font-bold"><i class="fa fa-calendar"></i> VIENI A TROVARCI</a>
					</div>
					<div class="medium-6 columns text-left">
						<a href="<? echo createUrl('frontend/contatti/GetIndex')[1]; ?>"class="button fullWidth button-gray-2 font-size-08 font-bold"><i class="fa fa-envelope"></i> RICHIEDI INFORMAZIONI</a>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="breadcrumbs-row fullWidth">
		<div class="row">
			<div class="small-12 columns padding-top-05 padding-bottom-05">
				<ul class="breadcrumbs font-bold font-size-10">
					<!--<li><a href="#">Home</a></li>
					<li><a href="#">Features</a></li>
					<li class="unavailable"><a href="#">Gene Splicing</a></li>
					<li class="current"><a href="#">Cloning</a></li>-->
					<?php 
					foreach ($frontend["breadcrumbs"] as $breadcrumb) { 
						echo '<li class="' . $breadcrumb['current']  . '"><a href="' . $breadcrumb['url'] .'">' . $breadcrumb['name'] . '</a></li>';
					 } 
				 ?>
				</ul>

				
			</div>
		</div>
		</div>
		<div id="endheader"> </div>