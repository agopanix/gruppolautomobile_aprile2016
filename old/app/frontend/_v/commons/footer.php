<?
	$frontend = $app_data;
?>

<div class="fullWindow bkg-color-0 ">
	<div class="row padding-bottom-10 padding-top-10">
		<div class="small-12 columns text-center">
			<div class="titolo-sezione tfooter font-bold color-6"><span>&nbsp;&nbsp;Concessionarie Ufficiali&nbsp;&nbsp;</span></div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns padding-bottom-30">
			<ul class="concessionari">
				<li>
					<a href="http://concessionaria.bmw.it/automobile/it_IT/home.html" target="_blank">
						<div class="relative area-logo"><img class="centered-text" src="/cdn/images/bmw-logo.png"></div><span class="marca">BMW</span><br><span class="concessionaria">L'Automobile S.r.l.</span></li>
					</a>
					<li><a href="http://www.lauto.mercedes-benz.it/content/italy/retail-0/lauto/it/desktop/home.html" target="_blank">
						<div class="relative area-logo"><img class="centered-text" src="/cdn/images/mercedes-benz-logo.png"></div><span class="marca">Mercedes Benz</span><br><span class="concessionaria">L'Auto</span>
					</a></li>
					<li><a href="http://www.dealers.it.porsche.com/latina/ita/" target="_blank">
						<div class="relative area-logo"><img class="centered-text" src="/cdn/images/porche-logo.png"></div><span class="marca">Porche</span><br><span class="concessionaria">Centro Porche Latina</span>
					</a></li>
					<li><a href="http://www.automobile.mini.it/dealer/automobile/index.html" target="_blank">
						<div class="relative area-logo"><img class="centered-text" src="/cdn/images/mini-logo.png"></div><span class="marca">Mini</span><br><span class="concessionaria">L'Automobile S.r.l.</span>
					</a></li>
					<li><a href="http://www.lauto.mercedes-benz.it/content/italy/retail-0/lauto/it/desktop/home.html" target="_blank">
						<div class="relative area-logo"><img class="centered-text" src="/cdn/images/smart-logo.png"></div><span class="marca">Smart</span><br><span class="concessionaria">L'Auto S.r.l.</span>
					</a></li>
					<li><a href="http://www.concessionario.citroen.it/lautomotive" target="_blank">
						<div class="relative area-logo"><img class="centered-text" src="/cdn/images/citroen-logo.png"></div><span class="marca">Citroen</span><br><span class="concessionaria">L'Automotive S.r.l.</span>
					</a></li>
					<li><a href="http://auto.suzuki.it/dealer/93885/l-automotive-srl.aspx" target="_blank">
						<div class="relative area-logo"><img class="centered-text" src="/cdn/images/suzuki-logo.png"></div><span class="marca">Suzuki</span><br><span class="concessionaria">L'Automotive S.r.l.</span>
					</a></li>
					<li>
						<a href="http://mobilitysolutions.aldautomotive.it/" target="_blank">
							<div class="relative area-logo ald-fix"><img class="centered-text" src="/cdn/images/ald-logo.png"></div><span class="marca">Autorent</span><br><span class="concessionaria">ALD Automotive</span></li>
						</a>
					</ul>
				</div>
			</div>
		</div>
		<div class="fullWidth bkg-color-4 color-0  padding-top-25">
			<div class="footer row">
				<div class="medium-6 large-4 columns">
					<ul class="contacts">
						<li class="font-bold">Gruppo L’ Automobile</li>
						<li>Via Enrico Fermi, 27</li>
						<li>03100 Frosinone (FR)</li>
						<li>Tel. 0775.8850200</li>
						<li>Fax 0775.8850200</li>
						<li>E-mail : info@gruppolautomobile.it</li>
						<li>Partita IVA 11356071008</li>
						<li>Termini e condizioni d'uso</li>
						<li><a href="<? echo createUrl('frontend/privacy/GetIndex')[1]; ?>">Privacy</a></li>
						<li>Credits: <a href="mailto:http://www.mandarinoadv.com">http://www.mandarinoadv.com</a></li>
					</ul>
				</div>
				<div class="medium-6 large-4 columns">
					<ul class="contacts">
						<li><a href="/">Home</a></li>
						<li><a href="/">Scegli la tua Auto</a></li>
						<li><a href="<? echo createUrl('frontend/appuntamento/GetIndex')[1]; ?>">Prendi un appuntamento</a></li>
						<li><a href="<? echo createUrl('frontend/preferiti/GetIndex')[1]; ?>">La tua lista dei desideri</a></li>
						<li><a href="<? echo createUrl('frontend/chisiamo/GetIndex')[1]; ?>">Chi siamo</a></li>
						<li><a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>">Dove siamo</a></li>
						<li><a href="<? echo createUrl('frontend/contatti/GetIndex')[1]; ?>">Contatti</a></li>
						<!--<li class="socials">
									<a href="https://www.facebook.com/pages/Auto365/398395653621974?ref=hl" target="_blank" ><i class="fa fa-facebook-official"></i></a>
									<a href="https://plus.google.com/109520175614070557000" target="_blank"><i class="fa fa-google-plus-square" ></i></a>
									<a href="http://www.youtube.com/channel/UCRl0qCxK-xiF2UbUSONNWKQ/feed" target="_blank"><i class="fa fa-youtube-square" ></i></a>
						</li>-->
						<li class="social-area">
							<a class="social-icon button button-gray font-bold" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><i class="fa fa-facebook"></i></a>
							<a class="social-icon button button-gray font-bold" target="_blank" href="https://plus.google.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><i class="fa fa-google-plus"></i></a>
							<a class="social-icon button button-gray font-bold" target="_blank"	href="https://twitter.com/home?status= <?php echo $auto['descrizione']; ?> <?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/' . $auto['id'] )[1];  ?>"><i class="fa fa-twitter"></i></a>
							<a class="social-icon button button-gray font-bold" target="_blank"	href="https://twitter.com/home?status= <?php echo $auto['descrizione']; ?> <?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/' . $auto['id'] )[1];  ?>"><i class="fa fa-youtube"></i></a>
						</li>
					</ul>
				</div>
				<div class="medium-12 large-4 columns">
					<form class="contact-form" action="#">
						<input type="text" class="form-control" name="name" placeholder="Nome e Cognome" required />
						<input type="email" class="form-control" name="email" placeholder="Email" required />
						<textarea rows="4" cols="30" class="form-control" name="message" placeholder="Messaggio" required></textarea>
						<div class="margin-top-1">
							<input id="autorize-action-mail" type="checkbox" class="autorize-action-mail"><label class="autorize-action-mail-label" for="autorize-action-mail">Acconsento al trattamento dei dati ai sensie per gli effetti di cui all'articolo 13, D.Lgs 30/06/2003.</label>
						</div>
						<div class="text-right padding-top-10">
							<button type="button" class="submit-gray">Invia</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- footer-->
		<script src="/cdn/js/vendor/jquery.js"></script>
		<script src="/cdn/js/vendor/jquery.fittext.js"></script>
		<script type="text/javascript" src="/cdn/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
		<!-- Add fancyBox main JS and CSS files -->
		<script type="text/javascript" src="/cdn/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="/cdn/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
		<!-- Add Button helper (this is optional) -->
		<link rel="stylesheet" type="text/css" href="/cdn/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
		<script type="text/javascript" src="/cdn/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<!-- Add Thumbnail helper (this is optional) -->
		<link rel="stylesheet" type="text/css" href="/cdn/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
		<script type="text/javascript" src="/cdn/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
		<!-- Add Media helper (this is optional) -->
		<script type="text/javascript" src="/cdn/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
		<script src="/cdn/js/app.js"></script>
		<script src="/cdn/js/foundation.min.js"></script>
		<script src="/cdn/js/foundation/foundation.clearing.js"></script>
		<script>
			$(document).foundation();
		</script>

		<script type="text/javascript">

			$(document).ready(function(){

				if ($('#vat-map-2').length) {

					var concessionarie = <?php echo json_encode($frontend->concessionari); ?>; 
			       
			        var map = new google.maps.Map(document.getElementById('vat-map-2'), {
			            disableDefaultUI: true,
			            disableDefaultUI: true,
			            navigationControl: false,
			            mapTypeControl: false,
			            scaleControl: false,
			            draggable: false,
			            scrollwheel: false
			        });

			        var markers = [];

			        var infowindow =  new google.maps.InfoWindow({
       					content: ""
					});
			         



			    	for( i = 0; i < concessionarie.length; i++) {

			    		var latLng = new google.maps.LatLng(concessionarie[i].lat, concessionarie[i].long);
			            var marker = new google.maps.Marker({
			                position: latLng,
			                map: map,
			                title: concessionarie[i].concessionaria + " " +  concessionarie[i].concessionaria
			            });

			            markers.push(marker);
			            bindInfoWindow(marker, map, infowindow, concessionarie[i]);
				    }


				    function bindInfoWindow(marker, map, infowindow, description) {
					    google.maps.event.addListener(marker, 'click', function() {
					    	var txt = '<b>' + description.concessionaria + '</b><br>' + description.indirizzo  + '<br>' +  description.cap + ' ' +  description.citta;
					    	if(description.capoluogo != description.citta)
					    		txt+= description.capoluogo;
					        
					        txt+=' ('+description.sigla+') <br>';
					        if(description.telefono)
					        	txt+='Telefono: ' + description.telefono + '<br>';
					        if(description.fax)
					        	txt+='Fax: ' + description.fax + '<br>';
					        infowindow.setContent(txt);
					        infowindow.open(map, marker);
					    });
					}

				    map.fitBounds(markers.reduce(function(bounds, marker) {
    					return bounds.extend(marker.getPosition());
					}, new google.maps.LatLngBounds()));


			        google.maps.event.addDomListener(window, 'resize', function() {
			            map.setCenter(centerMap);
			        });

			    }

			});

		</script>

	</body>
</html>