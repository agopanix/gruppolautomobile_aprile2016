<div class="vieniatrovarci useraction ">
	<div id="vieniatrovarci-btn"  class="label-text relative vieniatrovarci-btn">
		<img src="/cdn/images/label-vieni-a-trovarci.png">
	</div>
	<div id="vieniatrovarciarea" class="closed area font-size-13 animate">
		<div class="row">
			<div class="small-12 columns">
				<i class="fa fa-calendar"></i> <span class="font-bold">Vieni a tovarci</span>
				<div class="map" id="vat-map"></div>
				<div class="font-size-11"> <i class="color-9 fa fa-map-marker"></i> Via Enrico Fermi, 27 - 03100 - Frosinone</div>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<hr>
			</div>
			<div class="small-7 columns">
				<div class="font-size-08 color-10">Hai bisogno di un appuntamento specifico?</div>
				<div class="font-size-1 color-10 font-bold">Tel. 0775.8850200</div>
			</div>
			<div class="small-5 columns text-right">
				<a class="button font-size-07" href="<? echo createUrl('frontend/appuntamento/GetIndex')[1]; ?>">Prenota un appuntamento</a>
			</div>
		</div>
	</div>
</div>