<form action="" method="post">
	<div class="row">
		<div class="large-6 columns padding-top-20">
			<div class="font-bold padding-bottom-10">DATI PERSONALI</div>
			<label>
				Nome e Cognome*
				<input type="text" name="nomecognome" required>
			</label>
			<label>
				Indirizzo di Residenza
				<input type="text" name="indirizzoresidenza" >
			</label>
			<label>
				Citt&agrave; di Residenza
				<input type="text" name="cittaresidenza" >
			</label>
			<label>
				Telefono
				<input type="text" name="telefono" >
			</label>
			<label>
				Cellulare
				<input type="text" name="cellulare" >
			</label>
			<label>
				Fax
				<input type="text" name="fax" >
			</label>
			<label>
				Email*
				<input type="text" name="email" required>
			</label>
		</div>
		<div class="large-6 columns padding-top-20">
			<div class="font-bold padding-bottom-10">DATI VETTURA</div>
			<label>
				Marca e Modello*
				<input type="text" name="marcamodello" required>
			</label>
			<label>
				Anno di Immatricolazione*
				<input type="number" name="immatricolazione" required>
			</label>
			<label>
				KM percorsi*
				<input type="number" name="kmpercorsi" required>
			</label>
			<label>
				Targa*
				<input type="number" name="targa" required>
			</label>
			<label>
				Data richiesta per tagliando*
				<input type="date" name="datatagliando" required>
			</label>
			<label>
				Prima Data alternativa per tagliando
				<input type="date" name="datatagliando" required>
			</label>
			<label>
				Seconda Data alternativa  per tagliando*
				<input type="date" name="datatagliando" required>
			</label>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<label>
				Messaggio
				<textarea rows="10"></textarea>
			</label>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns text-right">
			<button type="submit">Invia</button>
		</div>
	</div>
</form>