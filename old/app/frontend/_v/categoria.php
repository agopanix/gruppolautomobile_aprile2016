<div class="row show-for-small-down">
	<div class="small-12 padding-top-10 text-center font-bold font-size-15">
		<?php echo $this->frontend->config["titleHeader"]; ?>
	</div>
</div>
<div class="row margin-top-20">
	<div class="large-9 large-push-3 columns">
		<div class="row collapse">
			<div class="large-6 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-left margin-bottom-10">
				<div class="padding-left-10">RISULTATO RICERCA</div>
			</div>
			<div class="large-6 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-right margin-bottom-10 padding-left-10">
				<i class="fa fa-sort-desc floatright margin-right-10"></i>
				ORDINA PER:
				<select class="oderbyselect font-bold">
					<option value="orderbyprice" class="no-bold">PREZZO DECRESCENTE</option>
					<option value="orderbyprice" class="no-bold">PREZZO CRESCENTE</option>
				</select>
				
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<ul class="category-list">
					<?php foreach ($this->frontend->categorie as $categoria) { ?>
					<li>
						<input type="checkbox"  name="ctagorie_group[]" <?php if($categoria['id']==$this->frontend->config["catsel"]){?>checked<? } ?> value="<?php echo $categoria['id'];?>">
						<?php echo $categoria['name'];?>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
		
		<div class="row">
			<div class="small-12 columns">
				<?php foreach ($this->frontend->risultato_ricerca as $auto) { ?>
				<div class="row car-schedule">
					<div class="medium-4 columns padding-top-10">
						<img
						data-interchange="
						[/timthumb.php?src=<?php echo $auto['img'];?>&h=300&w=460&zc=1, (small)],
						[/timthumb.php?src=<?php echo $auto['img'];?>&w=300&h=300&zc=1, (medium)],
						[/timthumb.php?src=<?php echo $auto['img'];?>&w=300&h=300&zc=1, (large)]"
						src="/timthumb.php?src=<?php echo $auto['img'];?>&w=300px&h=300&zc=1" >
					</div>
					<div class="medium-8 columns  padding-top-10">
						<div class="row primary-informations collapse">
							<div class="small-6 large-3 columns text-center padding-top-05 padding-bottom-05"><?php echo callFunction('frontend','price',$auto['importo'],'€');?></div>
							<div class="small-6 large-3 columns text-center padding-top-05 padding-bottom-05"><?php echo $auto['categoria'];?></div>
							<div class="small-6 large-3 columns text-center padding-top-05 padding-bottom-05"><?php echo $auto['immatricolazione'];?></div>
							<div class="small-6 large-3 columns text-center padding-top-05 padding-bottom-05"><?php echo $auto['cavalli'];?>cv </div>
						</div>
						<div class="row">
							<div class="medium-8 columns padding-top-10">
								<span class="font-bold"><?php echo $auto['marca'];?> <?php echo $auto['modello'];?></span><br>
								<div class="description">
									<span class="font-size-09">
										<?php
											foreach ($auto['accessori'] as $accessorio) {
												echo ' ' . callFunction('frontend','getAccessorio', $accessorio, $this->frontend->accessori) . ';';
											}
										?>
									</span>
								</div>
							</div>
							<div class="medium-4 columns">
								<ul class="font-size-09 padding-top-10">
									<li>Usato garantito</li>
									<li>Alim. <?php echo $auto['alimentazione'];?></li>
								</ul>
							</div>
						</div>

						<div class="row padding-top-08 collapse">
							<div class="small-6 medium-4 columns padding-right-02">
								<a href="<? echo createUrl('frontend/dettaglio/' . $_GET['cat'] .'/'. $auto['id'])[1]; ?>"class="button fullWidth font-size-08 ">VEDI ANNUNCIO</a>
							</div>
							<div class="small-2 medium-1 columns text-center">
								<a class="social-icon button button-gray" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/' . $_GET['cat'] .'/'. $auto['id'] )[1]; ?>"><i class="fa fa-facebook"></i></a>
							</div>
							<div class="small-2 medium-1 columns text-center">
								<a class="social-icon button button-gray" target="_blank" href="https://plus.google.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/' . $_GET['cat'] .'/'. $auto['id'] )[1];  ?>"><i class="fa fa-google-plus"></i></a>
							</div>
							<div class="small-2 medium-1 columns text-center">
								<a class="social-icon button button-gray" target="_blank" href="https://twitter.com/home?status= <?php echo $auto['descrizione']; ?> <?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/' . $_GET['cat'] .'/'. $auto['id'] )[1];  ?>"><i class="fa fa-twitter"></i></a>
							</div>
							<div class="medium-5 columns text-center padding-left-02">
								<a class="button fullWidth button-gray font-size-08"><i class="fa fa-car"></i> AGGIUNGI AI PREFERITI</a>
							</div>
						</div>

					</div>
				</div>
				<?php } ?>
			</div>

			<div class="row collapse padding-10">
				<div class="large-6 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-left margin-bottom-10">
					<div class="padding-left-10">
						RISULTATI VISIBILI
						<select class="oderbyselect quantity font-bold">
							<option value="10" class="no-bold">10</option>
							<option value="20" class="no-bold">20</option>
						</select>
						<i class="fa fa-sort-desc margin-right-10"></i>
					</div>
				</div>
				<div class="large-6 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-right margin-bottom-10">
					<span class="padding-right-10">PAGINA 1 DI <span class="font-bold">20</span></span>
					<span class="font-bold margin-right-10"><i class="fa fa-caret-left"></i>&nbsp;&nbsp;1,2,3,4 .... 1000&nbsp;&nbsp;<i class="fa fa-caret-right"></i> </span>
				</div>
			</div>

		</div>
	</div>
	<div class="large-3 large-pull-9 columns left">
		<form action="#" method="post">
			<div class="row">
				<div class="small-12 columns">
					<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">RICERCA AVANZATA</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Marchio
						<select name="marchio">
							<option value="-">-</option>
						</select>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Modello
						<select name="modello">
							<option value="-">-</option>
						</select>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Alimentazione
						<select name="alimentazione">
							<option value="-">-</option>
						</select>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Colore
						<select name="colore">
							<option value="-">-</option>
						</select>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Da Km percorsi
						<input type="number"name="kmpercorsi">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>A Km percorsi
						<input type="number"name="kmpercorsi">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Da anno immatric. (es.:2006)
						<input type="number" name="daannoimm">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Ad anno immatric. (es.:2006)
						<input type="number" name="adannoimm">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>Da prezzo (senza simbolo € )
						<input type="number"name="daprezzo">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label>A prezzo (senza simbolo € )
						<input type="number"name="daprezzo">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<button class="button fullWidth" type="submit">FILTRA RICERCA</button>
				</div>
			</div>
		</form>
		<hr class="padding-top-10">
		<form action="#" method="post">
			<div class="row">
				<div class="small-12 columns">
					<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">ACCESSORI</div>
				</div>
			</div>
			<?php
			
			foreach ($this->frontend->accessori as $accessorio) { ?>
			<div class="row">
				<div class="small-12 columns">
					<input type="checkbox"  name="accessori_group[]" value="<?php echo $accessorio['id'];?>">
					<?php echo $accessorio['titolo'];?>
				</div>
			</div>
			<? } ?>
			<div class="row">
				<div class="small-12 columns">
					<button class="button fullWidth" type="submit">FILTRA RICERCA</button>
				</div>
			</div>
		</form>
	</div>
</div>