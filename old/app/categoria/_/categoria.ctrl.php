<?php

class categoriaCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		
		foreach ($this->frontend->categorie as $categoria) {
			if($categoria["id"]==$_REQUEST["cat"]){
				$categoria_selezionata = $categoria["name"];
				$testo_categoria = $categoria["text"];
			}
		}


		$config = [
			'title' => 'Gruppo L&lsquo;Automobile',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> $categoria_selezionata,
			'subTitle'		=> $testo_categoria,
			'voceMenu' 		=> 'categoria',
			'header-bkg'	=> 'header-bkg-2.jpg',
			'little-header' => true,
			'header-opaque' => true,
			'breadcrumbs'	=> Array(
				['name'=>'Home','current'=>'', 'url'=>'/'],
				['name'=> $categoria_selezionata,'current'=>'current']
			)
		];

		$catsel = ['catsel'=>$_REQUEST["cat"]];


		
		$this->frontend->_config($config);
		$this->frontend->_config($catsel);
		$this->frontend->header();
		$this->view('frontend/_v/categoria');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

	}

	
}

