<?php

class preferitiCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile - Preferiti',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> 'LISTA PREFERITI',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> 'preferiti',
			'little-header' => true,
			'header-bkg'	=> 'header-bkg-3.jpg',
			'breadcrumbs'	=> Array(
				['name'=>'Lista dei preferiti','current'=>'current'],
			)
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/preferiti');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

	}

	
}

