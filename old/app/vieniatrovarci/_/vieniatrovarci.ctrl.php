<?php

class vieniatrovarciCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile',
			'description' 	=> 'Gruppo L&lsquo;Automobile - Vieni a Trovarci',
			'titleHeader' 	=> 'VIENI A TROVARCI',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> 'vianiatrovarci',
			'header-bkg'	=> 'header-bkg-1.jpg',
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/vieniatrovarci-page');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

	}

	
}

