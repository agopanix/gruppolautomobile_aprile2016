Gyural
======

PHP + MySQL Framework per aspiranti supereroi!  
*(english version of this documentation is under development)*

Impiego queste primissime righe per festeggiare con voi il quarto anno di età di Gyural, le quasi cento installazioni tra progetti personali e siti in produzione e il raggiungimento di una release importante, la prima *forse* che proverà ad aumentare la qualità e stabilità del codice.  

Buona Scrittura.

Federico Quagliotto [email](mailto:f.quagliotto@mandarinoadv.com), [twitter](http://twitter.com/@federicoq)

Prologo
-------

In questo capitolo vedremo brevemente le novità che sono state introdotte con la Versione 1.10, come aggiornare da una versione precedente (almeno 1.6x) e la roadmap per le versoni successive.

### Differenze con la versione 1.9

#### Cambiamenti Generali

- Il routing non è più affidato alla variabile `$_GET["x]` bensì a `$_SERVER["PATH_INFO]`

#### Struttura dei File/Cartelle

La runtime di Gyural non è più unicamente collegata alla cartella `app` ma anche a `core`, teoricamente le UnderscoreAPP in `core` non dovrebbero essere modificate dall'utente.

- core/ (questa cartella è nuova)
- app/

la priorità è affidata ad `app` e successivamente a `core`. (Il meccanismo è collegato con `LoadClass`, `LoadApp`, `CallFunction`)

> **ATTENZIONE** nel caso in cui un'UnderscoreAPP in `app` debba riscrivere una `core` è suggerito utilizzare un altro nome e mappare gli indirizzi nel router.

---

```php 
LoadApp('index', 1)->ApiHello()
``` 

verrà tradotta in

* `/app/index/_/index.ctrl.php` -> method ApiHello()
* `/core/index/_/index.ctrl.php` -> method ApiHello()

le due cartelle: `app` e `core` possono essere modificate nel `version.json`.

```json
"legacy": 0, set this to 1 if you want to use only the /app folder
"userFolder": "app", user customization*
"coreFolder": "core", core apps
```

Puoi continuare ad usare l'asterisco ( * ) come primo carattere nel `LoadClass` e `CallFunction` ma, come di consueto, andrà alla ricerca del file nelle cartelle globali /funcs e /lib.

> **NOTA** Tutte le lib originali (1.9) sono salvate in `/libs/legacy`

----------

#### Vendors / Autoload

A partire dalla **Versione 1.10** è stato aggiunto in Gyural il sistema di autoloading [PSR-4](http://www.php-fig.org/psr/psr-4/) che permette l'utilizzo delle classi PHP senza doverle includere prima dell'utilizzo. Tale meccanismo di autoloading è stato implementato in tutta l'architettura del framework..  
È quindi teoricamente possibile scrivere

```
$utenti = new users()->filter();
// invece di
$utenti = LoadClass('users', 1)->filter();
```

E lo stesso discorso può essere esteso ai **controllers**:
```
$utenti = new usersCtrl();
```

> Resta, in ogni caso, consigliabile mantenere l'utilizzo di `LoadClass()` e `LoadApp()` in quanto più performante.

I **vendors**, sono locati nella cartella: `/vendors` e per essere utilizzati devono sfruttare il meccanimo di `namespacing` caratteristico del [PHP](http://php.net/manual/en/language.namespaces.php)

Gyural viene distribuito con già una serie di `Vendors` pre-installati.  
Per maggiori informazioni su come creare/manutenere e gestire i vendors [vai al capitolo di riferimento](#gyural-le-basi-vendors)

### Aggiornare dalla 1.6+

Teoricamente la versione 1.10 mantiene la retrocompatibilità con tutte le funzionalità e sintassi previste dalla 1.x *(per questo motivo, nonostante la maturità non si è scelto di chiamarla 2.0)*  

Detto questo, potrebbe esserti sufficiente backuppare le cartelle `/app`, `/libs`, `/funcs/third` (e le eventuali `/upl`, `/cdn` e tutte quelle cartelle non di sistema che hai creato) e i file `sys/version.json` e `router.php`. Nel ripristinarle nella nuova installazione di Gyural 1.10 segui queste semplici regole

| file/cartelle     | azione                |
| ----------------- | --------------------- |
| /app              | sovrascrivi           |
| /libs             | **non sovrascrivere** |
| /funcs/third      | **non sovrascrivere** |
| router.php        | sovrascrivi           |
| /sys/version.json | sovrascrivi + editing |

A questo punto, devi modificare il `sys/version.json` aggiungendo le righe:

```json
"userFolder": "app",
"coreFolder": "core",
```

Adesso Gyural dovrebbe essere pronto per eseguire la tua applicazione.

La procedura consigliata, una volta ripristinata l'installazione, è quella di andare a rimuovere dalla cartella `/app` quelle applicazioni che sono state spostate all'interno di `/core`, facendo attenzione a rimuovere **solo** quelle che non sono state modificate.(tipicamente: gyu_sdk, gyu_optimization, gyu_bucket, users *ci sono delle note a fine paragrafo*)

Qualora, invece, Gyural non fosse ingrado di eseguire la tua applicazione puoi procedere aggiungendo, al `sys/version.json` la riga:

```json
"legacy": 1
```

Se nemmeno in questo modo riesci ad eseguire Gyural è possibile che te abbia effettuato delle modifiche più profonde al Framework e quindi per te l'aggiornamento alla versione 1.10 non è possibile.. I'm sorry :(

---
1: Gyu Optimization è stata rimossa dall'installazione base. Rimane disponibile nella Repository  
2: Gyu Bucket è stato rimosso e non è più distribuito

### Come Contribuire

Per contribuire alla `documentazione`, o alla `codebase` ti è sufficiente forkare la repository.

### Documentazione API

Se cerchi la documentazione delle singole funzioni/librerie del framework le puoi trovare all'indirizzo [https://bitbucket.org/federicoq/gyural/src](https://bitbucket.org/federicoq/gyural/src) navigando all'interno della repository

> Se non hai accesso alla repository, [contattami](mailto:f.quagliotto@mandarinoadv.com) e provvederò a fornirti l'accesso! :)

### Roadmap

Dal punto di vista del ciclo di vita del prodotto, la validità della versione è di 1 anno (Settembre 2016). Durante questo periodo non sono previsti nuovi rilasci **major**, ma solo dei **minor** di bug fixing.  
In teoria, per mantenere fede a quanto ipotizzato anni fa, non vorrei rilasciare mai una versione 2.0 o comunque perdere **totalmente** la retrocompatibilità con la versione 1.x

Let's talk about

Installazione
-------------

È possibile installare Gyural 1.10 in due modi:

- Tramite `/setup` (è la modalità standard)
- Manualmente, modificando il `version.json`

L'installazione manuale è consigliata per tutte quelle applicazioni che richiedono una struttura custom.

### Requisiti di Sistema

- PHP >= 5.4
- MySQL >= 5 (se si intende usare il database)
- JSON Extension
- Apache2 / Lite-speed
- mod_rewrite
- Basepath = ‘/’
- Short Tag <? attivi
- Love

### Installazione

Prima di procedere con l'installazione è consigliato controllare:

1. Il file `.htaccess` sia stato caricato correttamente nella root.
1. Si disponga dei permessi di scrittura per le cartelle
    - /upl
    - /cdn/cache
1. Che sia presente la cartella `cdn/cache/sys` e sia scrivibile
1. Che non sia presente il file `cdn/cache/sys/hooks.cache`

Oltre chiaramente ai requisiti richiesti per procedere con l'installazione.

> Ad oggi, non è possibile installare ed eseguire correttamente gyural su Microsoft IIS ®. È invece possibile eseguirlo su Apache2 su macchina windows.

#### version.json

Tramite il `version.json` è possibile gestire tutte le configurazioni dell'installazione.

```
{
    "active":"working",
    "working":{
        "ErrorApp":"error",
        "IndexApp":"index/index",
        "legacy":0,
        "userFolder":"app",
        "coreFolder":"core",
        "siteName":"Gyural 1.10",
        "uri":"http://110.gyu.space",
        "uriLogin":"users/GetLogin",
        "uploadPath":"upl/",
        "mysql":"mysql://username:password@localhost/dbname",
        "sendmail":"php",
        "tablesPrefix":"",
        "supportMail":"federico.quagliotto@gmail.com",
        "mail":"federico.quagliotto@gmail.com",
        "ssl":"0",
        "dev":"1",
        "logStack":"0",
        "db":1,
        "version":"1.10",
        "online":1,
        "cdn":"/cdn/"
    }
}
```

Le Basi
-------

In questo capitolo analizzeremo brevemente i meccanismi di funzionamento di Gyural. Non entreremo *mai* troppo specificatamente nei dettagli ma eseguiremo una panoramica generale su come funziona il framework e quali strumenti sono disponibili `out of the box`.

> La natura così semplice e minimalista del framework, in ogni caso, lascia piena possibilità allo sviluppatore di modificarne il funzionamento o di personalizzarlo nel comportamento.

### Tipi di Richieste

Gyural è ingrado di gestire le richieste in due modi:

1. Tramite Controller nelle `UnderscoreAPP`
1. Mediante PHP procedurale `APP` (il nome deriva dall'abbreviazione di `Application()` la funzione che inizialmente li richiamava)

Nel caso in cui si utilizzino i **controller** le tipologie di richiesta devono essere dichiarate nel nome del metodo, nel caso in cui si utilizzi, invece il **procedurale** è necessario aprire il file con una serie di funzioni che ne caratterizzano il metodo.
> Nel caso del `procedurale` se non è specificata la funzione che determina il metodo, questi potrà essere eseguito in ogni caso.

| metodo | gestita da | funzione procedurale |
| ------ | ----------- | ---- |
| Get | `Get`, `Ctrl` e .php procedurali | `MethodStandard()` |
| Post | `Post`, `Ctrl` e .php procedurali | `MethodStandard()` |
| Ajax | `Api`, `Ajax`, `Ctrl` e .php procedurali | `MethodAjax()` |
| Api | `Api` e il routing avviene per /api/controller.metodo (non può essere riscritto dal `Router`) | x |
| Cli | `Cli` | x |

Eventuali altre gestioni di richieste che si vogliono aggiungere/personalizzare sono presenti nella funzione `MethodDetect` di `methods.php`

> È nelle prossime release **minor** la possibilità di aggiungere un'hook per poter iniettare nuovi metodi.

### Routing

Il sistema di routing è piuttosto semplice. A partire dall'`URL` Gyural spacchetta il percorso con il separatore `/` e controlla all'interno di ogni singola porzione se queste hanno un corrispettivo funzionale Gyural.  

> Per esempio, le variabili `$_GET` possono essere passate sia nella modalità canonica: `?varName=content` sia nella notazione **colon** `varName:content`.

Dopo aver bonificato l'`URL`, procede alla determinazione del candidato migliore per eseguire la richiesta.

Prendiamo un esempio: `http://www.example.com/var1:1/news/var2:2/detail/id:1`  
Dopo la bonifica avremo:

- **Richiesta:** `/news/detail`
- **Variabili:** `$_GET["var1"] = 1`; `$_GET["var2] = 2`; `$_GET["id] = 1`

*questo è lo stack di controlli che effettua per determinare che metodo/controller/App invocare*

1. esiste una regola di `Routing` per la chiamata?
1. esiste una `SubUnderscoreAPP` chiamata **news_detail**?
1. esiste una `UnderscoreAPP` chiamata **news**? ed ha un metodo chiamato **detail**?
1. esiste una `UnderscoreAPP` chiamata **news**? e il **controller** ha la proprietà $index_tollerant? chiamo l'`Index`.
1. esiste una `UnderscoreAPP` chiamata **news**? e esiste un'APP chiamata: `/news/detail.php`?
1. esiste il file `news/detail.php`?
1. niente, è tempo di chiamare l'errore.. :)

A partire da Gyural 1.8 è possibile riscrivere, internamente, le url mediante l'utilizzo del file `router.php` o agganciandosi all'evento `gyu.router.append`.

Il file `/router.php`, è richiamato in maniera `lineare` all'interno di Gyural, e può essere utilizzato anche per dichiarare funzioni custom.

---

Il metodo con il quali è possibile riscrivere un'url è `map()`

`map($match, $direction, $code, $filters);`
```
 * Rewrite an url to a determinate Controller/Page
 * 
 * @param  string $match Pattern to match
 * @param  string $direction Rewrite rule
 * @param  integer $code HttpCode Response
 * @param  array $filters Filters to attach to the execution (gyuFilters @ standardController)
 * @return false
```

*esempio*
```
$router->map('hi/#', 'index/test/name:#'); 
// http://www.example.com/hello/federico => http://www.example.com/index/test/name:federico

$router->map('is_gyural', function() {
    echo 'yes it is.';
    echo 'v. '. version . '<br /><br />';
    echo '<a href="http://www.mandarinoadv.com">http://www.mandarinoadv.com</a><br />';
    echo '<a href="http://www.gyural.com">http://www.gyural.com</a>';
});
```

Dall'esempio si nota come è possibile passare una callback come $direction che verrà eseguita all'esecuzione della richiesta.

### Vendors

La cartella `/vendors` segue lo standard PSR-4 dell'autoloading delle classi `PHP`:

I file all'interno della cartella devono seguire la sintassi:  

`NomeDelVendor/NomeDellaLib.php`

e il file dovrà specificare il namespace e dovrà essere conforme al nome.

```
namespace NomeDelVendor;

class NomeDellaLib {
    
}
```

e, quando servirà utilizzarla bisognerà specificarne il percorso:

```
$classe = new \NomeDelVendor\NomeDellaLib();
```

questo è chiaramente reiterabile all'infinito:

```
new \NomeDelVendor\NomeDellaSubVendor\NomeAltroSub\Etc\Yay\Hooray();

=> /vendors/NomeDelVendor/NomeDellaSubVendor/NomeAltroSub/Etc/Yay/Hooray.php

```

Di default `Gyural` è distribuito con le classi Vendor: `\Gyu` (Cache, Cli, Config, Form, Hooks, Upload, Validation)

> Gli Hooks, nonostante siano stati spostati all'interno dei `Vendors` possono ancora essere invocati da `CallFunction('hooks, 'get/set');`

## Underscore

**Underscore**, **UnderscoreAPP**, o genericamente **/_/** è la metodologia con la quale è possibile pacchettizare porzioni di codice e l'unico modo per esporre punti d'accesso all'applicazione. 

*(in questa porzione di documentazione utilizzeremo la parola `APP` per intendere un'`Underscore`)*

Affinché un'`APP` sia valida è necessario che: 

- esista la cartella all'interno di `/app/` o `/core/` con il *nome* dell'`APP`
- esista la cartella `_` all'interno della cartella dell'`APP`
- esista la cartella `_v` all'interno della cartella dell'`APP` (se si vuole utilizzare il nuovo metodo `$this->view(`) nei controller)

Ogni `APP` può, contenere all'interno della cartella `/_/`:

- Controllers (*.ctrl.php)
- Libs (*.lib.php)
- Hooks (*.hooks.php)
- Funcs (*.funcs.php)

Ogni `APP` può essere caratterizzata con altre UnderscoreAPP che dipendono (più o meno) strettamente dalla `APP` principale.  
Tali UnderscoreAPP le chiamiamo **SubUnderscoreAPP** e nella nomenclatura dei file devono rispettare la sintassi: `appgenitore_subapp.`

```
UnderscoreAPP: *news*

/app
    /news
        /_
            /news.ctrl.php
            /news.lib.php
            /news.hooks.php
            /news.funcs.php
            /news_categories.ctrl.php
            /news_categories.lib.php
        /_v
```

Fuori dal contesto `/_/` può essere presente la cartella `/_v/` che si lega al meccanismo `$this->view()` all'interno del controller.

### Controllers

I **Controller** sono da preferirsi al vecchio metodo di interfacciamento (orientato al procedurale, e utilizzato mediante la funzione `Application()`). 

I Controller devono (e possono) essere presenti all'interno della cartella `Underscore` di riferimento e chiamarsi rispettando la sintassi: `appname.ctrl.php`

> Tieni a mente che, eventuali **SubUnderscoreAPP** dovranno rispettare la sintassi `appname_controller.ctrl.php`

Ogni controller, per essere tale, deve estendere la classe **standardController** e chiamarsi **appnameCtrl**

> **NOTA** Non è *indispensabile* che la classe estenda la *standardController* ma è necessario che abbia i metodi: `__haveControllerAutotest`, `__className`, `__haveController`, `__exec` se vuoi maggiori informazioni su come scrivere dei meccanimi di Controlling diversi [contattami](mailto:f.quagliotto@mandarinoadv.com) o ispeziona `/libs/standardController.lib.php`

```
/app/news/_/news.ctrl.php

class newsCtrl extends standardController {

}
```
```
/app/news/_/news_categories.ctrl.php

class news_categoriesCtrl extends standardController {

}
```

Per quanto riguarda il comportamento del **Controller** è possibile cambiare i valori di default della *standardController*, nello specifico:

```
/**
  * If there's no candidate for the request, use the index?
  * (false, by default)
  * @var boolean
  */
  var $index_tollerant = false;
    
/**
  * Mantain the move params after the move?
  * (false by default)
  * @var null
  */
  var $gyu_preserveMove = false;
```

---

Di default, i metodi presenti all'interno dei **Controller**, per essere eseguiti dall'esterno (tipicamente da un browser) devono specificare nella dichiarazione il metodo d'accesso:

| metodo | descrizione | esempio |
| ------ | ----------- | ------- |
| Ctrl | Questo è il modo più astratto per dichiarare un metodo e risponderà a qualunque tipologia di chiamata | `CtrlIndex()` |
| Get | Questo metodo risponderà solamente alle richieste pervenute tramite `GET` | `GetIndex()` |
| Post | Questi metodi risponderanno solo a chiamate `POST` | `PostIndex()` |
| Ajax | Questi metodi risponderanno solo a chiamate `xmlhttprequest` sia che provengano da `GET` sia da `POST` | `AjaxIndex()` |
| Api | Questo metodo non risponde a `HEAD` specifici, ma viene routato da `/api/controller.metodo` | `ApiIndex()` |
| Cli | Questo metodo non risponde a `HEAD` specifici, ma può essere eseguito solamente da `CLI` | `CliIndex()` |

> **NOTA** Solo i metodi `Ctrl` sono ingrado di rispondere in alternativa al metodo specifico.

---

A partire dalla versione *1.10* è possibile dichiarare nel metodo anche gli attributi che ci aspettiamo di ricevere. Tali attributi verranno processati a partire dalla `url` escludendo: 

- Le variabili `varName:content`
- Il nome del controller
- Il nome del metodo

```
public function GetSingle($id, $titolo) {
    echo $id;
    echo $titolo;
}

=> http://www.example.com/controllerName/single/varName:test/2/test:ciao/titolo+della+notizia
=> Stamperà:
"2"
"titolo della notizia"
```

> Per poter utilizzare la dichiarazione degli attributi nel metodo, è necessario che il controller abbia l'attributo `$index_tollerant` impostato a true.

#### Redirezionare la navigazione — `move()`
*Supporto: Gyural 1.10*

`$this->move(where, with = false)`

Il metodo **Move** consente di spostare la navigazione dell'utente all'interno dello stesso *Environment* con la possibilità di inviare delle informazioni al metodo ricevente.

```
 * Move the request to another Controller Action
 * 
 * @param  string $to The location
 * @param  array Attributes to pass to $to (retrived by $this->)
 * @param  integer $code HTTP response code
 * @return false
```

*example*
```php
$this->move('index/GetIndex', array('name' => 'fed');
…
function GetIndex() {
    echo $this->name;
}

// Will output "fed"

```

#### Caricare una vista — `view()`
*Supporto: Gyural 1.10*

`$this->view(view, app_data = null)`

Mediante il metodo `->view` è possibile caricare (dal controller) una vista.  
Questo metodo, introdotto dalla 1.10, sostituisce per le vista la funzione `Application()`

```
 * Load a specific view
 * 
 * @param  string $view Filename of the view.
 * @param  mixed  $app_data attributes to cross between method and view. (note that $this still be available)
 * @return [type]
```

È da tenere presente che, all'interno della vista sarà possibile richiamare `$this` e ottenere l'intero **Oggetto Controllore** e, contestualmente, è possibile continuare ad utilizzare `$app_data` come nelle versioni precedenti.

#### Ottenere il nome dell'app — `__className()`
*Supporto: Gyural 1.7+*

```
 * Retrive the name of the Controller (without Ctrl suffix)
 * 
 * @return string
```

#### Ottenere la path (URI) dell'app — `_base()`
*Supporto: Gyural 1.10*

```
 * Retrive the url-parsed of the current controller
 * 
 * @param  boolean $pre If true the trailing slashe is prepend to the output.
 * @return string
```

#### Filters — `gyuFilter()`
*Supporto: Gyural 1.10*

I Filtri sono dei middleware che possono essere iniettati, all'*inizio* o alla *fine* della chiamata, ad un controller.
Possono essere utilizzati, per esempio: per validare un form, verificare la possibilità di accedere ad una determinata area/pagina dell'applicazione, o per pre-fetchare il contenuto che poi dovrà essere utilizzato dal controllore.

> Puoi vedere un paio di esempi astratti su `dev/filters` o un esempio di applicazione complessa analizzando il codice di `core/users/_/users.ctrl.php`

Tecnicamente, l'implementazione dei Filtri consiste nel popolamento di due attributi nel **controller**:

- (array) `gyu_filterBefore`
- (array) `gyu_filterAfter` 

Rispettivamente vengono eseguiti *prima (before)* e/o *dopo (after)*  

I filtri possono essere agganciati al **metodo** di un **Controller** in due modi:

- Nel costruttore `__construct``
- Nel `Router.php`

> **ATTENZIONE** Affinché un filtro possa essere definito **passed** dovrà restituire *false*. Generalmente risultati diversi da **false** serviranno per creare messaggi d'errore o informazioni per gestire al meglio l'esecuzione. 

##### Agganciare un filtro dal `__construct`

`$this->gyuFilter(type, what, stop, redirect, restrict);`

```
 * Add a Filter to a Controller Action
 * 
 * @param  string $type Before or After
 * @param  mixed $what Action to perfor to determine if the filter is true or false
 * @param  integer $stop If 1/true if the filter fail stop the execution
 * @param  string $redirect If stop = 1, move to $redirect
 * @param  array $strict Array of methods that use that filter.
 * @return [type]
```

*example 1* - Validazione di un Form
```php
function __construct() {
    $this->gyuFilter('before', 'news.formValidate', 1, 'index/GetIndex', array('PostNew', 'PostEdit'));
}
```

*example 2* - Pre-Fetchare `$_GET["news_id"]` come oggetto `news`

```php
function __construct() {
    $this->gyuFilter('before', function() {
        if(isset($_GET["news_id"]))
            $this->news = LoadClass('news', 1)->get($_GET["news_id"]);

        return false; // this will never block the execution... set to true or give an array, and pass the *stop* attr.
    });
}
```

##### Agganciare un filtro dal `Router`.

In base alla strutturazione del `router.php` e di `libs/router.lib.php` i metodi `map` e `b_map` accettano un quarto attributo che rappresenta il vettore del **gyuFilter**.

`array(type, what, stop, redirect)`

E quindi, nel pratico:

```
$router->map('hi/#', '/index/hello/name:#', null, ['before', 'index.doSomething', 1, 'error']);
```

Anche qui, **what** può essere una callback.

> Nel caso in cui avessi la necessità di agganciare più di un filtro tramite il router è suggeribile utilizzare una callback.. che a sua volta può aggiungere dei filtri $this->gyuFilter...

### Libs

Le Libs (Libraries) sono lo strato dedicato alla manipolazione dei dati (probabilmente collegato con un database).

Le **libs** possono essere richiamate mediante la funzione `LoadClass('className', 1);` oppure (in Gyural 1.10) semplicemente `new className()`

Normalmente, in Gyural, le libs estendo la **standardObject** che è la libreria di comunicazione con il Database MySQL.

#### standardObject

Per maggiori informazioni sulla `standardObject` puoi ispezionare: `/libs/standardObject.lib.php`

##### Attributi

```
 * Name of the table. prepend the string with * to ignore the prefix
 * @type {String}
var $gyu_table = '';

 * PRIMARY KEY
 * @var string $gyu_id;
var $gyu_id = 'id';
```

##### Metodi Astratti

```
 * Get Attribute
 * if exists, execute the get$name method, otherwise return the attribute value
 * 
 * @param  string $name The name of the attribute (column name)
 * @param  mixed $args List of arguments
 * @return mixed
public function getAttr($name, $args = null)

 * Set Attribute
 * like the getter, set$name is the first try
 * 
 * @param string $key The name of the attribute (column name)
 * @param mixed $vValue The value for the attribute
 * @return mixed
public function setAttr($key, $vValue)

 * This works like: getAttr(), render() binded to render$name
 * 
 * @param  string $name The name of the attribute (column name)
 * @param  mixed $args List of Arguments
 * @return mixed
public function verb($name, $args = null)

 * This works like: ->getAttr, ->verb binded to render$name
 * 
 * @param  string $name The name of the attribute (column name)
 * @param  mixed $args List of arguments
 * @return mixed
public function render($name, $args)

 * Check if an Attribute is null
 * 
 * @param  string $what The name of attribute (column name)
 * @return boolean
public function notNull($what)

 * Refill an object with an associative array.
 * 
 * @param  mixed $vArray associative array
 * @return [type]
public function refill($vArray = null)
```

##### Database (mySQL)

```
 * Database Layer (HELPER)
 * Helper for SELECT queries
 * conditions/option passed as function arguments
 * 
 * @param mixed
 * @return object
public function filter()

 * Database Layer (HELPER)
 * Helper for SELECT queries
 * conditions/option passed as array
 * 
 * @param  mixed $args List of arguments
 * @return [type]
public function filter_array($args)

 * Database Layer
 * Helper for SELECT queries
 * 
 * @param  mixed $args List of arguments
 * @return mixed object or array of objects
public function filterExecute($args)

 * Database Layer
 * Get the object identified by the primary_key field
 * 
 * @param  mixed $index Value of the primary key to retrive
 * @param  array $constructArgs List of arguments for __construct of the class
 * @return [type]
public function get($index, $constructArgs = null)

 * Database Layer
 * Helper for INSERT queries (NOT EXECUTION)
 * 
 * @param  array $vArray List of Attributes (if !null ->refill())
 * @return string
public function hang($vArray = null)

 * Database Layer
 * Helper for INSERT queries (EXECUTION)
 * 
 * @param  array $vArray List of Attributes
 * @return mixed the last-insert id (note that $this also contain the whole object)
public function hangExecute($vArray = null)

 * Database Layer
 * Helper for UPDATE queries (NOT EXECUTION)
 * 
 * @param  array $vArray List of Attributes (if !null ->refill())
 * @param  mixed $id The identifier (if null $this->gyu_id)
 * @return string
public function put($vArray = null, $id = null)

 * Database Layer
 * Helper for UPDATE queries (EXECUTION)
 * 
 * @param  array $vArray List of attributes (if !null ->refill())
 * @param  mixed $id The identifier (if null $this->gyu_id)
 * @return resource (note that $this still contain the whole object)
public function putExecute($vArray = null, $id = null)

 * Database Layer
 * Helper for DELETE queries (NOT EXECUTION)
 * 
 * @param  mixed $id The identifier (if null $this->gyu_id)
 * @return string
public function delete($id = null)

 * Database Layer
 * Helper for DELETE queries (EXECUTION)
 * 
 * @param  mixed $id The identifier
 * @return resource ($this is still active)
public function deleteExecute($id = null)
```

##### Database Relationship
*Supporto: Gyural 1.10*
```
 * Database Layer, Relation Manager
 * $this to One.
 * ->ROneObjectName
 * 
 * @param  string $name Name of object
 * @param  mixed $var Configuration for override the gyu_id values
 * @return object
function toOne($name, $var = false)

 * Database Layer, Relation Manager
 * $this to Many.
 * ->RManyObjectName
 * 
 * @param  string $name Name of Object
 * @param  mixed $var Configuration of override the gyu_id values
 * @param  array $args List of conditions/option 
 * @return array
function toMany($name, $var = false, $args = false)
```

### Functions

### Hooks