<?php

/**
 * Gyural - Form Helper
 *
 * Based on CodeIgniter Form Helpers!
 *
 * @version 0.1
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

/**
 * CodeIgniter Form Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/form_helper.html
 */

// ------------------------------------------------------------------------

namespace Gyu;

class Form {

	static function open($action = '', $attributes = array(), $hidden = array()) {

		// If no action is provided then set to the current url
		if (!$action) {
			$action = "";
		}
		
		$attributes = static::_attributes_to_string($attributes);

		if (stripos($attributes, 'method=') === FALSE) {
			$attributes .= ' method="post"';
		}

		$form = '<form action="'.$action.'"'.$attributes.">\n";

		if (is_array($hidden)) {
			foreach ($hidden as $name => $value) {
				$form .= '<input type="hidden" name="'.$name.'" value="'.$value.'" style="display:none;" />'."\n";
			}
		}

		return $form;

	}

	static function open_multipart($action = '', $attributes = array(), $hidden = array()) {

		if (is_string($attributes)) {
			$attributes .= ' enctype="multipart/form-data"';
		}
		else {
			$attributes['enctype'] = 'multipart/form-data';
		}

		return static::open($action, $attributes, $hidden);

	}

	/**
	 * Hidden Input Field
	 *
	 * Generates hidden fields. You can pass a simple key/value string or
	 * an associative array with multiple values.
	 *
	 * @param	mixed	$name		Field name
	 * @param	string	$value		Field value
	 * @param	bool	$recursing
	 * @return	string
	 */
	static function hidden($name, $value = '', $recursing = FALSE) {

		static $form;

		if ($recursing === FALSE) {
			$form = "\n";
		}

		if (is_array($name)) {
			foreach ($name as $key => $val) {
				static::hidden($key, $val, TRUE);
			}

			return $form;
		}

		if (!is_array($value)) {
			$form .= '<input type="hidden" name="'.$name.'" value="'.$value."\" />\n";
		} else {
			foreach ($value as $k => $v) {
				$k = is_int($k) ? '' : $k;
				static::hidden($name.'['.$k.']', $v, TRUE);
			}
		}

		return $form;
	}

	/**
	 * Text Input Field
	 *
	 * @param	mixed
	 * @param	string
	 * @param	mixed
	 * @return	string
	 */
	static function input($data = '', $value = '', $extra = '') {
		$defaults = array(
			'type' => 'text',
			'name' => is_array($data) ? '' : $data,
			'value' => $value
		);

		return '<input '.static::_parse_form_attributes($data, $defaults).static::_attributes_to_string($extra)." />\n";
	}

	/**
	 * Password Field
	 *
	 * Identical to the input function but adds the "password" type
	 *
	 * @param	mixed
	 * @param	string
	 * @param	mixed
	 * @return	string
	 */
	static function password($data = '', $value = '', $extra = '') {
		is_array($data) OR $data = array('name' => $data);
		$data['type'] = 'password';
		return static::input($data, $value, $extra);
	}

	/**
	 * Upload Field
	 *
	 * Identical to the input function but adds the "file" type
	 *
	 * @param	mixed
	 * @param	string
	 * @param	mixed
	 * @return	string
	 */
	static function upload($data = '', $value = '', $extra = '') {
		$defaults = array('type' => 'file', 'name' => '');
		is_array($data) OR $data = array('name' => $data);
		$data['type'] = 'file';

		return '<input '.static::_parse_form_attributes($data, $defaults).static::_attributes_to_string($extra)." />\n";
	}

	/**
	 * Textarea field
	 *
	 * @param	mixed	$data
	 * @param	string	$value
	 * @param	mixed	$extra
	 * @return	string
	 */
	static function textarea($data = '', $value = '', $extra = '') {
		$defaults = array(
			'name' => is_array($data) ? '' : $data,
			'cols' => '40',
			'rows' => '10'
		);

		if ( ! is_array($data) OR ! isset($data['value']))
		{
			$val = $value;
		}
		else
		{
			$val = $data['value'];
			unset($data['value']); // textareas don't use the value attribute
		}

		return '<textarea '.static::_parse_form_attributes($data, $defaults).static::_attributes_to_string($extra).'>'
			.$val
			."</textarea>\n";
	}

	/**
	 * Multi-select menu
	 *
	 * @param	string
	 * @param	array
	 * @param	mixed
	 * @param	mixed
	 * @return	string
	 */
	static function multiselect($name = '', $options = array(), $selected = array(), $extra = '') {

		$extra = static::_attributes_to_string($extra);
		if (stripos($extra, 'multiple') === FALSE) {
			$extra .= ' multiple="multiple"';
		}

		return static::dropdown($name, $options, $selected, $extra);

	}

	/**
	 * Drop-down Menu
	 *
	 * @param	mixed	$data
	 * @param	mixed	$options
	 * @param	mixed	$selected
	 * @param	mixed	$extra
	 * @return	string
	 */
	static function dropdown($data = '', $options = array(), $selected = array(), $extra = '') {
		$defaults = array();

		if (is_array($data)) {
			if (isset($data['selected'])) {
				$selected = $data['selected'];
				unset($data['selected']); // select tags don't have a selected attribute
			}

			if (isset($data['options'])) {
				$options = $data['options'];
				unset($data['options']); // select tags don't use an options attribute
			}
		}
		else {
			$defaults = array('name' => $data);
		}

		is_array($selected) OR $selected = array($selected);
		is_array($options) OR $options = array($options);

		// If no selected state was submitted we will attempt to set it automatically
		if (empty($selected)) {
			if (is_array($data)) {
				if (isset($data['name'], $_POST[$data['name']])) {
					$selected = array($_POST[$data['name']]);
				}
			}
			elseif (isset($_POST[$data])) {
				$selected = array($_POST[$data]);
			}
		}

		$extra = static::_attributes_to_string($extra);

		$multiple = (count($selected) > 1 && stripos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

		$form = '<select '.rtrim(static::_parse_form_attributes($data, $defaults)).$extra.$multiple.">\n";

		foreach ($options as $key => $val) {
			$key = (string) $key;

			if (is_array($val)) {
				if (empty($val)) {
					continue;
				}

				$form .= '<optgroup label="'.$key."\">\n";

				foreach ($val as $optgroup_key => $optgroup_val) {
					$sel = in_array($optgroup_key, $selected) ? ' selected="selected"' : '';
					$form .= '<option value="'.$optgroup_key.'"'.$sel.'>'
						.(string) $optgroup_val."</option>\n";
				}

				$form .= "</optgroup>\n";
			} else {
				$form .= '<option value="'.$key.'"'
					.(in_array($key, $selected) ? ' selected="selected"' : '').'>'
					.(string) $val."</option>\n";
			}
		}

		return $form."</select>\n";

	}

	/**
	 * Checkbox Field
	 *
	 * @param	mixed
	 * @param	string
	 * @param	bool
	 * @param	mixed
	 * @return	string
	 */
	static function checkbox($data = '', $value = '', $checked = FALSE, $extra = '') {
		$defaults = array('type' => 'checkbox', 'name' => ( ! is_array($data) ? $data : ''), 'value' => $value);

		if (is_array($data) && array_key_exists('checked', $data)) {
			$checked = $data['checked'];

			if ($checked == FALSE) {
				unset($data['checked']);
			} else {
				$data['checked'] = 'checked';
			}
		}

		if ($checked == TRUE) {
			$defaults['checked'] = 'checked';
		} else {
			unset($defaults['checked']);
		}

		return '<input '.static::_parse_form_attributes($data, $defaults).static::_attributes_to_string($extra)." />\n";
	}

	/**
	 * Radio Button
	 *
	 * @param	mixed
	 * @param	string
	 * @param	bool
	 * @param	mixed
	 * @return	string
	 */
	static function radio($data = '', $value = '', $checked = FALSE, $extra = '') {
		is_array($data) OR $data = array('name' => $data);
		$data['type'] = 'radio';

		return static::checkbox($data, $value, $checked, $extra);
	}

	/**
	 * Submit Button
	 *
	 * @param	mixed
	 * @param	string
	 * @param	mixed
	 * @return	string
	 */
	static function submit($data = '', $value = '', $extra = '') {
		$defaults = array(
			'type' => 'submit',
			'name' => is_array($data) ? '' : $data,
			'value' => $value
		);

		return '<input '.static::_parse_form_attributes($data, $defaults).static::_attributes_to_string($extra)." />\n";
	}

	/**
	 * Reset Button
	 *
	 * @param	mixed
	 * @param	string
	 * @param	mixed
	 * @return	string
	 */
	static function reset($data = '', $value = '', $extra = '') {

		$defaults = array(
			'type' => 'reset',
			'name' => is_array($data) ? '' : $data,
			'value' => $value
		);

		return '<input '.static::_parse_form_attributes($data, $defaults).static::_attributes_to_string($extra)." />\n";

	}

	/**
	 * Form Button
	 *
	 * @param	mixed
	 * @param	string
	 * @param	mixed
	 * @return	string
	 */
	static function button($data = '', $content = '', $extra = '') {

		$defaults = array(
			'name' => is_array($data) ? '' : $data,
			'type' => 'button'
		);

		if (is_array($data) && isset($data['content'])) {
			$content = $data['content'];
			unset($data['content']); // content is not an attribute
		}

		return '<button '.static::_parse_form_attributes($data, $defaults).static::_attributes_to_string($extra).'>'
			.$content
			."</button>\n";

	}

	/**
	 * Form Label Tag
	 *
	 * @param	string	The text to appear onscreen
	 * @param	string	The id the label applies to
	 * @param	string	Additional attributes
	 * @return	string
	 */
	static function label($label_text = '', $id = '', $attributes = array()) {

		$label = '<label';

		if ($id !== '') {
			$label .= ' for="'.$id.'"';
		}

		if (is_array($attributes) && count($attributes) > 0) {
			foreach ($attributes as $key => $val) {
				$label .= ' '.$key.'="'.$val.'"';
			}
		}

		return $label.'>'.$label_text.'</label>';

	}

	/**
	 * Fieldset Tag
	 *
	 * Used to produce <fieldset><legend>text</legend>.  To close fieldset
	 * use form_fieldset_close()
	 *
	 * @param	string	The legend text
	 * @param	array	Additional attributes
	 * @return	string
	 */
	static function fieldset($legend_text = '', $attributes = array()) {

		$fieldset = '<fieldset'.static::_attributes_to_string($attributes).">\n";
		if ($legend_text !== '') {
			return $fieldset.'<legend>'.$legend_text."</legend>\n";
		}

		return $fieldset;

	}

	/**
	 * Fieldset Close Tag
	 *
	 * @param	string
	 * @return	string
	 */
	static function fieldset_close($extra = '') {

		return '</fieldset>'.$extra;

	}

	/**
	 * Form Close Tag
	 *
	 * @param	string
	 * @return	string
	 */
	static function close($extra = '') {

		return '</form>'.$extra;

	}

	/**
	 * Parse the form attributes
	 *
	 * Helper function used by some of the form helpers
	 *
	 * @param	array	$attributes	List of attributes
	 * @param	array	$default	Default values
	 * @return	string
	 */
	static function _parse_form_attributes($attributes, $default) {

		if (is_array($attributes)) {
			foreach ($default as $key => $val) {
				if (isset($attributes[$key])) {
					$default[$key] = $attributes[$key];
					unset($attributes[$key]);
				}
			}

			if (count($attributes) > 0) {
				$default = array_merge($default, $attributes);
			}
		}

		$att = '';

		foreach ($default as $key => $val) {
			if ($key === 'value') {
				$val = $val;
			}
			elseif ($key === 'name' && ! strlen($default['name'])) {
				continue;
			}

			$att .= $key.'="'.$val.'" ';
		}

		return $att;
	}

	/**
	 * Attributes To String
	 *
	 * Helper function used by some of the form helpers
	 *
	 * @param	mixed
	 * @return	string
	 */
	static function _attributes_to_string($attributes) {
		if (empty($attributes)) {
			return '';
		}

		if (is_object($attributes)) {
			$attributes = (array) $attributes;
		}

		if (is_array($attributes)) {
			$atts = '';

			foreach ($attributes as $key => $val) {
				$atts .= ' '.$key.'="'.$val.'"';
			}

			return $atts;
		}

		if (is_string($attributes)) {
			return ' '.$attributes;
		}

		return FALSE;
	}

}