<?php

/**
 * {{name}}
 *
 *
 * @version {{version}}
 * @author {{author}}
 */

class {{ctrl}} extends standardObject {
	
	var $gyu_table = "{{ctrl}}";
	var $gyu_id = '{{ctrl}}_id';

	function __construct() {
		if(!isset($this->{$this->gyu_id})) {
			$this->setAttr('creationTime', time());
			$this->setAttr('updateTime', time());
		}
	}

}