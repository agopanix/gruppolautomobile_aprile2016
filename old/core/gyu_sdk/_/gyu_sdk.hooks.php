<?php

\Gyu\Hooks::set('gyu.created', 'gyu_sdk__created');
\Gyu\Hooks::set('gyu.off', 'gyu_sdk__off');
\Gyu\Hooks::set('gyu.off', 'deb_clean');
\Gyu\Hooks::set('gyu.dev-zone', 'gyu_sdk__devPrint');

function gyu_sdk__created($time) {

	# Memory Usage #
	$GLOBALS["__memory"]["__startMemoryUsage"] = memory_get_usage();

	# Time #
	$GLOBALS["__exectime"]["__start"] = microtime(true);

}

function gyu_sdk__off($time) {

	# Memory #
	$GLOBALS["__memory"]["__finalMemoryUsage"] = memory_get_usage();
	$GLOBALS["__memory"]["__peakMemoryUsage"] = memory_get_peak_usage();

	$GLOBALS["__memory"]["delta"] = $GLOBALS["__memory"]["__peakMemoryUsage"] - $GLOBALS["__memory"]["__startMemoryUsage"];
	$GLOBALS["__memory"]["deltaInKB"] = $GLOBALS["__memory"]["delta"] / (1024*100);

	$GLOBALS["__memory"]["ten_user_resource_KB"] = $GLOBALS["__memory"]["deltaInKB"]*10;
	$GLOBALS["__memory"]["hundred_user_resource_KB"] = $GLOBALS["__memory"]["deltaInKB"]*100;
	$GLOBALS["__memory"]["thousand_user_resource_MB"] = ($GLOBALS["__memory"]["deltaInKB"]/1024)*1000;
	$GLOBALS["__memory"]["deltaInKB"] = $GLOBALS["__memory"]["delta"] / (1024*100);
	
	$GLOBALS["__memory"]["deltaInKB"] = round($GLOBALS["__memory"]["deltaInKB"]);

	# Time #
	$GLOBALS["__exectime"]["__end"] = microtime(true);
	$GLOBALS["__exectime"]["__duration"] = $GLOBALS["__exectime"]["__end"] - $GLOBALS["__exectime"]["__start"];

}

function gyu_sdk__devPrint($time, $env) {

	$app_data = array(
		'env' => $env,
		'time' => $time,
		'memory' => $GLOBALS["__memory"],
		'execution' => $GLOBALS["__exectime"],
		'nQuery' => $GLOBALS["num_query"],
		'routes' => $GLOBALS["__route_stack"],
		'methods' => MethodDetect(),
		'stack' => deb_logs()

	);

	if(!isset($env->error)) {
		if(!$env->methods["get"] || MethodDetect('api') || MethodDetect('post'))
			return false;
	}

	Application('gyu_sdk/_v/console', null, $app_data);

}
