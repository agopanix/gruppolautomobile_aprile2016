<?php
	
	if( isset($_GET['id']) ){
		$item = LoadApp('usato', 1)->ApiGet($_GET['id']);
		$title = 'Modifica';
	}else{
		$item = LoadClass('usato', 1);
		$title = 'Aggiungi';
	}
?>

<h1><?=$title?></h1>

<form method="post" enctype="multipart/form-data">
	
	<div class="form-group col-sm-4">
		<label for="inofferta">In Offerta <br>(visibile nella pagina Offerte)</label>
		<?php 
		if($item->inofferta == 1){
			$checkedstr = "checked";
		}else{
			$checkedstr = "";
		}
		?>
		<input class="form-control input-lg" type="checkbox" <?=$checkedstr; ?> name="inofferta" value="1"/>
	</div>

	<div class="form-group col-sm-4">
		<label for="offertahome">In Offerta in Homepage <br>(solo ultime due visibili)</label>
		<?php 
		if($item->offertahome == 1){
			$checkedstr = "checked";
		}else{
			$checkedstr = "";
		}
		?>
		<input class="form-control input-lg" type="checkbox" <?=$checkedstr; ?> name="offertahome" value="1"/>
	</div>

	<div class="form-group col-sm-4">
		<label for="offertapopup">In Offerta Popup <br>(solo ultima visibile)</label>
		<?php 
		if($item->offertapopup == 1){
			$checkedstr = "checked";
		}else{
			$checkedstr = "";
		}
		?>
		<input class="form-control input-lg" type="checkbox" <?=$checkedstr; ?> name="offertapopup" value="1"/>
	</div>
	
	<div class="form-group col-sm-6">
		<label for="descmarca">Marca</label>
		<textarea class="form-control" rows="4" name="descmarca" id="summernote"><?=$item->descmarca?></textarea>
	</div>
	

	
	<div class="form-group col-sm-6">
		<label for="descallestimento">Allestimento</label>
		<textarea class="form-control" rows="4" name="descallestimento" id="summernote"><?=$item->descallestimento?></textarea>
	</div>
	

	<div class="form-group col-sm-6">
		<label for="alim">Alimentazione</label>
		<input class="form-control input-lg" type="text"  name="alim" value="<?=$item->alim?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="intt">Intt</label>
		<input class="form-control input-lg" type="text"  name="intt" value="<?=$item->intt?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="cambio">Cambio</label>
		<input class="form-control input-lg" type="text"  name="cambio" value="<?=$item->cambio?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="catal">Catalizzatore</label>
		<input class="form-control input-lg" type="text"  name="catal" value="<?=$item->catal?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="categ">Categoria</label>
		<input class="form-control input-lg" type="text"  name="categ" value="<?=$item->categ?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="causaleven">Causaleven</label>
		<input class="form-control input-lg" type="text"  name="causaleven" value="<?=$item->causaleven?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="cp">Cp</label>
		<input class="form-control input-lg" type="text"  name="cp" value="<?=$item->cp?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="clientevendopz">Clientevendopz</label>
		<input class="form-control input-lg" type="text"  name="clientevendopz" value="<?=$item->clientevendopz?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="kmperco">Km percorsi</label>
		<input class="form-control input-lg" type="text"  name="kmperco" value="<?=$item->kmperco?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="cil">Cilindrata</label>
		<input class="form-control input-lg" type="text"  name="cil" value="<?=$item->cil?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="capcliente">Cap cliente</label>
		<input class="form-control input-lg" type="text"  name="capcliente" value="<?=$item->capcliente?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="indirizzocliente">Indirizzo cliente</label>
		<input class="form-control input-lg" type="text"  name="indirizzocliente" value="<?=$item->indirizzocliente?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="localcliente">Località cliente</label>
		<input class="form-control input-lg" type="text"  name="localcliente" value="<?=$item->localcliente?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="provcliente">Provincia cliente</label>
		<input class="form-control input-lg" type="text"  name="provcliente" value="<?=$item->provcliente?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="telcliente">Telefono cliente</label>
		<input class="form-control input-lg" type="text"  name="telcliente" value="<?=$item->telcliente?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="codinterno">Cod. interno</label>
		<input class="form-control input-lg" type="text"  name="codinterno" value="<?=$item->codinterno?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="infocar">Cod. Infocar</label>
		<input class="form-control input-lg" type="text"  name="infocar" value="<?=$item->infocar?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="marca">Cod. Marca</label>
		<input class="form-control input-lg" type="text"  name="marca" value="<?=$item->marca?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="modello">Cod. Modello</label>
		<input class="form-control input-lg" type="text"  name="modello" value="<?=$item->modello?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="cod">Cod</label>
		<input class="form-control input-lg" type="text"  name="cod" value="<?=$item->cod?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="codgar">Codgar</label>
		<input class="form-control input-lg" type="text"  name="codgar" value="<?=$item->codgar?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="colore">Colore</label>
		<input class="form-control input-lg" type="text"  name="colore" value="<?=$item->colore?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="cgiace">Cgiace</label>
		<input class="form-control input-lg" type="text"  name="cgiace" value="<?=$item->cgiace?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="costieuro">Costi euro</label>
		<input class="form-control input-lg" type="text"  name="costieuro" value="<?=$item->costieuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="dvendita">D Vendita</label>
		<input class="form-control input-lg" type="text"  name="dvendita" value="<?=$item->dvendita?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="effrit">Eff Rit</label>
		<input class="form-control input-lg" type="text"  name="effrit" value="<?=$item->effrit?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="immatric">Immatricolazione</label>
		<input class="form-control input-lg" type="text"  name="immatric" value="<?=$item->immatric?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="inserimen">Inserimento</label>
		<input class="form-control input-lg" type="text"  name="inserimen" value="<?=$item->inserimen?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="prevrit">Prev. Rit.</label>
		<input class="form-control input-lg" type="text"  name="prevrit" value="<?=$item->prevrit?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="prrevis">Prev. Revis.</label>
		<input class="form-control input-lg" type="text"  name="prrevis" value="<?=$item->prrevis?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="scbollo">Scadenza bollo</label>
		<input class="form-control input-lg" type="text"  name="scbollo" value="<?=$item->scbollo?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ulrevis">Ultima revisione</label>
		<input class="form-control input-lg" type="text"  name="ulrevis" value="<?=$item->ulrevis?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="deposito">Deposito</label>
		<input class="form-control input-lg" type="text"  name="deposito" value="<?=$item->deposito?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="descgar">Desc. garanzia</label>
		<input class="form-control input-lg" type="text"  name="descgar" value="<?=$item->descgar?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="accessori">Accessori</label>
		<textarea class="form-control" rows="4" name="accessori" id="summernote"><?=$item->accessori?></textarea>
	</div>


	<div class="form-group col-sm-6">
		<label for="descmodello">Desc. modello</label>
		<textarea class="form-control" rows="4" name="descmodello" id="summernote"><?=$item->descmodello?></textarea>
	</div>

	<div class="form-group col-sm-6">
		<label for="destinazione">Destinazione</label>
		<input class="form-control input-lg" type="text"  name="destinazione" value="<?=$item->destinazione?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="dv">DV</label>
		<input class="form-control input-lg" type="text"  name="dv" value="<?=$item->dv?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ecarreuro">Ecarreuro</label>
		<input class="form-control input-lg" type="text"  name="ecarreuro" value="<?=$item->ecarreuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="einteuro">Einteuro</label>
		<input class="form-control input-lg" type="text"  name="einteuro" value="<?=$item->einteuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="emecceuro">Emecceuro</label>
		<input class="form-control input-lg" type="text"  name="emecceuro" value="<?=$item->emecceuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="epneeuro">Epneeuro</label>
		<input class="form-control input-lg" type="text"  name="epneeuro" value="<?=$item->epneeuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="abs">Abs</label>
		<input class="form-control input-lg" type="text"  name="abs" value="<?=$item->abs?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="aip">Airbag passeggero</label>
		<input class="form-control input-lg" type="text"  name="aip" value="<?=$item->aip?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="aig">Airbag lato guida</label>
		<input class="form-control input-lg" type="text"  name="aig" value="<?=$item->aig?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ail">Airbag laterali</label>
		<input class="form-control input-lg" type="text"  name="ail" value="<?=$item->ail?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="caa">Cambio Automatico</label>
		<input class="form-control input-lg" type="text"  name="caa" value="<?=$item->caa?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="cerchiinlega">Cerchi in lega</label>
		<input class="form-control input-lg" type="text"  name="cerchiinlega" value="<?=$item->cerchiinlega?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="cli">Climatizzatore</label>
		<input class="form-control input-lg" type="text"  name="cli" value="<?=$item->cli?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ctr">Controllo trazione</label>
		<input class="form-control input-lg" type="text"  name="ctr" value="<?=$item->ctr?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="internipregiati">Interni Pregiati</label>
		<input class="form-control input-lg" type="text"  name="internipregiati" value="<?=$item->internipregiati?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="navigatore">Navigatore</label>
		<input class="form-control input-lg" type="text"  name="navigatore" value="<?=$item->navigatore?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ser">Servosterzo</label>
		<input class="form-control input-lg" type="text"  name="ser" value="<?=$item->ser?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="soa">Sospensioni Autoregolabili</label>
		<input class="form-control input-lg" type="text"  name="soa" value="<?=$item->soa?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="tea">Tetto apribile</label>
		<input class="form-control input-lg" type="text"  name="tea" value="<?=$item->tea?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="fa">FA</label>
		<input class="form-control input-lg" type="text"  name="fa" value="<?=$item->fa?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="equipcodici">Codici equip.</label>
		<textarea class="form-control" rows="4" name="equipcodici" id="summernote"><?=$item->equipcodici?></textarea>
	</div>

	<div class="form-group col-sm-6">
		<label for="equipdesc">Descrizione equip.</label>
		<textarea class="form-control" rows="4" name="equipdesc" id="summernote"><?=$item->equipdesc?></textarea>
	</div>

	
		<div class="form-group col-sm-6">
			<label for="foto1">Foto1</label>
			<input type="hidden" class="form-control" name="foto1" value="<?=$item->foto1;?>">
			<?php 
					
				if(strlen(trim($item->foto1))>0){
						$url = 'http://www.ocholding.com/infocar/infocar/'.$item->foto1.'';
				
						$mioarrayurl = get_headers($url, 1);
						if($mioarrayurl[0]=="HTTP/1.0 200 OK"){
							$dettfoto1 = "http://www.ocholding.com/infocar/infocar/".$item->foto1;
						}else{
							$dettfoto1 = "http://www.ocholding.com/infocar/infocar2/".$item->foto1;
						}
						echo '<br>Anteprima: <br><img src="'.$dettfoto1.'" style="width:100%;">';
				}else{
						echo '<br>Anteprima non disponibile.';
				}
			
			?>
		</div>
	
		<div class="form-group col-sm-6">
			<label for="foto2">Foto2</label>
			<input type="hidden" class="form-control" name="foto2" value="<?=$item->foto2;?>">
			<?php 
					
				if(strlen(trim($item->foto2))>0){
						$url = 'http://www.ocholding.com/infocar/infocar/'.$item->foto2.'';
				
						$mioarrayurl = get_headers($url, 1);
						if($mioarrayurl[0]=="HTTP/1.0 200 OK"){
							$dettfoto2 = "http://www.ocholding.com/infocar/infocar/".$item->foto2;
						}else{
							$dettfoto2 = "http://www.ocholding.com/infocar/infocar2/".$item->foto2;
						}
						echo '<br>Anteprima: <br><img src="'.$dettfoto2.'" style="width:100%;">';
				}else{
						echo '<br>Anteprima non disponibile.';
				}
			
			?>
		</div>
	
		<div class="form-group col-sm-6">
			<label for="foto3">foto3</label>
			<input type="hidden" class="form-control" name="foto3" value="<?=$item->foto3;?>">
			<?php 
					
				if(strlen(trim($item->foto3))>0){
						$url = 'http://www.ocholding.com/infocar/infocar/'.$item->foto3.'';
				
						$mioarrayurl = get_headers($url, 1);
						if($mioarrayurl[0]=="HTTP/1.0 200 OK"){
							$dettfoto3 = "http://www.ocholding.com/infocar/infocar/".$item->foto3;
						}else{
							$dettfoto3 = "http://www.ocholding.com/infocar/infocar2/".$item->foto3;
						}
						echo '<br>Anteprima: <br><img src="'.$dettfoto3.'" style="width:100%;">';
				}else{
						echo '<br>Anteprima non disponibile.';
				}
			
			?>
		</div>
	
		<div class="form-group col-sm-6">
			<label for="foto4">foto4</label>
			<input type="hidden" class="form-control" name="foto4" value="<?=$item->foto4;?>">
			<?php 
					
				if(strlen(trim($item->foto4))>0){
						$url = 'http://www.ocholding.com/infocar/infocar/'.$item->foto4.'';
				
						$mioarrayurl = get_headers($url, 1);
						if($mioarrayurl[0]=="HTTP/1.0 200 OK"){
							$dettfoto4 = "http://www.ocholding.com/infocar/infocar/".$item->foto4;
						}else{
							$dettfoto4 = "http://www.ocholding.com/infocar/infocar2/".$item->foto4;
						}
						echo '<br>Anteprima: <br><img src="'.$dettfoto4.'" style="width:100%;">';
				}else{
						echo '<br>Anteprima non disponibile.';
				}
			
			?>
		</div>
	
		<div class="form-group col-sm-6">
			<label for="foto5">foto5</label>
			<input type="hidden" class="form-control" name="foto5" value="<?=$item->foto5;?>">
			<?php 
					
				if(strlen(trim($item->foto5))>0){
						$url = 'http://www.ocholding.com/infocar/infocar/'.$item->foto5.'';
				
						$mioarrayurl = get_headers($url, 1);
						if($mioarrayurl[0]=="HTTP/1.0 200 OK"){
							$dettfoto5 = "http://www.ocholding.com/infocar/infocar/".$item->foto5;
						}else{
							$dettfoto5 = "http://www.ocholding.com/infocar/infocar2/".$item->foto5;
						}
						echo '<br>Anteprima: <br><img src="'.$dettfoto5.'" style="width:100%;">';
				}else{
						echo '<br>Anteprima non disponibile.';
				}
			
			?>
		</div>
	

	
		<div class="form-group col-sm-6">
			<label for="foto6">foto6</label>
			<input type="hidden" class="form-control" name="foto6" value="<?=$item->foto6;?>">
			<?php 
					
				if(strlen(trim($item->foto6))>0){
						$url = 'http://www.ocholding.com/infocar/infocar/'.$item->foto6.'';
				
						$mioarrayurl = get_headers($url, 1);
						if($mioarrayurl[0]=="HTTP/1.0 200 OK"){
							$dettfoto6 = "http://www.ocholding.com/infocar/infocar/".$item->foto6;
						}else{
							$dettfoto6 = "http://www.ocholding.com/infocar/infocar2/".$item->foto6;
						}
						echo '<br>Anteprima: <br><img src="'.$dettfoto6.'" style="width:100%;">';
				}else{
						echo '<br>Anteprima non disponibile.';
				}
			
			?>
		</div>
	

	
		<div class="form-group col-sm-6">
			<label for="foto7">foto7</label>
			<input type="hidden" class="form-control" name="foto7" value="<?=$item->foto7;?>">
			<?php 
					
				if(strlen(trim($item->foto7))>0){
						$url = 'http://www.ocholding.com/infocar/infocar/'.$item->foto7.'';
				
						$mioarrayurl = get_headers($url, 1);
						if($mioarrayurl[0]=="HTTP/1.0 200 OK"){
							$dettfoto7 = "http://www.ocholding.com/infocar/infocar/".$item->foto7;
						}else{
							$dettfoto7 = "http://www.ocholding.com/infocar/infocar2/".$item->foto7;
						}
						echo '<br>Anteprima: <br><img src="'.$dettfoto7.'" style="width:100%;">';
				}else{
						echo '<br>Anteprima non disponibile.';
				}
			
			?>
		</div>
	

	
		<div class="form-group col-sm-6">
			<label for="foto8">foto8</label>
			<input type="hidden" class="form-control" name="foto8" value="<?=$item->foto8;?>">
			<?php 
					
				if(strlen(trim($item->foto8))>0){
						$url = 'http://www.ocholding.com/infocar/infocar/'.$item->foto8.'';
				
						$mioarrayurl = get_headers($url, 1);
						if($mioarrayurl[0]=="HTTP/1.0 200 OK"){
							$dettfoto8 = "http://www.ocholding.com/infocar/infocar/".$item->foto8;
						}else{
							$dettfoto8 = "http://www.ocholding.com/infocar/infocar2/".$item->foto8;
						}
						echo '<br>Anteprima: <br><img src="'.$dettfoto8.'" style="width:100%;">';
				}else{
						echo '<br>Anteprima non disponibile.';
				}
			
			?>
		</div>
	

	<div class="form-group col-sm-6">
		<label for="fc">fc</label>
		<input class="form-control input-lg" type="text"  name="fc" value="<?=$item->fc?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="funzionarioritiro">Funzionario ritiro</label>
		<input class="form-control input-lg" type="text"  name="funzionarioritiro" value="<?=$item->funzionarioritiro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="funzionariovendita">Funzionario vendita</label>
		<input class="form-control input-lg" type="text"  name="funzionariovendita" value="<?=$item->funzionariovendita?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ga">Ga</label>
		<input class="form-control input-lg" type="text"  name="ga" value="<?=$item->ga?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="giac">Giacenza</label>
		<input class="form-control input-lg" type="text"  name="giac" value="<?=$item->giac?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ip">ip</label>
		<input class="form-control input-lg" type="text"  name="ip" value="<?=$item->ip?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="le">le</label>
		<input class="form-control input-lg" type="text"  name="le" value="<?=$item->le?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="lc">lc</label>
		<input class="form-control input-lg" type="text"  name="lc" value="<?=$item->lc?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="normativa">Normativa</label>
		<input class="form-control input-lg" type="text"  name="normativa" value="<?=$item->normativa?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="numtelaio">Num. telaio</label>
		<input class="form-control input-lg" type="text"  name="numtelaio" value="<?=$item->numtelaio?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="noteauto">Note Auto</label>
		<textarea class="form-control" rows="4" name="noteauto" id="summernote"><?=$item->noteauto?></textarea>
	</div>

	<div class="form-group col-sm-6">
		<label for="noteinternet">Note Internet</label>
		<textarea class="form-control" rows="4" name="noteinternet" id="summernote"><?=$item->noteinternet?></textarea>
	</div>

	<div class="form-group col-sm-6">
		<label for="oneri">Oneri</label>
		<input class="form-control input-lg" type="text"  name="oneri" value="<?=$item->oneri?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="passpro">Pass. proprietà</label>
		<input class="form-control input-lg" type="text"  name="passpro" value="<?=$item->passpro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="immatricolaz">Immatricolazione</label>
		<input class="form-control input-lg" type="text"  name="immatricolaz" value="<?=$item->immatricolaz?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="cav">Cavalli fiscali</label>
		<input class="form-control input-lg" type="text"  name="cav" value="<?=$item->cav?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="kw">Kilowatt</label>
		<input class="form-control input-lg" type="text"  name="kw" value="<?=$item->kw?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="pcarreuro">Pcarreuro</label>
		<input class="form-control input-lg" type="text"  name="pcarreuro" value="<?=$item->pcarreuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="pinteuro">Pinteuro</label>
		<input class="form-control input-lg" type="text"  name="pinteuro" value="<?=$item->pinteuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="pmecceuro">Pmecceuro</label>
		<input class="form-control input-lg" type="text"  name="pmecceuro" value="<?=$item->pmecceuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ppneuro">Ppneuro</label>
		<input class="form-control input-lg" type="text"  name="ppneuro" value="<?=$item->ppneuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="prezzoveuro">Prezzoveuro</label>
		<input class="form-control input-lg" type="text"  name="prezzoveuro" value="<?=$item->prezzoveuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="prezzovalt">Prezzovalt</label>
		<input class="form-control input-lg" type="text"  name="prezzovalt" value="<?=$item->prezzovalt?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="prezzoveff">Prezzoveff</label>
		<input class="form-control input-lg" type="text"  name="prezzoveff" value="<?=$item->prezzoveff?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="minprivato">Min privato</label>
		<input class="form-control input-lg" type="text"  name="minprivato" value="<?=$item->minprivato?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="stimato">Stimato</label>
		<input class="form-control input-lg" type="text"  name="stimato" value="<?=$item->stimato?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="prezzointernet">Prezzointernet</label>
		<input class="form-control input-lg" type="text"  name="prezzointernet" value="<?=$item->prezzointernet?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="proprietario">Proprietario</label>
		<input class="form-control input-lg" type="text"  name="proprietario" value="<?=$item->proprietario?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="provenienza">Provenienza</label>
		<input class="form-control input-lg" type="text"  name="provenienza" value="<?=$item->provenienza?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="prf">Prf</label>
		<input class="form-control input-lg" type="text"  name="prf" value="<?=$item->prf?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="provve">Provve</label>
		<input class="form-control input-lg" type="text"  name="provve" value="<?=$item->provve?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="puntoritiro">Punto ritiro</label>
		<input class="form-control input-lg" type="text"  name="puntoritiro" value="<?=$item->puntoritiro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="quotaze">Quotaz. E.</label>
		<input class="form-control input-lg" type="text"  name="quotaze" value="<?=$item->quotaze?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="quotazqrt">Quotaz. Qrt.</label>
		<input class="form-control input-lg" type="text"  name="quotazqrt" value="<?=$item->quotazqrt?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ricveuro">Ricveuro</label>
		<input class="form-control input-lg" type="text"  name="ricveuro" value="<?=$item->ricveuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="spveuro">Spveuro</label>
		<input class="form-control input-lg" type="text"  name="spveuro" value="<?=$item->spveuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="statocar">Stato carrozzeria</label>
		<input class="form-control input-lg" type="text"  name="statocar" value="<?=$item->statocar?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="statointe">Stato interni</label>
		<input class="form-control input-lg" type="text"  name="statointe" value="<?=$item->statointe?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="statomecc">Stato mecc.</label>
		<input class="form-control input-lg" type="text"  name="statomecc" value="<?=$item->statomecc?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="statopneu">Stato pneum.</label>
		<input class="form-control input-lg" type="text"  name="statopneu" value="<?=$item->statopneu?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="supervalut">Supervalutazione</label>
		<input class="form-control input-lg" type="text"  name="supervalut" value="<?=$item->supervalut?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ta">Ta</label>
		<input class="form-control input-lg" type="text"  name="ta" value="<?=$item->ta?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="targa">Targa</label>
		<input class="form-control input-lg" type="text"  name="targa" value="<?=$item->targa?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="tipologiainterna">Tipologia interna</label>
		<input class="form-control input-lg" type="text"  name="tipologiainterna" value="<?=$item->tipologiainterna?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="tipologiaveicolo">Tipologia veicolo</label>
		<input class="form-control input-lg" type="text"  name="tipologiaveicolo" value="<?=$item->tipologiaveicolo?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="titoloannuncio">Titolo annuncio</label>
		<input class="form-control input-lg" type="text"  name="titoloannuncio" value="<?=$item->titoloannuncio?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ubicazione">Ubicazione</label>
		<input class="form-control input-lg" type="text"  name="ubicazione" value="<?=$item->ubicazione?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="valorecarico">Valore carico</label>
		<input class="form-control input-lg" type="text"  name="valorecarico" value="<?=$item->valorecarico?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="vpritiroe">Vpritiroe</label>
		<input class="form-control input-lg" type="text"  name="vpritiroe" value="<?=$item->vpritiroe?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="ritiroeuro">Ritiro euro</label>
		<input class="form-control input-lg" type="text"  name="ritiroeuro" value="<?=$item->ritiroeuro?>"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="vareqritiroe">Vareqritiroe</label>
		<input class="form-control input-lg" type="text"  name="vareqritiroe" value="<?=$item->vareqritiroe?>"/>
	</div>

	<input type="hidden" name="creationTime" value="<?=$item->creationTime?>">
	<input type="hidden" name="updateTime" value="<?=$item->updateTime?>">



	<div style="clear:both;"></div>
	
	<? if( isset($item->usato_id) ): ?>
	
	<div class="row">
		<div class="col-sm-3">
			
			<button type="submit" class="btn btn-default btn-block">Modifica</button>
			
		</div>
		<div class="col-sm-9 text-right">
			
			<input type="hidden" name="usato_id" value="<?=$item->usato_id?>" />
			<div class="checkbox">
				<label><input type="checkbox" name="delete" value="1" /> Elimina definitivamente l'inserimento</label>
			</div>
			
		</div>
	</div>
	
	<? else: ?>
	
	<div class="row">
		<div class="col-sm-3">
			
			<button type="submit" class="btn btn-default btn-block">Inserisci</button>
	
		</div>
	</div>
	
	<? endif; ?>
		
	<div class="clearfix margin-40"></div>
	
	<? CallFunction('hooks', 'get', 'form.end', $this); ?>
		
</form>