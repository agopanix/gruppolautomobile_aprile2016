<div class="page-header">
	<ul class="list-inline pull-right">
		<li><a class="btn btn-default" href="/usato/admin/form/"><i class="fa fa-cloud-upload"></i> Aggiungi</a></li>
	</ul>
	
	<h1>Parco Usato</h1>
</div>

<? 
					
$count 			= LoadClass('usato', 1)->filter('COUNT');

$perPage 		= 40;
$maxPage 		= ceil($count / $perPage);
$currentPage 	= ($_GET['p']) ? $_GET['p'] : 1;
$startItem 		= $perPage*($currentPage-1);
	
$items 			= LoadClass('usato', 1)->filter( array('LIMIT', $startItem, $perPage), ' ORDER BY descmarca ASC ' );

if( $items ) : 

?>

	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Marca e Allestimento</th>
				<th>Cod</th>
				<th>Infocar</th>
				<th>Visualizza</th>
			</tr>
		</thead>
		<tbody>	
	
			<?php foreach( $items as $item ) : ?>
			
			<tr>
				<td>
					<?=$item->usato_id?>
				</td>
				<td>
					<a href="/usato/admin/form/id:<?=$item->usato_id?>"><?=$item->descmarca?> <?=$item->descallestimento?></a>
				</td>
				<td>
					<a href="/usato/admin/form/id:<?=$item->usato_id?>"><?=$item->cod?></a>
				</td>
				<td>
					<a href="/usato/admin/form/id:<?=$item->usato_id?>"><?=$item->infocar?></a>
				</td>
				<td>
					<a href="/usato/admin/form/id:<?=$item->usato_id?>">Visualizza</a>
				</td>
			</tr>
			
			<?php endforeach; ?>
			
		</tbody>
	</table>	
	
	<?php if( $count > 0 ) CallFunction('admin', 'pagination', $currentPage, $maxPage, '/usato/admin'); ?>

<? else: ?>

	<div class="alert alert-info">
		Ancora nessun elemento inserito...
	</div>
	
<? endif; ?>				