<?php

### GYURAL ###

/*

----------
usato 
----------

Filename: /app/usato/_/usato.lib.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 12/11/2015
	
*/

class usato extends standardObject {
	
	var $gyu_table	= 'usato';
	var $gyu_id 	= 'usato_id';

	function __construct() {
			
		if( $this->usato_id ){
		
			$this->gyu_updateTime = $this->updateTime;
			$this->gyu_url = '/frontend/dettaglio/cod:'.$this->cod;
				
			CallFunction('hooks', 'get', 'get.end', $this);
		
		}else{
		
			$this->creationTime = time();
		
		}
		
		$this->updateTime = time();
		
	}

	function getUsatotutto() {

		return LoadClass('usato', 1)->filter(' WHERE 1 ORDER BY descmarca ASC');

	}

	function getUsatofromcod($cod) {

		return LoadClass('usato', 1)->filter(['cod', $cod], 'ONE');

	}
	
	function get_permalink(){
		
		if( $this->usato_id )
			return '/frontend/usato/id:'.$this->usato_id;
		else
			return false;
	}

}


?>