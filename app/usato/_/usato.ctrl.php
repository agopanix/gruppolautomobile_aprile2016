<?php

### GYURAL ###

/*

----------
usato 
----------

Filename: /app/usato/_/usato.ctrl.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 12/11/2015
	
*/

class usatoCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {
		
	}

	function ctrlIndex() {
		Header( "HTTP/1.1 301 Moved Permanently" );
		header("Location: /frontend/usato");
		// $listusato = $this->apiGets();

		// $app_data["listusato"] = $listusato;

		// Application('frontend/_v/commons/header', null, $app_data);
		// Application('frontend/_v/usato', null, $app_data);
		// Application('frontend/_v/commons/footer', null, $app_data);

		// $this->listusato = $this->apiGets();
		// $this->view('frontend/_v/commons/header');
		// $this->view('frontend/_v/usato');
		// $this->view('frontend/_v/commons/footer');
	}

	function CtrlDettaglio(){
		#die("Stop");
		Header( "HTTP/1.1 301 Moved Permanently" );
		header("Location: /frontend/dettaglio/cod:".$_REQUEST['cod']);
	}
	
	// api di richiamo di tutti gli oggetti dell'app
	function apiGets($args = null){
		#echo '<pre>'.print_r($args, 1).'</pre>';
		#die();
		return LoadClass('usato', 1)->filter(array('LIMIT', 0, 10000), ' ORDER BY descmarca ASC, descmodello ASC ');
		
	}
	
	// api di richiamo del singolo oggetto dell'app
	function apiGet($id){
		
		if($_REQUEST['id']){
			$id = $_REQUEST['id'];
		}
		
		if($id){
			return LoadClass('usato', 1)->get($id);
		}else{
			return false;
		}
		
	}

	function apiGetfromcod($cod){
		
		if($_REQUEST['cod']){
			$cod = $_REQUEST['cod'];
		}
		
		if(strlen($cod)>0){
			return LoadClass('usato', 1)->filter(['cod', $cod]);

		}else{
			return false;
		}
		
	}

	function ApiMarchiusato() {

		return FetchObject(Database()->query("SELECT DISTINCT descmarca FROM usato WHERE 1 ORDER BY descmarca ASC"));

	}

	function ApiAlimentazioneusato() {

		return FetchObject(Database()->query("SELECT DISTINCT `alim` FROM `usato` ORDER BY `alim` ASC"));

	}

	function ApiColoreusato() {

		return FetchObject(Database()->query("SELECT DISTINCT `colore` FROM `usato` ORDER BY `colore` ASC"));

	}

	function ApiGetofferte() {

		return FetchObject(Database()->query("SELECT * FROM usato WHERE foto1 <> '' ORDER BY giac DESC LIMIT 2"));

	}

	function ApiAltrointeresse($cod) {



		if($_REQUEST['cod']){
			$cod = $_REQUEST['cod'];
		}

		//echo 'dentro apialtrointeresse con cod: '.$cod.'<br>';

		if(strlen($cod)>0){
			$miavett = FetchObject(Database()->query("SELECT * FROM `usato` WHERE cod = '".$cod."'ORDER BY `giac` DESC LIMIT 1"));

			if(strlen($miavett->prezzointernet)>0 && $miavett->prezzointernet > 0){
								$mioprezzook = $miavett->prezzointernet;
								
							}else{
								$mioprezzook = $miavett->stimato;
								
							}

			//echo $miavett->descmarca.' - '.$mioprezzook;
			//die();

			return FetchObject(Database()->query("SELECT * FROM `usato` WHERE descmarca = '".$miavett->descmarca."' OR (prezzointernet > ".($mioprezzook-2000)." AND prezzointernet < ".($mioprezzook+2000).") ORDER BY `giac` DESC LIMIT 3"));

		}else{
			return false;
		}

	}
	
}

?>