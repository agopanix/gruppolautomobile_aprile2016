<?php

### GYURAL ###

/*

----------
usato 
----------

Filename: /app/usato/_/usato_admin.ctrl.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 12/11/2015
	
*/

class usato_adminCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	// vista per la lista in admin
	function getIndex(){
				
		Application('admin/_v/header', null, $app_data);
		Application('usato/_v/index', null, $app_data);
		Application('admin/_v/footer', null, $app_data);
		
	}
	
	// vista per il form creazione/modifica in admin
	function getForm(){
		
		Application('admin/_v/header', null, $app_data);
		Application('usato/_v/form', null, $app_data);
		Application('admin/_v/footer', null, $app_data);
		
	}
	
	// post di modifica o creazione o cancellazione dell'oggetto
	function postForm(){
		
		//post
		
		if( $_POST['usato_id'] and $_POST['delete'] ){
			
			//delete item
			if( !$item = LoadClass('usato', 1)->get($_POST['usato_id']) ) exit('Errore load');			
			if( !$item->deleteExecute() ) exit('Errore delete');
						
			CallFunction('hooks', 'get', 'delete.end', $item);
			
			header('Location: /usato/admin/delete:1');
			exit;
			
		}elseif( $_POST['usato_id'] ){
			
			//update item
			if( !$item = LoadClass('usato', 1)->get($_POST['usato_id']) ) exit('Errore load');
		
		} else {
						
			//create item
			$item = LoadClass('usato', 1);
		
		}
		
		$item->refill($_POST);
		
		// execution
		if( $_POST['usato_id'] ){
			
			if( !$item->putExecute() ) exit('Errore put');
		
		} else {
						
			if( !$item->hangExecute() ) exit('Errore hang');
		
		}
		
		CallFunction('hooks', 'get', 'post.end', $item);
		
		header('Location: /usato/admin/form/id:'.$item->usato_id.'/update:1');
		
	}
	
}

?>