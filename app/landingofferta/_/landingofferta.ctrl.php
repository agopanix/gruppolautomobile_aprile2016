<?php

class  landingoffertaCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile - Chi Siamo',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> '',
			'subTitle'		=> '',
			'voceMenu' 		=> '',
			'header-bkg'	=> 'header-bkg-4.jpg',
			'header-opaque' => true,
			'showHomebtn'	=> true
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/landing');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

	}

	
}
