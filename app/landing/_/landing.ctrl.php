
<?php

class  landingCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile - Chi Siamo',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> 'LANDING PER OFFERTA',
			'subTitle'		=> 'Sotto titolo della landing page dedicata ad un&rsquo; offerta specifica',
			'voceMenu' 		=> '',
			'header-bkg'	=> 'header-bkg-5.jpg',
			'header-opaque' => true,
			'showHomebtn'	=> true,
			'landingOfferta' => true
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/landingofferta');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

	}

	
}


