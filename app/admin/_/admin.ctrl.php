<?php

### GYURAL ###

/*

----------
admin 
----------

Filename: /app/admin/_/admin.ctrl.php
 Version: 0.1
  Author: Andrea Rufo <orangedropdesign@gmail.com>
    Date: 20/10/2014
	
*/

class adminCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {
		
	}
	
	function GetIndex(){
		
		Application('/admin/_v/header', null);
		Application('/admin/_v/index', null);
		Application('/admin/_v/footer', null);
		
	}
	
	function getLogin(){
		
		Application('/admin/_v/login', null);
		
	}
	
	function postLogin() {
		
		header('location: /admin');
		
	}
	
	function getLogout(){
		
		if( !session_destroy() ) exit('Errore sessione');
		header('location: /admin');
		
	}

}

?>