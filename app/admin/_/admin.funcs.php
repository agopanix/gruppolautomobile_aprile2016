<?php

### GYURAL ###

/*

----------
admin 
----------

Filename: /app/admin/_/admin.funcs.php
 Version: 0.1
  Author: Andrea Rufo <orangedropdesign@gmail.com>
    Date: 20/10/2014
	
*/

function admin__sample() {

	// this is a sample funcs!
	return 'Hello!';

}

function admin__lang($lang, $string){
	if(isset($lang)){

	}
	else{
		$lang = $GLOBALS['lang'];
	}

	#return "<pre>".print_r($string, 1)."</pre>Lang:".$lang;
	$string_array = unserialize($string);
	if(is_array($string_array)){	
		if(array_key_exists($lang, $string_array)){
			return $string_array[$lang];
		}
		else{
			return $string_array["it"];	
		}
	}
	else{
		#return "<pre>".print_r($string_array, 1)."</pre>".$string;
		return $string;
	}
}

function admin__camp($element) {

	// this is a sample funcs!
	#$html = "<pre>".print_r($element, 1)."</pre>";
	$html = "";
	if($element['type'] == "text"){
		if($element['langs']){
			foreach ($element['langs'] as $k => $lang) {
				# code...
				$html .= '<div class="form-group">
					<label for="'.$element['nome']."[".$lang."]".'">'.$element['label'].' '.$lang.'</label>
					<input class="form-control input-lg" type="text" required name="'.$element['nome']."[".$lang."]".'" value="'.$element['obj']->getattr($element['nome'], $lang).'"/>
				</div>';
			}
		}
		else{
			$html .= '<div class="form-group">
				<label for="'.$element['nome'].'">'.$element['label'].'</label>
				<input class="form-control input-lg" type="text" required name="'.$element['nome'].'" value="'.$element['obj']->getattr($element['nome']).'"/>
			</div>';
		}
	}
	else if($element['type'] == "textarea"){
		if($element['langs']){
			foreach ($element['langs'] as $k => $lang) {
				# code...
				$html .= '<div class="form-group">
					<label for="'.$element['nome']."[".$lang."]".'">'.$element['label'].' '.$lang.'</label>
					<textarea class="form-control" rows="4" name="'.$element['nome']."[".$lang."]".'" id="summernote">'.$element['obj']->getattr($element['nome'], $lang).'</textarea>
				</div>';
			}
		}
		else{
			$html .= '<div class="form-group">
				<label for="'.$element['nome'].'">'.$element['label'].'</label>
				<textarea class="form-control" rows="4" name="'.$element['nome'].'" id="summernote">'.$element['obj']->getattr($element['nome']).'</textarea>
			</div>';
		}
	}

	return $html;

}

function admin__icon($val) {

	if($val == 1)
		return ('<i class="fa fa-check-circle text-success"></i>');
	elseif($val == 0)
		return ('<i class="fa fa-times-circle text-danger"></i>');
	elseif($val == 2)
		return ('<i class="fa fa-minus-circle text-warning"></i>');
	elseif($val == 3)
		return ('<i class="fa fa-exclamation-circle text-info"></i>');
}

function admin__pagination($page, $max, $root){
	
	?>
				
	<nav class="text-center">
		<ul class="pagination">
			<li <?php if($page == 1){ ?>class="disabled"<?php } ?>>
				<a href="<?=$root?>/p:<?=($page-1)?>"><span class="fa fa-angle-left"></span><span class="sr-only">Previous</span></a>
			</li>
			
			<?php for($i=1; $i<$max+1; $i++){ ?> 
				<li <?php if($page == $i){ ?>class="active"<?php } ?>>
					<a href="<?=$root?>/p:<?=$i ?>"><?=$i?></a>
				</li>
			<?php } ?>
			
			<li <?php if($page == $max){ ?>class="disabled"<?php } ?>>
				<a href="<?=$root?>/p:<?=($page+1)?>"><span class="fa fa-angle-right"></span><span class="sr-only">Next</span></a>
			</li>
		</ul>
	</nav>
	
	<?
	
}

?>