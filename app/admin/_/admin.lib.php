<?php

### GYURAL ###

/*

----------
admin 
----------

Filename: /app/admin/_/admin.lib.php
 Version: 0.1
  Author: Andrea Rufo <orangedropdesign@gmail.com>
    Date: 20/10/2014
	
*/

class admin extends standardObject {
	
	function __construct() {
		
		$this->app_data['apps'] = array();
		
		$this->app_data['apps']['admin'] = array(
			'label'		=>	'dashboard',
			'icon'		=>	'dashboard',
			'count'		=>	false
		);
		
		$this->app_data['apps'][] = 'break';
		
		

		$this->app_data['apps']['paginahome'] = array(
			'label'		=>	'Pagina Home',
			'icon'		=>	'home',
			'count'		=>	true
		);

		$this->app_data['apps']['servizi'] = array(
			'label'		=>	'Servizi',
			'icon'		=>	'map-signs',
			'count'		=>	true
		);

		$this->app_data['apps']['chisiamo'] = array(
			'label'		=>	'Chi Siamo',
			'icon'		=>	'black-tie',
			'count'		=>	true
		);

		$this->app_data['apps']['usato'] = array(
			'label'		=>	'Usato',
			'icon'		=>	'automobile',
			'count'		=>	true
		);

		

		
								
	}

}


?>