<!DOCTYPE html>
<html lang="it-IT">
<head>
	
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Accedi!</title>
	
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/flatly/bootstrap.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
	
	<link href="/app/admin/_assets/css/app.css" type="text/css" charset="utf-8" rel="stylesheet" media="screen">

</head>
<body class="admin-login">
	
	<div class="container">
		<form method="post">
			
			<img src="/app/admin/_assets/mndr.png" alt="Accedi!" width="400" height="401">
			
			<div class="form-group">
				<label for="username" class="sr-only">Username</label>
				<input name="username" type="email" class="form-control" placeholder="Username" required autofocus>
			</div>
			<div class="form-group">
				<label for="password" class="sr-only">Password</label>
				<div class="input-group">
					<input name="password" type="password" class="form-control" placeholder="Password" required>
					<span class="input-group-btn">
						<button id="show" class="btn btn-default" type="button">
							<i class="fa fa-fw fa-eye"></i>
						</button>
					</span>
			    </div><!-- /input-group -->
				
			</div>
			
			<button class="btn btn-primary btn-block" type="submit"><i class="fa fa-key"></i> Accedi</button>
			
			<hr>
				
			<a class="btn btn-success btn-block" href="/"><i class="fa fa-home"></i> Torna alla home del sito </a>
				
		</form>
		
	</div>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	
	<script>
		$(document).ready(function(){
			
			$('#show').click(function(){
				
				var type = $('input[name="password"]').attr( 'type' );
				
				if( type == 'password' ){
					
					$('input[name="password"]').attr( 'type', 'text' );
					$('#show i').removeClass( 'fa-eye' ).addClass( 'fa-eye-slash' );
					
				}else{
					
					$('input[name="password"]').attr( 'type', 'password' );
					$('#show i').removeClass( 'fa-eye-slash' ).addClass( 'fa-eye' );
					
				}
				
			});
			
		});
	</script>
	
</body>
</html>