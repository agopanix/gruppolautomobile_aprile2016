<?php

if( !logged() ) { header('Location: /admin/login/e:1'); }

$app_data['apps'] = LoadClass('admin', 1)->app_data['apps'];
$e = explode('/', $_SERVER['REQUEST_URI']); $currentApp = $e[1];

?>

<!DOCTYPE html>
<html lang="it-IT">
	
<head>

	<meta charset="utf-8">

	<title><?= siteName ?> Pannello</title>
	
	<meta http-equiv="x-ua-compatible" content="ie=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/flatly/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	
	<link href="/app/admin/_assets/css/app.css" type="text/css" charset="utf-8" rel="stylesheet" media="screen" title="app">
	
	<!-- script -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	
	<!-- validator -->
	<link rel="stylesheet" href="//pink.gyural.com/cdn/formvalidation-master/dist/css/formValidation.min.css">
	<script src="//pink.gyural.com/cdn/formvalidation-master/dist/js/formValidation.min.js" type="text/javascript"></script>
	<script src="//pink.gyural.com/cdn/formvalidation-master/dist/js/framework/bootstrap.min.js" type="text/javascript"></script>
	<script src="//pink.gyural.com/cdn/formvalidation-master/dist/js/language/it_IT.js" type="text/javascript"></script>
	
	<!-- include summernote css/js-->
	<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.5.10/summernote.css" rel="stylesheet">
	<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.5.10/summernote.min.js"></script>
	
	<!-- mask -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/0.9.0/jquery.mask.min.js"></script>
	
	<!-- select2 -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2-bootstrap.min.css"/>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2_locale_it.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/stupidtable/0.0.1/stupidtable.min.js"></script>
	
	<script src="/app/admin/_assets/scripts/app-min.js" charset="utf-8"></script>

</head>
<body>
	
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
		
			<div class="navbar-header">
				<a class="navbar-brand" href="/admin">Amministrazione <?=siteName?></a>
			</div>
		
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/"><i class="fa fa-home"></i> Home page</a></li>
				<li><a href="/admin/logout"><i class="fa fa-sign-out"></i> Log out</a></li>
			</ul>
				
		</div>
	</nav>
	
	<div class="container-fluid">
		
		<div class="row">
			
			<aside class="col-sm-2">
				
				<div class="list-group">						
				<? foreach($app_data['apps'] as $app=>$items) : ?>
				
					<? if($items == 'break') : ?>
					
				</div>
				<div class="list-group">
							
					<? else : ?>
				
					<a href="/<?=$app?>/admin/" class="list-group-item <?php if($currentApp==$app) echo 'active'; ?>">
						<? if($items['count']){ ?>
							<span class="badge"><?=LoadClass($app, 1)->filter('COUNT')?></span>
						<? } ?>
						<i class="fa fa-fw fa-<?=$items['icon']?>"></i> <?=ucfirst($items['label'])?>
					</a>
				
					<? endif; ?>
				
				<? endforeach; ?>
				</div>
				
			</aside>
			
			<div class="col-sm-10">
				
				<?php if($_GET['update']): ?>
					<div class="alert alert-top alert-success" role="alert">Operazione effettuata con successo</div>
				<?php elseif($_GET['delete']): ?>
					<div class="alert alert-top alert-danger" role="alert">Eliminazione effettuata con successo</div>
				<?php endif; ?>
				