<? $app_data['apps'] = LoadClass('admin', 1)->app_data['apps']; ?>

<div class="page-header">
	<h1>Benvenuto</h1>
</div>

<p class="lead">Benvenuto nel pannello di controllo</p>

<!-- div class="alert alert-warning">Ricordati di aggiungere le nuove app via /app/admin/admin.lib.php</div -->

<div class="row indexapp">
		
	<?php 
		
		foreach($app_data['apps'] as $app=>$items){ 
			
			if($items != 'break'){ 
				
	?>
	
		<div class="col-sm-3 text-center">
			<div class="panel panel-default">
				<div class="panel-body">
  
					<a href="/<?=$app?>/admin/">
						<i class="fa fa-fw fa-3x fa-<?=$items['icon']?>"></i>
						<br>
						<?=ucfirst($items['label'])?>
					</a>
				
				</div>
			</div>
		</div>
		
	<?php
		
			}
		}
		
	?>

</div>
