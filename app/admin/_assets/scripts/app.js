(function($){

	var script = {};
	
	
	
	/*-----------------------------------------------------------------------------------*/
	/*	Define Functions
	/*-----------------------------------------------------------------------------------*/
	
	script.tltp = function () {
		
		$('[data-toggle="tooltip"]').tooltip();
	
	};
		
	script.deletecheck = function(){
		
		$('input[name="delete"]').change(function() {
			
			if($('input[name="delete"]').is(':checked')){
				
				$('button[type="submit"]:not(.extra)').removeClass('btn-default').addClass('btn-danger').html('Elimina');
				
			}else{
				
				$('button[type="submit"]:not(.extra)').removeClass('btn-danger').addClass('btn-default').html('Modifica');
			
			}
		});
	
	};
	
	script.validform = function(){
		
		$('form').bootstrapValidator({
			framework: 'bootstrap',
			live: 'disabled',
			locale: 'it_IT',
			feedbackIcons: {
				valid: 		'glyphicon glyphicon-ok',
				invalid: 	'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			}
		})
		.on('success.form.bv', function() {	
			
			$(this).append('<div class="attendere"> <i class="fa fa-refresh fa-spin fa-5x"></i> </div>');
		
		});
				
	};
	
	script.txteditor = function(){
			
		$(document).ready(function() {
			$('.summernote').summernote({
				height: 300,                 // set editor height
			});
		});
	
	};
	
	script.inputdate = function(){
		$('input.datetime').mask('00/00/0000 00:00:00');
		$('input.date').mask('00/00/0000');
	};
	
	script.alerttop = function(){
		
		$('.alert-top').delay(2000).slideUp();
		
	};
	
	script.selectform = function(){
		
		$('select:not(.noserach)').select2();
		
	};
	
	script.stpdtbl = function(){
		
		$('.table-stupidtable').stupidtable();
		
	}
	
	/*-----------------------------------------------------------------------------------*/
	/*	Start Functions
	/*-----------------------------------------------------------------------------------*/
	
	$(window).load(function(){ });
	
	$(document).ready(function() {
	
		script.tltp();
		script.deletecheck();
		script.validform();
		script.txteditor();
		script.inputdate();
		script.alerttop();
		script.selectform();
		script.stpdtbl();
		
	});
	
})(jQuery);
