<?php

### GYURAL ###

/*

----------
{{app}} {{ctrl.clean}}
----------

Filename: /app/{{app}}/_/{{ctrl}}.ctrl.php
 Version: 0.1
  Author: {{user}} <{{email}}>
    Date: {{time}}
	
*/

class {{ctrl}}Ctrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	// api di richiamo di tutti gli oggetti dell'app
	function apiGets($args = null){
		
		return LoadClass('{{ctrl}}', 1)->filter_array($args);
		
	}
	
	// api di richiamo del singolo oggetto dell'app
	function apiGet($id){
		
		if($_REQUEST['id']){
			$id = $_REQUEST['id'];
		}
		
		if($id){
			return LoadClass('{{ctrl}}', 1)->get($id);
		}else{
			return false;
		}
		
	}
	
}

?>