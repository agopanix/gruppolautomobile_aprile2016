<?php

### GYURAL ###

/*

----------
{{app}} {{ctrl.clean}}
----------

Filename: /app/{{app}}/_/{{ctrl}}_admin.ctrl.php
 Version: 0.1
  Author: {{user}} <{{email}}>
    Date: {{time}}
	
*/

class {{ctrl}}_adminCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	// vista per la lista in admin
	function getIndex(){
				
		Application('admin/_v/header', null, $app_data);
		Application('{{ctrl}}/_v/index', null, $app_data);
		Application('admin/_v/footer', null, $app_data);
		
	}
	
	// vista per il form creazione/modifica in admin
	function getForm(){
		
		Application('admin/_v/header', null, $app_data);
		Application('{{ctrl}}/_v/form', null, $app_data);
		Application('admin/_v/footer', null, $app_data);
		
	}
	
	// post di modifica o creazione o cancellazione dell'oggetto
	function postForm(){
		
		//post
		
		if( $_POST['{{id}}'] and $_POST['delete'] ){
			
			//delete item
			if( !$item = LoadClass('{{app}}', 1)->get($_POST['{{id}}']) ) exit('Errore load');			
			if( !$item->deleteExecute() ) exit('Errore delete');
						
			CallFunction('hooks', 'get', 'delete.end', $item);
			
			header('Location: /{{app}}/admin/delete:1');
			exit;
			
		}elseif( $_POST['{{id}}'] ){
			
			//update item
			if( !$item = LoadClass('{{app}}', 1)->get($_POST['{{id}}']) ) exit('Errore load');
		
		} else {
						
			//create item
			$item = LoadClass('{{app}}', 1);
		
		}
		
		$item->refill($_POST);
		
		// execution
		if( $_POST['{{id}}'] ){
			
			if( !$item->putExecute() ) exit('Errore put');
		
		} else {
						
			if( !$item->hangExecute() ) exit('Errore hang');
		
		}
		
		CallFunction('hooks', 'get', 'post.end', $item);
		
		header('Location: /{{app}}/admin/form/id:'.$item->{{id}}.'/update:1');
		
	}
	
}

?>