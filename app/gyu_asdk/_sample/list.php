<div class="page-header">
	<ul class="list-inline pull-right">
		<li><a class="btn btn-default" href="/{{app}}/admin/form/"><i class="fa fa-cloud-upload"></i> Aggiungi</a></li>
	</ul>
	
	<h1>{{app}}</h1>
</div>

<? 
					
$count 			= LoadClass('{{app}}', 1)->filter('COUNT');

$perPage 		= 40;
$maxPage 		= ceil($count / $perPage);
$currentPage 	= ($_GET['p']) ? $_GET['p'] : 1;
$startItem 		= $perPage*($currentPage-1);
	
$items 			= LoadClass('{{app}}', 1)->filter( array('LIMIT', $startItem, $perPage) );

if( $items ) : 

?>

	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Titolo</th>
				<th>Inserimento</th>
				<th>Aggiornamento</th>
				<th>Visualizza</th>
			</tr>
		</thead>
		<tbody>	
	
			<?php foreach( $items as $item ) : ?>
			
			<tr>
				<td>
					<?=$item->{{id}}?>
				</td>
				<td>
					<a href="/{{app}}/admin/form/id:<?=$item->{{id}}?>"><?=$item->title?></a>
				</td>
				<td>
					<?=date('d/m/Y H:i', $item->creationTime)?>
				</td>
				<td>
					<?=date('d/m/Y H:i', $item->gyu_updateTime)?>
				</td>
				<td>
					<a href="<?=$item->get_permalink()?>">Visualizza</a>
				</td>
			</tr>
			
			<?php endforeach; ?>
			
		</tbody>
	</table>	
	
	<?php if( $count > 0 ) CallFunction('admin', 'pagination', $currentPage, $maxPage, '/{{app}}/admin'); ?>

<? else: ?>

	<div class="alert alert-info">
		Ancora nessun elemento inserito...
	</div>
	
<? endif; ?>				