<?php

### GYURAL ###

/*

----------
{{app}} {{ctrl.clean}}
----------

Filename: /app/{{app}}/_/{{ctrl}}.lib.php
 Version: 0.1
  Author: {{user}} <{{email}}>
    Date: {{time}}
	
*/

class {{ctrl}} extends standardObject {
	
	var $gyu_table	= '{{ctrl}}';
	var $gyu_id 	= '{{id}}';

	function __construct() {
			
		if( $this->{{id}} ){
		
			$this->gyu_updateTime = $this->updateTime;
			
			CallFunction('hooks', 'get', 'get.end', $this);
		
		}else{
		
			$this->creationTime = time();
		
		}
		
		$this->updateTime = time();
		
	}
	
	function get_permalink(){
		
		if( $this->{{id}} )
			return '/frontend/{{ctrl}}/id:'.$this->{{id}};
		else
			return false;
	}

}


?>