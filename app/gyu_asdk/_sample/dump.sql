
DROP TABLE IF EXISTS `{{prefix}}{{app}}`;

CREATE TABLE `{{prefix}}{{app}}` (
  `{{id}}` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `creationTime` int(11) DEFAULT NULL,
  `updateTime` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`{{id}}`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;