<?php

### GYURAL ###

/*

----------
frontend 
----------

Filename: /app/frontend/_/frontend_{{app}}.ctrl.php
 Version: 0.1
  Author: Andrea Rufo <orangedropdesign@gmail.com>
    Date: 14/07/2015
	
*/

class frontend_{{app}}Ctrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	function getIndex(){
		
		if( isset($_GET['id']) ){
			
			$app_data['item'] = LoadApp('{{app}}', 1)->ApiGet( $_GET['id'] );
			
			$app_data['head']['title'] 			= $app_data['item']->getAttr('title');
			$app_data['head']['description'] 	= $app_data['item']->getAttr('description');
			
			$template = '{{app}}-single';
			
		}else{
			
			$app_data['item'] = LoadApp('{{app}}', 1)->ApiGets( );
			
			$template = '{{app}}-archive';
		
		}
		
		Application('frontend/_v/header', null, $app_data);
		Application('frontend/_v/'.$template, null, $app_data);
		Application('frontend/_v/footer', null, $app_data);
		
	}
	
}

?>
