<?php

### GYURAL ###

/*

----------
Gyural 1.9.1
----------

Filename: /app/gyu_asdk/_/gyu_asdk.ctrl.php
 Version: 1.9
  Author: Andrea Rufo <orangedropdesign@gmail.com>
    Date: 06/08/2015
	
*/

class gyu_asdkCtrl extends standardController {
	
	var $index_tollerant = true;
	var $format;
	
	function __construct() {
		
		if(!dev){
			die('Enable DEV Mode.');
		}
			
		if(!function_exists('deb_log')) {
			exit('Debug Repository is needed!');
			return false;
		}
	
	}

	function ApiExists($appName) {
		
		if(!$appName)
			$appName = $_REQUEST['appName'];

		if(isApplication($appName))
			return false;
		else
			return true;

	}
	
	function GetIndex() {

		Application('gyu_asdk/_v/asdk');	

	}

	function PostIndex() {
		
		// controllo se l'app non esiste gia
		if( isApplication($_POST['appName']) ) {
			
			header('Location: /gyu_asdk/ext:1/post:'.serialize($_POST));
			exit;
			
		}
		
		// controllo se esistono le app standard che ci servono
		if( !isApplication('admin') or !isApplication('frontend') ) {
			
			header('Location: /gyu_asdk/ext:2/post:'.serialize($_POST));
			exit;
			
		}
		
		$log[] = print_r($_POST, 1);
		
		// controllo se e` stato selezionato qualcosa da creare
		if( empty($_POST['global']) ){
			
			header('Location: /gyu_asdk/ext:3/post:'.serialize($_POST));
			exit;
			
		}
		
		//stabilisco i rimpiazzi da fare nei _sample	
		$replaces['{{app}}'] 			= $_POST['appName'];
		$replaces['{{ctrl}}'] 			= $_POST['appName'];
		$replaces['{{id}}'] 			= ( $_POST['appSingular'] != '' ) ? $_POST['appSingular'].'_id' : 'id';
		$replaces['{{ctrl.clean}}'] 	= '';
		$replaces['{{time}}'] 			= date('d/m/Y');
		$replaces['{{user}}'] 			= $_POST['user'];
		$replaces['{{email}}'] 			= $_POST['email'];
		$replaces['{{pid}}'] 			= md5(time() * rand(0,100));
		$replaces['{{prefix}}']			= tablesPrefix; // from /sys/version.json
		
		$log[] = print_r($replaces, 1);
		
		// creo le chiavi di rimpiazzo.
		$keys = array_keys($replaces);
		
		$log[] = print_r($keys, 1);
		
		// tutti i template selezionabili
		$und = array(
					
			'ctrl'	,
			'admin'	,
			'lib'	,
			'funcs'	,   
			'hooks'	,   
			'list'	,   
			'edit'	,
			
			'fectrl',
			'fearc'	,
			'fesin'	,
			
		);
		
		// prendo il contenuto dei _sample php
		foreach( $und as $tmp ){
			
			$templates[$tmp] 	= file_get_contents(app . '/gyu_asdk/_sample/'.$tmp.'.php');
			
		}
		
		// prendo il contenuto del dump db
		$templates['dump']		= file_get_contents(app . '/gyu_asdk/_sample/dump.sql');
		
		// version
		$templates['version']	= file_get_contents(app . '/gyu_asdk/_sample/version.gapp');
		
		$log[] = print_r($templates, 1);
		
		//creo la dir principale dell'app
		$base = application . $_POST['appName'];
		
		$log[] = print_r($base, 1);
				
		if( !mkdir($base)			) exit('Errore cartella base');
		if( !mkdir($base . '/_/')	) exit('Errore cartella _');
		if( !mkdir($base . '/_v/')	) exit('Errore cartella _v');
				
		/*	'ctrl'	,	'lib'	,	'funcs'	,   'hooks'	,   'list'	,   'edit'	,	'fectrl',	'fearc'	,	'fesin'	,	*/
			
		// creo i file php della cartella _
		$underscore = array('ctrl', 'lib', 'funcs', 'hooks');
		foreach( $underscore as $u ){		
			if($_POST['global'][$u]){
				$file = array($replaces['{{app}}'].'.'.$u.'.php', str_replace($keys, $replaces, $templates[$u]));
				if( !file_put_contents($base.'/_/'.$file[0], $file[1]) ) exit('Errore '.$file[0].'!');
				$log[] = print_r('Creato file '.$file[0], 1);
			}
		}
		if($_POST['global']['admin']){
			$file = array($replaces['{{app}}'].'_admin.ctrl.php', str_replace($keys, $replaces, $templates['admin']));
			if( !file_put_contents($base.'/_/'.$file[0], $file[1]) ) exit('Errore '.$file[0].'!');
			$log[] = print_r('Creato file '.$file[0], 1);
		}
		
		// creo i file php della cartella _v
		if($_POST['global']['list']){
			$file = array('index.php', str_replace($keys, $replaces, $templates['list']));
			if( !file_put_contents($base.'/_v/'.$file[0], $file[1]) ) exit('Errore '.$file[0].'!');
			$log[] = print_r('Creato file '.$file[0], 1);
		}
		if($_POST['global']['edit']){
			$file = array('form.php', str_replace($keys, $replaces, $templates['edit']));
			if( !file_put_contents($base.'/_v/'.$file[0], $file[1]) ) exit('Errore '.$file[0].'!');
			$log[] = print_r('Creato file '.$file[0], 1);
		}
		
		// creo i file del frontend
		if($_POST['global']['fectrl']){
			$file = array('frontend_'.$replaces['{{app}}'].'.ctrl.php', str_replace($keys, $replaces, $templates['fectrl']));
			if( !file_put_contents(application.'frontend/_/'.$file[0], $file[1]) ) exit('Errore '.$file[0].'!');
			$log[] = print_r('Creato file '.$file[0], 1);
		}
		if($_POST['global']['fearc']){
			$file = array($replaces['{{app}}'].'-archive.php', str_replace($keys, $replaces, $templates['fearc']));
			if( !file_put_contents(application.'frontend/_v/'.$file[0], $file[1]) ) exit('Errore '.$file[0].'!');
			$log[] = print_r('Creato file '.$file[0], 1);
		}
		if($_POST['global']['fesin']){
			$file = array($replaces['{{app}}'].'-single.php', str_replace($keys, $replaces, $templates['fesin']));
			if( !file_put_contents(application.'frontend/_v/'.$file[0], $file[1]) ) exit('Errore '.$file[0].'!');
			$log[] = print_r('Creato file '.$file[0], 1);
		}
		
		// scrivo il db
		if($_POST['global']['dump']){
			$dump = str_replace($keys, $replaces, $templates['dump']);
			if( !Database()->multi_query( $dump ) ) exit('Errore dump!');
			$log[] = print_r('Database dump eseguito', 1);
		}
		
		// version
		$file = array('version.gapp', str_replace($keys, $replaces, $templates['version']));
		if( !file_put_contents($base.'/_/'.$file[0], $file[1]) ) exit('Errore version!');
		$log[] = print_r('Creato file '.$file[0], 1);
		
		// fine log e lo salvo
		$log[] = print_r('Done', 1);
		file_put_contents( $base.'/_/'.$replaces['{{app}}'].'.log', implode("\n\n", $log) );
		
		// torna a casa
		header('Location: /gyu_asdk/done:1');

	}

}

?>