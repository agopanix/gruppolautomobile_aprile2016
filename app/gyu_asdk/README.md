# Gyural aSDK

## For Gyural 1.9.1

An advanced SDK for Gyural.

Basa la truttura dei file sugli standard definiti nelle _Gyural Think Talk_ e quindi funziona solo insieme alle app `admin` e `frontend`.

Permette la creazione delle classi, controllori e librerie, oltre che delle viste per backend e controllore e viste base per il frontend.