<!DOCTYPE html>
<!--[if lt IE 7]>	   <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>		   <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>		   <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Advanced SDK for Gyural">	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Gyu aSDK</title>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/headjs/1.0.3/head.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.5.1/less.min.js" type="text/javascript"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/yeti/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	
	<style type="text/css" media="screen">
		pre{
		 	display: none;
		}
	</style>

</head>
<body>

	<!--[if lt IE 7]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	
	<div class="container">
		
		<div class="well">
			
			<div class="row">
				<div class="col-sm-6">
					
					<h1 class="text-center">Gyural <? echo version; ?></h1>
					<p class="lead text-center">aSDK is the <em>advanced SDK</em>, ver. 1.9</p>
			
				</div>
				<div class="col-sm-6">
			
					<p>L'<strong>aSDK</strong> aiuta nella creazione di <code>app</code> gyural secondo lo standard stabilito durante le <em>Gyural Think Talk</em>.</p>
					<p>Secondo standard nella cartella propria dell'app verranno inseriti solo i file necessari per fornire metodi API al frontend e quindi le viste per l'app standard di pannello di controllo <code>admin</code>. È possibile caricare anche un dump del database.</p>
					<p>È possibile creare anche il controllore specifico per l'app standard <code>frontend</code> e le viste per l'archivio e il singolo elemento.</p>
			
				</div>
			</div>
	
		</div>
	
		<? if($_GET['done']):?>
		
			<div class="alert alert-success alert-lg text-center" role="alert">Nuova applicazione creata con successo: <em>hokidoky!</em></div>
		
		<? elseif($_GET['ext'] == 1):?>
		
			<div class="alert alert-danger alert-lg text-center" role="alert">L'applicazione che stai creando esiste già. Prova un altro nome o controlla quella esistente.</div>
		
		<? elseif($_GET['ext'] == 2):?>
		
			<div class="alert alert-warning alert-lg text-center" role="alert">Per poter usare aSDK sono necessarie le app standard admin e frontend: non ci sembra di vederle!</div>
		
		<? elseif($_GET['ext'] == 3):?>
		
			<div class="alert alert-warning alert-lg text-center" role="alert">What a f***k! Non hai selezionato nulla da creare!</div>
		
		<? endif; ?>
			
		<form role="form" method="post">
			
			<div class="row">
				<div class="col-sm-6">
					
					<div class="form-group">
						<label for="user">Il tuo nome</label>
						<input type="text" class="form-control" name="user" required placeholder="Jon Doe">
					</div>
				
					<div class="form-group">
						<label for="email">La tua email</label>
						<input type="email" class="form-control" name="email" required placeholder="j.doe@mandarinoadv.com">
					</div>
					
					<div class="form-group">
						<label for="appName">Nome dell'app</label>
						<input type="text" class="form-control pl" name="appName" required placeholder="pages">
					</div>
					
					<div class="form-group">
						<label for="appName">Nome singolare (usato per l'id della tabella)</label>
						<input type="text" class="form-control sn" name="appSingular" required placeholder="page">
					</div>
						
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">
							<i class="fa fa-hand-spock-o"></i> Long life and prosperity!
						</button>
					</div>
					
					<p><em>Nota bene: ricorda di aggiungere la tua app su <code>/app/admin/_/admin.lib.php</code> per visualizzarla nel menu backend.</em></p>
				
				</div>
				<div class="col-sm-6">
					<? 
						
						$und = array(
						
							'ctrl'		=>	'/app/<span>examples</span>/_/<span>examples</span>.ctrl.php',
							'admin'		=>	'/app/<span>examples</span>/_/<span>examples</span>_admin.ctrl.php',
							'lib'		=>	'/app/<span>examples</span>/_/<span>examples</span>.lib.php',
							'funcs'		=>	'/app/<span>examples</span>/_/<span>examples</span>.funcs.php',
							'hooks'		=>	'/app/<span>examples</span>/_/<span>examples</span>.hooks.php', 
							'list'		=>	'/app/<span>examples</span>/_v/index.php', 
							'edit'		=>	'/app/<span>examples</span>/_v/form.php', 
							'fectrl'	=>	'/app/frontend/_/frontend_<span>examples</span>.ctrl.php',
							'fearc'		=>	'/app/frontend/_v/<span>examples</span>-archive.php',
							'fesin'		=>	'/app/frontend/_v/<span>examples</span>-single.php',
							'dump'		=>	tablesPrefix.'<span>examples</span> database table with <code>id</code>',
					
						);
						
					?>
						
					<ul class="three">
						<? foreach( $und as $key => $item) : ?>
						
							<li>
								<div class="checkbox">
									<label>
										<input checked type="checkbox" name="global[<?=$key?>]" value="1" />
										<?=$item?>
									</label>
								</div>
							</li>
						
						<? endforeach; ?>
					</ul>
						
				</div>
			</div>
			
		</form>
		
		<hr>
		
		<p class="text-center">
			Made with <i class="fa fa-code text-warning"></i> and a little bit of <i class="fa fa-heart text-danger"></i> by Andrea Rufo<br>
			Respect for the <a href="/gyu_sdk/">original SDK</a>
		</p>
	
		
	</div> <!-- container -->
		
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
	
	<script>
		
		$('input.pl').keyup(function(){
			
			$('.three span').html(this.value);
			
		});
		
		$('input.sn').keyup(function(){
			
			$('.three code').html(this.value+'_id');
			
		})
		
	</script>
	
</body>
</html>
 