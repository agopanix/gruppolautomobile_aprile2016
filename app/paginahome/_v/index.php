<div class="page-header">
	
	
	<h1>Pagina Home</h1>
</div>

<? 
					
$count 			= LoadClass('paginahome', 1)->filter('COUNT');

$perPage 		= 40;
$maxPage 		= ceil($count / $perPage);
$currentPage 	= ($_GET['p']) ? $_GET['p'] : 1;
$startItem 		= $perPage*($currentPage-1);
	
$items 			= LoadClass('paginahome', 1)->filter( array('LIMIT', $startItem, $perPage) );

if( $items ) : 

?>

	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Titolo</th>
				<th>Inserimento</th>
				<th>Aggiornamento</th>
				<th>Visualizza</th>
			</tr>
		</thead>
		<tbody>	
	
			<?php foreach( $items as $item ) : ?>
			
			<tr>
				<td>
					<?=$item->paginahome_id?>
				</td>
				<td>
					<a href="/paginahome/admin/form/id:<?=$item->paginahome_id?>"><?=$item->title?></a>
				</td>
				<td>
					<?=date('d/m/Y H:i', $item->creationTime)?>
				</td>
				<td>
					<?=date('d/m/Y H:i', $item->gyu_updateTime)?>
				</td>
				<td>
					<a href="/paginahome/admin/form/id:<?=$item->paginahome_id?>">Visualizza</a>
				</td>
			</tr>
			
			<?php endforeach; ?>
			
		</tbody>
	</table>	
	
	<?php if( $count > 0 ) CallFunction('admin', 'pagination', $currentPage, $maxPage, '/paginahome/admin'); ?>

<? else: ?>
	<ul class="list-inline pull-right">
		<li><a class="btn btn-default" href="/paginahome/admin/form/"><i class="fa fa-cloud-upload"></i> Aggiungi</a></li>
	</ul>
	<div class="alert alert-info">
		Ancora nessun elemento inserito...
	</div>
	
<? endif; ?>				