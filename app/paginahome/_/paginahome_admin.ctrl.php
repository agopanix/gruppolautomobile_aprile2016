<?php

### GYURAL ###

/*

----------
paginahome 
----------

Filename: /app/paginahome/_/paginahome_admin.ctrl.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 11/11/2015
	
*/

class paginahome_adminCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	// vista per la lista in admin
	function getIndex(){
				
		Application('admin/_v/header', null, $app_data);
		Application('paginahome/_v/index', null, $app_data);
		Application('admin/_v/footer', null, $app_data);
		
	}
	
	// vista per il form creazione/modifica in admin
	function getForm(){
		
		Application('admin/_v/header', null, $app_data);
		Application('paginahome/_v/form', null, $app_data);
		Application('admin/_v/footer', null, $app_data);
		
	}
	
	// post di modifica o creazione o cancellazione dell'oggetto
	function postForm(){
		
		//post
		
		if( $_POST['paginahome_id'] and $_POST['delete'] ){
			
			//delete item
			if( !$item = LoadClass('paginahome', 1)->get($_POST['paginahome_id']) ) exit('Errore load');			
			if( !$item->deleteExecute() ) exit('Errore delete');
						
			CallFunction('hooks', 'get', 'delete.end', $item);
			
			header('Location: /paginahome/admin/delete:1');
			exit;
			
		}elseif( $_POST['paginahome_id'] ){
			
			//update item
			if( !$item = LoadClass('paginahome', 1)->get($_POST['paginahome_id']) ) exit('Errore load');
		
		} else {
						
			//create item
			$item = LoadClass('paginahome', 1);
		
		}
		
		$item->refill($_POST);
		
		// execution
		if( $_POST['paginahome_id'] ){
			
			if( !$item->putExecute() ) exit('Errore put');
		
		} else {
						
			if( !$item->hangExecute() ) exit('Errore hang');
		
		}
		
		CallFunction('hooks', 'get', 'post.end', $item);
		
		header('Location: /paginahome/admin/form/id:'.$item->paginahome_id.'/update:1');
		
	}
	
}

?>