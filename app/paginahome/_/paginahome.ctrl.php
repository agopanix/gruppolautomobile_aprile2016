<?php

### GYURAL ###

/*

----------
paginahome 
----------

Filename: /app/paginahome/_/paginahome.ctrl.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 11/11/2015
	
*/

class paginahomeCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	// api di richiamo di tutti gli oggetti dell'app
	function apiGets($args = null){
		
		return LoadClass('paginahome', 1)->filter_array($args);
		
	}
	
	// api di richiamo del singolo oggetto dell'app
	function apiGet($id){
		
		if($_REQUEST['id']){
			$id = $_REQUEST['id'];
		}
		
		if($id){
			return LoadClass('paginahome', 1)->get($id);
		}else{
			return false;
		}
		
	}
	
}

?>