<?php

### GYURAL ###

/*

----------
paginahome 
----------

Filename: /app/paginahome/_/paginahome.lib.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 11/11/2015
	
*/

class paginahome extends standardObject {
	
	var $gyu_table	= 'paginahome';
	var $gyu_id 	= 'paginahome_id';

	function __construct() {
			
		if( $this->paginahome_id ){
		
			$this->gyu_updateTime = $this->updateTime;
			
			CallFunction('hooks', 'get', 'get.end', $this);
		
		}else{
		
			$this->creationTime = time();
		
		}
		
		$this->updateTime = time();
		
	}
	
	function get_permalink(){
		
		if( $this->paginahome_id )
			return '/frontend/paginahome/id:'.$this->paginahome_id;
		else
			return false;
	}

}


?>