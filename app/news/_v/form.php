<?php
	
	if( isset($_GET['id']) ){
		$item = LoadApp('news', 1)->ApiGet($_GET['id']);
		$title = 'Modifica';
	}else{
		$item = LoadClass('news', 1);
		$title = 'Aggiungi';
	}
?>

<h1><?=$title?></h1>

<form method="post" enctype="multipart/form-data">

	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
	  <li class="active"><a href="#ita" aria-controls="home" role="tab" data-toggle="tab">Italiano</a></li>
	  <li><a href="#eng" aria-controls="profile" role="tab" data-toggle="tab">English</a></li>
	</ul>

	<!-- Tabs -->
	<div class="tab-content">
	    <div role="tabpanel" class="tab-pane active" id="ita">
	
			<div class="form-group">
				<label for="title">Titolo</label>
				<input class="form-control input-lg" type="text" required name="title" value="<?=$item->title?>"/>
			</div>
			
			<div class="form-group">
				<label for="content">Contenuto</label>
				<textarea class="form-control summernote" rows="4" name="content"><?=$item->content?></textarea>
			</div>

			
		</div>

		<div role="tabpanel" class="tab-pane" id="eng">
		    
		    <div class="form-group">
				<label for="title">Titolo inglese</label>
				<input class="form-control input-lg" type="text" required name="title_en" value="<?=$item->title_en?>"/>
			</div>
			
			<div class="form-group">
				<label for="content">Contenuto inglese</label>
				<textarea class="form-control summernote" rows="4" name="content_en"><?=$item->content_en?></textarea>
			</div>
		    
	    </div>
	</div>

	<!-- end tabs -->
	
	<hr>
		
	<div class="well">
		<div class="form-group">
			<label for="img">Immagine in evidenza</label>
			<input type="file" class="form-control" name="img">
			<?php 
					
				if($item->img){
					echo 'Anteprima: <br><img src="/upl/'.$item->img.'" style="width:25%;">';
				}
			
			?>
		</div>
	</div>
		
	<? if( isset($item->news_id) ): ?>
	
	<div class="row">
		<div class="col-sm-3">
			
			<button type="submit" class="btn btn-default btn-block">Modifica</button>
			
		</div>
		<div class="col-sm-9 text-right">
			
			<input type="hidden" name="news_id" value="<?=$item->news_id?>" />
			<div class="checkbox">
				<label><input type="checkbox" name="delete" value="1" /> Elimina definitivamente l'inserimento</label>
			</div>
			
		</div>
	</div>
	
	<? else: ?>
	
	<div class="row">
		<div class="col-sm-3">
			
			<button type="submit" class="btn btn-default btn-block">Inserisci</button>
	
		</div>
	</div>
	
	<? endif; ?>
		
	<div class="clearfix margin-40"></div>
	
	<? CallFunction('hooks', 'get', 'form.end', $this); ?>
		
</form>