<?php

### GYURAL ###

/*

----------
news 
----------

Filename: /app/news/_/news.lib.php
 Version: 0.1
  Author: agopanix <a.panizzoli@mandarinoadv.com>
    Date: 21/08/2015
	
*/

class news extends standardObject {
	
	var $gyu_table	= 'news';
	var $gyu_id 	= 'news_id';

	function __construct() {
			
		if( $this->id ){
		
			$this->gyu_updateTime = $this->updateTime;
			
			CallFunction('hooks', 'get', 'get.end', $this);
		
		}else{
		
			$this->creationTime = time();
		
		}
		
		$this->updateTime = time();
		
	}
	
	function get_permalink(){
		
		if( $this->news_id )
			return '/frontend/news/id:'.$this->news_id;
		else
			return false;
	}

	function getImg(){
		
		if( $this->has_img() )
			return '/'.uploadPath. $this->img;
		else
			return false;
		
	}

	function getTitle(){
		if($_SESSION["lang"]=="en"){
			return $this->title_en;
		}else{
			return $this->title;
		}
	}

	function getContent(){
		if($_SESSION["lang"]=="en"){
			return $this->content_en;
		}else{
			return $this->content;
		}
	}
	
	function getExcerpt($lenght = 120){
		
		if( $this->excerpt ){
			if($_SESSION["lang"]=="en"){
				return $this->excerpt_en;
			}else{
				return $this->excerpt;
			}
		}else{ 
			if($_SESSION["lang"]=="en"){
				return CallFunction('strings', 'taglia', $this->content_en, $lenght);
			}else{
				return CallFunction('strings', 'taglia', $this->content, $lenght);
			}
		}	
	}
	
	function getBodyclass(){
		
		return 'news news-single news-id-'.$this->news_id;
		
	}
	
	// funzioni di utilità
	
	function has_img(){
		
		if( $this->img )
			return true;
		else 
			return false;
			
	}
	
	function getMenuActive(){
		
		return $this->gyu_fields['menuactive'];
		
	}

	

}


?>