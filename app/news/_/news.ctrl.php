<?php

### GYURAL ###

/*

----------
news 
----------

Filename: /app/news/_/news.ctrl.php
 Version: 0.1
  Author: agopanix <a.panizzoli@mandarinoadv.com>
    Date: 21/08/2015
	
*/

class newsCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	// api di richiamo di tutti gli oggetti dell'app
	function apiGets($args = null){
		
		return LoadClass('news', 1)->filter_array($args);
		
	}
	
	// api di richiamo del singolo oggetto dell'app
	function apiGet($id){
		
		if($_REQUEST['id']){
			$id = $_REQUEST['id'];
		}
		
		if($id){
			return LoadClass('news', 1)->get($id);
		}else{
			return false;
		}
		
	}
	
}

?>