<?php

### GYURAL ###

/*

----------
news 
----------

Filename: /app/news/_/news_admin.ctrl.php
 Version: 0.1
  Author: agopanix <a.panizzoli@mandarinoadv.com>
    Date: 21/08/2015
	
*/

class news_adminCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	// vista per la lista in admin
	function getIndex(){
				
		Application('admin/_v/header', null, $app_data);
		Application('news/_v/index', null, $app_data);
		Application('admin/_v/footer', null, $app_data);
		
	}
	
	// vista per il form creazione/modifica in admin
	function getForm(){
		
		Application('admin/_v/header', null, $app_data);
		Application('news/_v/form', null, $app_data);
		Application('admin/_v/footer', null, $app_data);
		
	}
	
	// post di modifica o creazione o cancellazione dell'oggetto
	function postForm(){
		
		//post
		
		if( $_POST['news_id'] and $_POST['delete'] ){
			
			//delete item
			if( !$item = LoadClass('news', 1)->get($_POST['news_id']) ) exit('Errore load');			
			if( !$item->deleteExecute() ) exit('Errore delete');
						
			CallFunction('hooks', 'get', 'delete.end', $item);
			
			header('Location: /news/admin/delete:1');
			exit;
			
		}elseif( $_POST['news_id'] ){
			
			//update item
			if( !$item = LoadClass('news', 1)->get($_POST['news_id']) ) exit('Errore load');
		
		} else {
						
			//create item
			$item = LoadClass('news', 1);
		
		}

		/** inizio codice per caricare img **/
		//echo upload;
		//die();
		$arr = false;
		if(is_file($_FILES["img"]["tmp_name"])) {
			$porzioni = explode('.', $_FILES["img"]["name"]);
			$nuovoFile = str_replace($porzioni[count($porzioni)-1], '', $_FILES["img"]["name"]) . time() . '.' . $porzioni[count($porzioni)-1];

			if(copy($_FILES["img"]["tmp_name"], upload . $nuovoFile))
			{
				$arr = $nuovoFile;
				list($width, $height, $type, $attr) = getimagesize(upload . $nuovoFile);
				// echo $width.' - '.$height.' - '.$type.' - '.$attr;
				/**
				if($width > $height)
				{
					$oriental = 'H';
				}
				else
				{
					$oriental = 'V';
				}
				**/
				
			}else{
				echo 'errore else';
				die();
			}

		}

		if($arr){
			$item->setAttr('img', $arr);
		}
		
		/** fine codice per caricare img **/
		
		$item->refill($_POST);
		
		// execution
		if( $_POST['news_id'] ){
			
			if( !$item->putExecute() ) exit('Errore put');
		
		} else {
						
			if( !$item->hangExecute() ) exit('Errore hang');
		
		}
		
		CallFunction('hooks', 'get', 'post.end', $item);
		
		header('Location: /news/admin/form/id:'.$item->news_id.'/update:1');
		
	}
	
}

?>