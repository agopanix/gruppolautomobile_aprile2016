<?php

### GYURAL ###

/*

----------
wishlists 
----------

Filename: /app/wishlists/_/wishlists.lib.php
 Version: 0.1
  Author: Andrea Rufo <a.rufo@mandarinoadv.com>
    Date: 12/11/2015
	
*/

class wishlists extends standardObject {
	
	var $gyu_table	= 'wishlists';
	var $gyu_id 	= 'wishlist_id';

	function __construct() {
			
		if( $this->wishlist_id ){
		
			$this->gyu_updateTime = $this->updateTime;
			
			CallFunction('hooks', 'get', 'get.end', $this);
		
		}else{
		
			$this->creationTime = time();
		
		}
		
		$this->updateTime = time();
		
	}
	
	function getId(){
	
		return $this->{$this->gyu_id};
	
	}
	
	function get_permalink(){
		
		if( $this->wishlist_id )
			return '/frontend/wishlists/id:'.$this->wishlist_id;
		else
			return false;
	}
	
	// funzioni di utilità immagine
	
	function getImg(){
		
		if( $this->has_img() )
			return '/'.uploadPath. $this->img;
		else
			return false;
		
	}
	
	function has_img(){
		
		if( $this->img )
			return true;
		else 
			return false;
			
	}
	
	/*
		restituisce una immagine già in html con tutti i dati passati nell'array $args:
		$args = array(
			'w' 	=> larghezza,
			'h' 	=> altezza,
			'zc'	=> zoom/crop
			'class' => class di img
			'alt' 	=> testo alt
			'title' => titolo img
		);
		modificare se necessario la posizione di thimthumb
	*/
	function the_img($args){
		
		$timthumb = '/app/frontend/_v/timthumb.php';
		
		if( !$this->has_img() )
			return '<img src="http://placehold.it/'.$args['w'].'x'.$args['h'].' title="Not found" alt="Not found" class="img-responsive">';
		
		$img = $timthumb.'?src='.$this->getAttr('img');
		
		if($args['w'])	$img .= '&w='.$args['w'];
		if($args['h'])	$img .= '&h='.$args['h'];
		if($args['zc']) $img .= '&zc='.$args['zc'];
		
		$class = ($args['class']) ? $args['class'] : 'img-responsive';
		
		return '<img src="'.$img.'" title="'.$args['title'].'" alt="'.$args['alt'].'" class="'.$class.'" >';
		
	}
}


?>