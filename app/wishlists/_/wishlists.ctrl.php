<?php

### GYURAL ###

/*

----------
wishlists 
----------

Filename: /app/wishlists/_/wishlists.ctrl.php
 Version: 0.1
  Author: Andrea Rufo <a.rufo@mandarinoadv.com>
    Date: 12/11/2015
	
*/

class wishlistsCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	function apiLists(){
		$return['esito'] = 1;

		// $return['auto'][] = ['id'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'];
		// ['id'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		if($_SESSION['wishlist']['products']){
			foreach ($_SESSION['wishlist']['products'] as $key => $cod) {
				# code...
				#echo '<p>Cod: '.$cod.'</p>';
				$usato = LoadClass("usato", 1)->filter(Array("cod", $cod), "ONE");
				#echo '<pre>'.print_r($usato, 1).'</pre>';
				if($usato){

					if(strlen(trim($usato->foto1))>0){
	                	$url = 'http://www.ocholding.com/infocar/infocar/'.$usato->foto1.'';
					
						$mioarrayurl = get_headers($url, 1);
						if($mioarrayurl[0]=="HTTP/1.1 200 OK" || $mioarrayurl[0]=="HTTP/1.0 200 OK"){
		                	$foto1 = "http://www.ocholding.com/infocar/infocar/".$usato->foto1."";
							$lauto = 0;
						}else{
							$foto1 = "http://www.ocholding.com/infocar/infocar2/".$usato->foto1."";
							$lauto = 1;
						}
	                }else{
	                	$foto1 = "/cdn/images/nofoto.jpg";
	                }
					$return['auto'][] = ['id'=>$usato->id,
					'descrizione'=>$usato->descmarca." ".$usato->descallestimento,
					'equip' => $usato->equipdesc,
					'tipint' => $usato->tipologiainterna,
					'cod' => $usato->cod,
					'img'=>$foto1,
					'cat'=>$usato->categ,
					'cav' => $usato->cav,
					'immatric' => $usato->immatric,
					'prezzostimato' => $usato->stimato,
					"url" => $usato->gyu_url];
				}
			}
		}
		return $return;
	}

	// api di richiamo di tutti gli oggetti dell'app
	function apiGets($args = null){
		
		return LoadClass('wishlists', 1)->filter_array($args);
		
	}
	
	// api di richiamo del singolo oggetto dell'app
	function apiGet($id){
		
		if($_REQUEST['id']){
			$id = $_REQUEST['id'];
		}
		
		if($id){
			return LoadClass('wishlists', 1)->get($id);
		}else{
			return false;
		}
		
	}
	
	function ApiAdd(){
		
		$return['esito'] = 0;
		$prod = $_REQUEST['prod'];
				
		// controlo se c'è gia
		if( in_array($prod, $_SESSION['wishlist']['products']) ):
		
			// lo rimuovo
			$pos = array_search($prod, $_SESSION['wishlist']['products']);
			unset($_SESSION['wishlist']['products'][$pos]);
			$return['esito'] = 2;
			
		else:
			
			// lo aggiungo
			$_SESSION['wishlist']['products'][] = $prod;
			
			$return['esito'] = 1;

		endif;
		
		//controllo se tutti i prodotti sono a catalogo
		/*foreach( $_SESSION['wishlist']['products'] as $k=>$i){
			
			if( !LoadApp('products',1)->apiGet($i) ){
				unset($_SESSION['wishlist']['products'][$k]);
				#$return['esito'] = 0;
			}
			else{
				#$return['esito'] = 1;
			}
			
		}*/
		
		// salvo se ho la mail
		// if( $_SESSION['wishlist']['email'] ){
			
		//	LoadApp('wishlists', 1)->apiSave( $_SESSION['wishlist']['email'], $_SESSION['wishlist']['products'] );
		
		// }
		
		return $return;
		
	}

	
	// function apiSave($email, $products){
		
	// 	if( $email == '' )
	// 		return array( 'error'=>'Email non pervenuta' );
		
	// 	// controllo se gia esiste
	// 	if( $wl = LoadClass('wishlists', 1)->filter( array('email', $email), 'ONE' ) ){
			
	// 		/*
	// 		// prendo i prodotti già salvati
	// 		$ex = unserialize($wl->products);
			
	// 		foreach( $products as $id )
	// 			if( !in_array($id, $ex) )
	// 				$ex[] = $id;
			
	// 		// ci aggiungo gli altri
	// 		$wl->products = serialize($ex);
	// 		*/
			
	// 		$wl->products = serialize($products);
			
	// 		// salvo
	// 		$wl->putExecute();
			
	// 	}else{
			
	// 		// nuova lista
	// 		$wl = LoadClass('wishlists', 1);
			
	// 		// imposto parametri
	// 		$wl->email = $email;
	// 		$wl->products = serialize($products);
			
	// 		// salvo
	// 		$wl->hangExecute();
			
	// 		$link = uri.'/frontend/wishlist/id:'.$wl->getAttr('id');
	// 		$body = 'Accedi alla tua wishlist direttamente da <a href="'.$link.'">'.$link.'</a>. Inserisci la tua email e modificala.';
			
	// 		$body		= 'Ciao,<br>
			
	// 			grazie per aver salvato la tua lista desideri su La Luna Nuova. Puoi accedere e condividere la tua lista direttamente da <a href="'.$link.'">'.$link.'</a>.<br>
	// 			Buona navigazione sul nostro sito: www.lalunanuova.com<br>
	// 		<br><br>
	// 		La Luna Nuova srl <br>
	// 		Via Carturan - 04100 Latina<br>
	// 		TEL. +39 0773 48 47 44<br>
	// 		store@lalunanuova.it';
			
	// 		// la invio per email
	// 		if( !CallFunction('mail', 'save', $wl->email, 'La Luna Nuova lista desideri', $body) )
	// 			echo 'Errore invio email';
			
	// 	}
		
	// 	// lo imposto in sessione
	// 	$_SESSION['wishlist'] = array(
			
	// 		'id' 		=> $wl->getAttr('id'),
	// 		'email' 	=> $wl->email,
	// 		'products' 	=> unserialize($wl->products)
			
	// 	);
		
	// 	// echo '<pre>'.print_r($_SESSION['wishlist'], 1).'</pre>';
		
	// 	return $wl;
		
	// }
	
}

?>