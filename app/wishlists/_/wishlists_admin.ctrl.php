<?php

### GYURAL ###

/*

----------
wishlists 
----------

Filename: /app/wishlists/_/wishlists_admin.ctrl.php
 Version: 0.1
  Author: Andrea Rufo <a.rufo@mandarinoadv.com>
    Date: 12/11/2015
	
*/

class wishlists_adminCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	// vista per la lista in admin
	function getIndex(){
				
		Application('admin/_v/header', null, $app_data);
		Application('wishlists/_v/index', null, $app_data);
		Application('admin/_v/footer', null, $app_data);
		
	}
	
	// vista per il form creazione/modifica in admin
	function getForm(){
		
		Application('admin/_v/header', null, $app_data);
		Application('wishlists/_v/form', null, $app_data);
		Application('admin/_v/footer', null, $app_data);
		
	}
	
	// post di modifica o creazione o cancellazione dell'oggetto
	function postForm(){
		
		//post
		
		if( $_POST['wishlist_id'] and $_POST['delete'] ){
			
			//delete item
			if( !$item = LoadClass('wishlists', 1)->get($_POST['wishlist_id']) ) exit('Errore load');			
			if( !$item->deleteExecute() ) exit('Errore delete');
						
			CallFunction('hooks', 'get', 'delete.end', $item);
			
			header('Location: /wishlists/admin/delete:1');
			exit;
			
		}elseif( $_POST['wishlist_id'] ){
			
			//update item
			if( !$item = LoadClass('wishlists', 1)->get($_POST['wishlist_id']) ) exit('Errore load');
		
		} else {
						
			//create item
			$item = LoadClass('wishlists', 1);
		
		}
		
		$item->refill($_POST);
		
		// copio l'immagine in evidenza
		
		$file = $_FILES['img'];
		
		if( $file['name'] != '' ){
		
			if( $file['error'] )
				exit('errore file');
			elseif( $file['size'] > 4000000 )
				exit('file troppo grande');
			elseif($file['type'] != 'image/png' and $file['type'] != 'image/jpeg' )
				exit('formato file errato');
		
			// rinomino il file
		
			$info 		= pathinfo($file['name']);
		    $basename	= $info['basename']; 
		    $ext 		= $info['extension'];
			
			$filename 	= CallFunction('strings', 'slug', $basename, 50);
			$filename 	= time().'-'.$filename.'.'.$ext;
			
			//copio il file
			
			if( !copy($file['tmp_name'], upload.$filename) )
				exit('errore copia');
			
			// assegno il valore
			$item->setAttr('img', $filename);
	
		}

		// execution
		if( $_POST['wishlist_id'] ){
			
			if( !$item->putExecute() ) exit('Errore put');
		
		} else {
						
			if( !$item->hangExecute() ) exit('Errore hang');
		
		}
		
		CallFunction('hooks', 'get', 'post.end', $item);
		
		header('Location: /wishlists/admin/update:1');
		
	}
	
	function postDelete(){
		
		//delete item
		if( !$item = LoadClass('wishlists', 1)->get($_POST['id']) ) exit('Errore load');			
		if( !$item->deleteExecute() ) exit('Errore delete');
						
		CallFunction('hooks', 'get', 'delete.end', $item);
		
		header('Location: /wishlists/admin/delete:1');
	
	}
	
}

?>