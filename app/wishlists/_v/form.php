<?php
	
	if( isset($_GET['id']) ){
		$item = LoadApp('wishlists', 1)->ApiGet($_GET['id']);
		$title = 'Modifica';
	}else{
		$item = LoadClass('wishlists', 1);
		$title = 'Aggiungi';
	}
?>

<h1><?=$title?></h1>

<form method="post" enctype="multipart/form-data">
	
	<div class="form-group">
		<label for="title">Titolo</label>
		<input class="form-control input-lg" type="text" required name="title" value="<?=$item->title?>"/>
	</div>
	
	<div class="form-group">
		<label for="content">Contenuto</label>
		<textarea class="form-control summernote" rows="4" name="content"><?=$item->content?></textarea>
	</div>
	
	<div class="form-group">
		<label for="img">Immagine</label>
		<input class="form-control" id="img" type="file" name="img">
		<p class="help-block">File: <a href="<?=$item->getAttr('img')?>"><?=$item->getAttr('img')?></a></p>
	</div>
	
	<? if( isset($item->wishlist_id) ): ?>
	
	<div class="row">
		<div class="col-sm-3">
			
			<button type="submit" class="btn btn-default btn-block">Modifica</button>
			
		</div>
		<div class="col-sm-9 text-right">
			
			<input type="hidden" name="wishlist_id" value="<?=$item->wishlist_id?>" />
			<div class="checkbox">
				<label><input type="checkbox" name="delete" value="1" /> Elimina definitivamente l'inserimento</label>
			</div>
			
		</div>
	</div>
	
	<? else: ?>
	
	<div class="row">
		<div class="col-sm-3">
			
			<button type="submit" class="btn btn-default btn-block">Inserisci</button>
	
		</div>
	</div>
	
	<? endif; ?>
		
	<div class="clearfix margin-40"></div>
	
	<? CallFunction('hooks', 'get', 'form.end', $item); ?>
		
</form>