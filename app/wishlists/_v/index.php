<div class="page-header">
	<h1>Wishlists</h1>
</div>

<?
	
$items 			= LoadClass('wishlists', 1)->filter( array('LIMIT', 0, 9999), ' order by creationTime desc '  );

if( $items ) : 

?>

	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th width="100">ID</th>
				<th>Titolo</th>
				<th width="200">Inserimento</th>
				<th width="200">Aggiornamento</th>
				<th>Cancella</th>
			</tr>
		</thead>
		<tbody>	
	
			<?php foreach( $items as $item ) : ?>
			
			<tr>
				<td>
					<?=$item->wishlist_id?>
				</td>
				<td>
					<?=$item->email?>
				</td>
				<td>
					<?=date('d/m/Y H:i', $item->creationTime)?>
				</td>
				<td>
					<?=date('d/m/Y H:i', $item->gyu_updateTime)?>
				</td>
				<td>
					<form method="post" action="/wishlists/admin/delete">
						<input type="hidden" value="<?=$item->wishlist_id?>" name="id">
						<button class="btn btn-xs btn-danger" type="submit"><i class="fa fa-trash"></i></button>
					</form>
				</td>
			</tr>
			
			<?php endforeach; ?>
			
		</tbody>
	</table>

<? else: ?>

	<div class="alert alert-info">
		Ancora nessun elemento inserito...
	</div>
	
<? endif; ?>				