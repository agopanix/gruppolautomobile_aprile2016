<div class="row">
	<div class="small-12 columns padding-top-14 padding-bottom-10 text-center">
		<?php 
		  $items      = LoadClass('chisiamo', 1)->filter( ' ORDER BY chisiamo_id DESC ' );
		?>

		<?php 
		  	foreach ($items as $item) {
		?>
			<h1 class="h1 titolo-sezione font-bold color-6"><?= $item->getAttr('title') ?></h1>

			<img class="padding-top-10 padding-bottom-10" src="<?= $item->getAttr('img') ?>" style="width:100%">	
			<div class="padding-top-10 padding-bottom-10">
			<?= $item->getAttr('content') ?>
			</div>
		<?php 
			}
		?>
	</div>
</div>