<?php 
	$items      = LoadClass('servizi', 1)->filter( ' WHERE servizio_id = 3 ' );
		foreach ($items as $item) {
?>
<div class="row">
	<div class="small-12 columns padding-top-10 font-size-15 font-bold">
		<?php echo $item->getAttr('title');?>
	</div>
</div>
<div class="row">
	<div class="small-12 columns font-size-12  padding-top-10 color-7 font-bold">
		<?php echo $item->getExcerpt();?>
	</div>
</div>
<div class="row">
	<div class="medium-12 columns padding-top-10 color-7">
		<div class="row">
			<?php 
			if(strlen($item->getAttr('img'))>0){
			?>
			<div class="medium-12 columns">
				<img class="fullWidth" src="<?php echo $item->getAttr('img');?>" title="">
			</div>
			<?php 
			}
			?>
			<div class="medium-12 columns padding-top-10">
				<?php echo $item->getAttr('content');?>
			</div>
		</div>
	</div>
	
	
</div>
<?php 
		}
?>
<!--
<form acction="" method="post">
	
	<div class="row">
		<div class="medium-6 columns padding-top-20">
			<input type="text"  class="big" placeholder="USERNAME*"  tabindex="1" required/>
		</div>
		<div class="medium-6 columns padding-top-20">
			<input type="password"  class="big" placeholder="PASSWORD"  tabindex="2" required/>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns text-right">
			<button type="submit" tabindex="3" >Login</button>
		</div>
	</div>
</form>
-->