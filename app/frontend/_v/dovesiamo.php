<?
$frontend = $app_data;
$concessionari = $this->frontend->concessionari;
?>
<div class="row">
	<div class="small-12 columns padding-top-13 text-center">
		<h1 class="h1 titolo-sezione font-bold color-6">Le Nostre Sedi</h1>
	</div>
</div>
<div class="row">
	<div class="medium-12 columns padding-top-10 padding-bottom-20 font-size-12 color-7">
		<div class="map" id="vat-map-2"></div>
	</div>
</div>
<!-- RIGA 1 -->
<div class="row" >
	<div class="small-12">
		<?php foreach ($concessionari as $k => $concessionario) {?>
		<div class="columns medium-6">
			<div class="row  padding-top-10 collapse">
				<div class="small-12 font-bold"><?php echo $concessionario["titolo"];?></div>
				<div class="small-12"><?php echo $concessionario["concessionaria"];?></div>
				<div class="medium-5 columns" >
					<img class="img-contact" data-interchange="
					[/timthumb.php?src=<?php echo $concessionario["img"]; ?>&w=640&h=380&zc=1&a=c, (small)],
					[/timthumb.php?src=<?php echo $concessionario["img"]; ?>&w=640&h=380&zc=1&a=c, (medium)],
					[/timthumb.php?src=<?php echo $concessionario["img"]; ?>&w=185&h=140&zc=1&a=c, (large)]"
					src="/timthumb.php?src=<?php echo $concessionario["img"]; ?>&w=185&h=110&zc=1&a=c" >
				</div>
				<div class="medium-7 columns font-size-14">
					<br>
					<?php echo $concessionario["indirizzo"];?> 	<?php echo $concessionario["citta"];?> (<?php echo $concessionario["sigla"];?>)<br>
					<br><b>Telefono: </b> <?php echo $concessionario["telefono"];?><br>
					<br><b>Orari: </b><?php echo $concessionario["orari"];?>
				</div>
			</div>
			<hr>
		</div>
		<?php } ?>
	</div>
</div>
<div class="row">
	<div class="medium-12 columns padding-top-20  color-7">
		<span class="font-bold">SE HAI NECESSITÀ RICHIEDI UN APPUNTAMENTO</span><br>
		<span class="font-size-08">Un nostro responsabile la ricontatter&agrave; per fissare un appuntamento.<br></span>
		<br>

		<?
		Application('frontend/_v/prenota-appuntamento', null, $this);
		/*
		<form action="/frontend/invio/prenotazione" method="post">
			
			<input type="hidden" name="gyu_return" value="<?= $_SERVER['REQUEST_URI']; ?>" />
			<input type="text" name="gyu_ctrl" value="" style="opacity: 0; height: 0px; width: 0px; margin: 0px; padding: 0px;" />
			
			<div class="row ">
				<div class="large-12 columns">
					<input type="text"  class="big" placeholder="NOME E COGNOME"  tabindex="1" required/>
					<input type="email" class="big" placeholder="EMAIL"  tabindex="3" required/>
				</div>
				<div class="large-12 columns">
					<input type="number" class="big" placeholder="TELEFONO"  tabindex="4" required/>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<textarea rows="4" cols="30" class="form-control" name="message" tabindex="5" placeholder="MESSAGGIO" required=""></textarea>
				</div>
			</div>
			<div class="row">
				<div class="large-12 columns margin-top-1">
					<input style="margin-top:5px;" id="authorize-action" tabindex="6" type="checkbox"><label class="authorize-action-label" for="authorize-action">Autorizzo il trattamento dei dati personali Dlgs 196/2003</label>
				</div>
				<div class="large-12 columns text-right padding-top-13">
					<button type="submit" tabindex="7" >Prenota</button>
				</div>
			</div>
		</form>
		<br>
		*/
		?>
	</div>
</div>