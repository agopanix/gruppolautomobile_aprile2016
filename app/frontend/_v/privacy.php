<div class="row">
	<div class="small-12 columns padding-top-20 padding-bottom-20">
		<div class="line-height-20">
			Ex art. 13 D. Lgs. 196/03 <br>
			La scrivente Gruppo L'Automobile srl comunica che, al fine dell'instaurazione e gestione del rapporto contrattuale con Lei in corso, verrà in possesso di Suoi dati personali.
			La informiamo, pertanto, che: <br>
			1. Modalità del trattamento dei dati<br>
			Tali dati verranno trattati con il supporto di mezzi elettronici e cartacei. Tutti i dati di cui all’ oggetto verranno conservati presso la sede di Gruppo L'Automobile srl nel pieno rispetto della riservatezza in ottemperanza a tutte le norme vigenti.<br>
			2. Finalità del trattamento<br>
			I dati verranno conferiti per le seguenti finalità:<br>
			a) per esigenze preliminari alla stipula dei contratti di vendita, per dare esecuzione agli stessi e per la tutela delle posizioni creditorie da essi derivanti; <br>
			b) per adempiere a qualunque tipo di obbligo previsto da leggi o regolamenti vigenti, in particolare, &emsp;&nbsp;in materia fiscale;<br>
			c) per esigenze di tipo operativo, gestionale e contabile; <br>
			d) per la registrazione degli accessi al sito web della Società e l’utilizzo dei servizi prestati con tale sito; <br>
			e) per finalità commerciali e di marketing strategico e operativo. <br>
			3. Comunicazione e diffusione dei dati<br>
			Tutti i dati raccolti ed elaborati potranno essere trasferiti in Paesi all’interno dell’Unione Europea e/o in Paesi terzi ex artt. 42-43 D. Lgs. 196/03 per le medesime finalità di cui al punto 2. Inoltre, i dati personali raccolti per il raggiungimento delle finalità sopra indicate potranno essere comunicati, per quanto di loro specifica competenza, a soggetti pubblici e privati, persone fisiche e/o giuridiche e aventi finalità commerciali e/o di gestione dei sistemi informativi e/o dei sistemi di pagamento, compresi soggetti esterni che svolgono specifici incarichi per conto della nostra Società. In particolare, i dati potranno essere comunicati alle seguenti categorie di soggetti: rete commerciale, istituti bancari e società specializzate nella gestione dei pagamenti, studi legali e di consulenza, soggetti incaricati della revisione del bilancio della nostra società, pubbliche autorità, fornitori italiani ed esteri, società di finanziamento e di trasporto, terzi incaricati del controllo della qualità del flusso logistico-commerciale, nonché alle altre società facenti parte del nostro Gruppo. I dati potranno, altresì, essere diffusi, ma solo in forma aggregata, anonima e per finalità statistiche. La scrivente società si premunirà, per quanto in suo potere, di far garantire la massima riservatezza dai soggetti su indicati, e di far trattare i dati solo per le finalità di cui sopra. <br>
			4. Cessazione del trattamento<br>
			Tutti i dati di cui all’oggetto verranno conservati anche dopo la cessazione del rapporto contrattuale per l’espletamento di tutti gli eventuali adempimenti connessi o derivanti dalla conclusione del rapporto contrattuale stesso. <br>
			5. Titolare del trattamento<br>
			Titolare del trattamento dei dati conferiti è la scrivente Gruppo L'Automobile srl, nella persona del legale rappresentante pro tempore. <br>
			6. Responsabile del trattamento<br>
			Responsabile del Trattamento è Gruppo L'Automobile srl. Pertanto la S.V. potrà ad essa rivolgersi per tutte le informazioni riguardanti il trattamento dei Suoi dati e per tutto quanto previsto nell’ art. 7 D. Lgs. 196/03, nonché per ottenere la lista completa ed aggiornata di altri eventuali Responsabili. <br>
			7. Diritti dell’interessato<br>
			Relativamente ai dati personali in possesso della scrivente Società l’Interessato può esercitare i diritti previsti dall’ art. 7 D.Lgs 196/03. In caso di morte dell’Interessato i suoi diritti potranno essere fatti valere dai suoi eredi.
		</div>
	</div>
</div>