<div class="row">
	<div class="small-12 columns padding-top-10 padding-bottom-10 text-center">
		
			<h1 class="h1 titolo-sezione font-bold color-6">Pagina non trovata</h1>
				
			<div class="padding-top-10 padding-bottom-10">
			La pagina richiesta non è più disponibile. <a href="/">Prosegui la navigazione sul sito</a>
			</div>
		
	</div>
</div>