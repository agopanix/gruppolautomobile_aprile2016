<div class="row ">
	<div class="large-4 columns padding-top-20">
		<div class="font-bold font-size-12">CONTATTI</div>
		<span class="bold underline"><span class="italic">Gruppo</span> L'Automobile</span><br>
		<span>Via Enrico Fermi, 27<br>
			Telefono: +39 800.709.990<br>
			Mail: <a href="mailto:info@gruppolautomobile.it">info@gruppolautomobile.it</a><br>
		</span>
		
	</div>
	<div class="large-8 columns">
		<div class="row ">
			<div class="large-12 columns">
				<div class="padding-top-20 padding-bottom-10 font-bold font-size-12">RICHIEDI INFORMAZIONI</div>
				<form method="post" action="/frontend/invio/informazioni">
					<input type="hidden" name="gyu_return" value="<?= $_SERVER['REQUEST_URI']; ?>" />
					<input type="text" name="gyu_ctrl" value="" style="opacity: 0; height: 0px; width: 0px; margin: 0px; padding: 0px;" />
					<div class="row ">
						<div class="large-6 columns">
							<input type="text"  class="big" name="name" placeholder="NOME"  tabindex="1" required/>
							<input type="email" class="big" name="email" placeholder="EMAIL"  tabindex="3" required/>
						</div>
						<div class="large-6 columns">
							<input type="text" name="surname"  class="big" placeholder="COGNOME"  tabindex="2" required/>
							<input type="number" name="telefono" class="big" placeholder="TELEFONO"  tabindex="4" required/>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<textarea rows="4" cols="30" class="form-control" name="message" tabindex="5" placeholder="MESSAGGIO" required=""></textarea>
						</div>
					</div>
					<div><p>Tutti i campi sono obbligatori</p></div>
					<div class="row">
						<div class="large-9 columns margin-top-1 contacts-form">

							<input id="authorize-action" tabindex="6" type="checkbox" name="privacy" value="Si" required ><label class="authorize-action-label" for="authorize-action">Autorizzo il trattamento dei dati personali Dlgs 196/2003</label>
						</div>
						<div class="large-3 columns text-right margin-top-10">
							<button type="submit" tabindex="7" >Invia</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>