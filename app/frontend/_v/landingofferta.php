<div class="fullWindow bkg-color-0">
	<div class="row">
		<div class="small-12 columns text-center padding-top-20">
			Lorem ipsum dolor sit amet, falli tollit cetero te eos. Ea ullum liber aperiri mi, impetus ate philosophia ad duo, quem regione ne
			ius. Vis quis lobortis dissentias ex, in du aft philosophia, malis necessitatibus no mei. Volumus sensibus qui ex, eum duis
			doming ad. Modo liberavisse eu mel, no viris prompta sit. Pro labore sadipscing et. Ne peax egat usu te mel vivendo scriptorem.
			Pro labore sadipscing et. Ne pertinax egat usu te mel vivendo scriptorem.
			Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim
			assum. Typi non habent claritatem insitam est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt
			lectores legere me lius quod legunt saepius. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
			lobortis nisl ut aliquip ex ea commodo consequat.
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns text-center padding-top-20">
			Lorem ipsum dolor sit amet, falli tollit cetero te eos. Ea ullum liber aperiri mi, impetus ate philosophia ad duo, quem regione ne
			ius. Vis quis lobortis dissentias ex, in du aft philosophia, malis necessitatibus no mei. Volumus sensibus qui ex, eum duis
			doming ad. Modo liberavisse eu mel, no viris prompta sit. Pro labore sadipscing et. Ne peax egat usu te mel vivendo scriptorem.
			Pro labore sadipscing et. Ne pertinax egat usu te mel vivendo scriptorem.
			Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim
			assum. Typi non habent claritatem insitam est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt
			lectores legere me lius quod legunt saepius. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
			lobortis nisl ut aliquip ex ea commodo consequat.
		</div>
	</div>
	
	<div class="row margin-top-20">
		<div class="photo-scroller large-6 columns relative button-action-landing">
			<ul class="orbit" data-orbit  data-options="animation:slide; animation_speed:500;  timer:false; navigation_arrows:true; bullets:true; slide_number: false; resume_on_mouseout: false;">
				<li>
					<img
					data-interchange="
					[/timthumb.php?src=/cdn/images/photo-image-2.jpg&w=600&h=300&zc=1, (small)],
					[/timthumb.php?src=/cdn/images/photo-image-2.jpg&w=1200&h=620&zc=1, (medium)],
					[/timthumb.php?src=/cdn/images/photo-image-2.jpg&w=590&h=330&zc=1, (large)]"
					src="" >
				</li>
			</ul>
		</div>
		<div class="large-6 columns">
			<div class="row">
				<div class="small-12 columns">
					<span class="font-bold font-size-15">Eventuale paragrafo secondario</span>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns text-left">
					<div class="row">
						<div class="small-12 columns text-left">
							Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat.<br><br>
							<ul>
								<li>Nam liber</li>
								<li>nihil imperdiet</li>
								<li>id quod mazim</li>
								<li>eleifend option</li>
							</ul>
						</div>
					</div>
					<div class="row button-action-landing">
						<div class="medium-6 columns text-left">
						<a href="<? echo createUrl('dovesiamo/GetIndex')[1]; ?>"class="button fullWidth font-size-08 font-bold"><i class="fa fa-calendar"></i> VIENI A TROVARCI</a>
						</div>
						<div class="medium-6 columns text-left">
						<a href="<? echo createUrl('contatti/GetIndex')[1]; ?>"class="button fullWidth button-gray-2 font-size-08 font-bold"><i class="fa fa-envelope"></i> RICHIEDI INFORMAZIONI</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="row color-7 padding-bottom-20">
			<div class="small-6 columns padding-top-10">
				<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10 padding-left-10">INFO RIVENDITORE</div>
				<div class="row">
					<div class="small-12 columns">
						<div class="map-dettaglio margin-bottom-10" id="vat-map-2"></div>
						<span class="font-bold">GRUPPO L’AUTOMOBILE</span><br>
						Via Enrico Fermi, 27 - 03100 Frosinone<br>
						Tel.: +39 - 0775 - 88 50 222<br>
						Mob.: +39 - 345 - 39 70 670<br>
						Fax: +39 - 0775 - 88 50 235<br>
					</div>
				</div>
			</div>
			<div class="small-6 columns padding-top-10">
				<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">CONTATTA IL RIVENDITORE</div>
				<div class="row">
					<div class="small-12 columns">
						<form action="#">
							<div class="row ">
								<div class="large-6 columns">
									<input type="text"  class="big" placeholder="NOME"  tabindex="1" required/>
									<input type="email" class="big" placeholder="EMAIL"  tabindex="3" required/>
								</div>
								<div class="large-6 columns">
									<input type="text"  class="big" placeholder="COGNOME"  tabindex="2" required/>
									<input type="number" class="big" placeholder="TELEFONO"  tabindex="4" required/>
								</div>
							</div>
							<div class="row">
								<div class="small-12 columns">
									<textarea rows="4" cols="30" class="form-control" name="message" tabindex="5" placeholder="MESSAGGIO" required=""></textarea>
								</div>
							</div>
							<div class="row">
								<div class="large-12 columns margin-top-1">
									<input id="authorize-action" tabindex="6" type="checkbox"><label class="authorize-action-label" for="authorize-action">Autorizzo il trattamento dei dati personali Dlgs 196/2003</label>
								</div>
								<div class="large-12 columns text-left padding-top-20">
									<button type="submit" tabindex="7" >RICHIEDI INFORMAZIONI</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>