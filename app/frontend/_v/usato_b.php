<div class="row show-for-small-down">
	<div class="small-12 padding-top-15 text-center font-bold font-size-15">
		<?php echo $this->frontend->config["titleHeader"]; ?>
	</div>
</div>
<div class="row margin-top-20">
<div class="large-3  columns left">
		<form  id="raf" class="ricerca-avanzata-form" action="/frontend/usato/cercausato" method="post">
			<div class="row">
				<div class="small-12 columns">
					<div class="hidden-for-medium-down bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">RICERCA AVANZATA</div>
					<div id="openFormMobile" class="close show-for-medium-down bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">
						RICERCA AVANZATA 
						<i class="fa fa-angle-down"></i> <i class="fa fa-angle-up"></i>
					</div>
				</div>
			</div>
			<div class="area-form">
			<div class="dimension-form close">
			<div class="row">
				<div class="small-12 columns">
					<label><span class="show-for-large-up">Marchio</span>
						<select name="marchio">
			                <option value="TUTTI">TUTTI</option>
			                <?php 
			                $listamarchi = $this->listamarchi;

			                foreach ($listamarchi as $marchio) {
			                  if($_SESSION["marchiosel"]==strtoupper($marchio->descmarca)){
			                    $selectedmarchio = "selected";
			                  }else{
			                    $selectedmarchio = "";
			                  }
			                  echo '<option value="'.strtoupper($marchio->descmarca).'" '.$selectedmarchio.'>'.strtoupper($marchio->descmarca).'</option>';
			                }
			                ?>
			                
			            </select>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label><span class="show-for-large-up">Modello</span>
						<?php 
						if(strlen($_SESSION["modellosel"])>0){
							$miomodello = $_SESSION["modellosel"];
						}else{
							$miomodello = "";
						}
						?>
						<input type="text" name="modello" value="<?=$miomodello; ?>" placeholder="Modello">
					</label>
				</div>
			</div>
			<?php 
			//print_r($this->listaalim);
			//die();
			?>
			<div class="row">
				<div class="small-12 columns">
					<label><span class="show-for-large-up">Alimentazione</span>
						<select name="alimentazione">
			                <option value="TUTTE">TUTTE</option>
			                <?php 
			                $listaalim = $this->listaalim;

			                foreach ($listaalim as $alimentazione) {

			                	if($alimentazione->alim == B){
									$alimentazionemia = "BENZINA";
								}elseif($alimentazione->alim == D){
									$alimentazionemia = "DIESEL";
								}elseif($alimentazione->alim == G){
									$alimentazionemia = "GAS";
								}else{
									$alimentazionemia = "IBRIDA";
								}

			                  if($_SESSION["alimsel"]==strtoupper($alimentazione->alim)){
			                    $selectedalim = "selected";
			                  }else{
			                    $selectedalim = "";
			                  }
			                  echo '<option value="'.$alimentazione->alim.'" '.$selectedalim.'>'.strtoupper($alimentazionemia).'</option>';
			                }
			                ?>
			                
			            </select>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label><span class="show-for-large-up">Colore</span>
						<?php 
						if(strlen($_SESSION["coloresel"])>0){
							$miocolore = $_SESSION["coloresel"];
						}else{
							$miocolore = "";
						}
						?>
						<input type="text" name="colore" value="<?=$miocolore; ?>" placeholder="Colore">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label><span class="show-for-large-up">Da Km percorsi</span>
						<?php 
						if(strlen($_SESSION["dakmpercosel"])>0){
							$miodakm = $_SESSION["dakmpercosel"];
						}else{
							$miodakm = "";
						}
						?>
						<input type="number" name="dakmperco" value="<?=$miodakm; ?>" placeholder="Da Km percorsi">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label><span class="show-for-large-up">A Km percorsi</span>
						<?php 
						if(strlen($_SESSION["akmpercosel"])>0){
							$mioakm = $_SESSION["akmpercosel"];
						}else{
							$mioakm = "";
						}
						?>
						<input type="number"name="akmperco" value="<?=$mioakm; ?>" placeholder="A Km percorsi">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label><span class="show-for-large-up">Da anno immatric. (es.:2006)</span>
					<?php 
						if(strlen($_SESSION["daimmatricsel"])>0){
							$miodaimm = $_SESSION["daimmatricsel"];
						}else{
							$miodaimm = "";
						}
						?>
						<input type="number" name="daimmatricolaz" value="<?=$miodaimm; ?>" placeholder="Da anno immatric. (es.:2006)">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label><span class="show-for-large-up">Ad anno immatric. (es.:2006)</span>
						<?php 
						if(strlen($_SESSION["aimmatricsel"])>0){
							$mioaimm = $_SESSION["aimmatricsel"];
						}else{
							$mioaimm = "";
						}
						?>
						<input type="number" name="aimmatricolaz" value="<?=$mioaimm; ?>" placeholder="Ad anno immatric. (es.:2006)">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label><span class="show-for-large-up">Da prezzo (senza simbolo € )</span>
						<?php 
						if(strlen($_SESSION["daprezzosel"])>0){
							$miodapre = $_SESSION["daprezzosel"];
						}else{
							$miodapre = "";
						}
						?>
						<input type="number" name="daprezzo" value="<?=$miodapre; ?>" placeholder="Da prezzo (senza simbolo € )">
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<label><span class="show-for-large-up">A prezzo (senza simbolo € )</span>
						<?php 
						if(strlen($_SESSION["aprezzosel"])>0){
							$mioapre = $_SESSION["aprezzosel"];
						}else{
							$mioapre = "";
						}
						?>
						<input type="number" name="aprezzo" value="<?=$mioapre; ?>" placeholder="A prezzo (senza simbolo € )" >
					</label>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<button class="button fullWidth" type="submit">FILTRA RICERCA</button>
				</div>
			</div>
			</div>
		</div>
		</form>
		<hr class="padding-top-10 hidden-for-medium-down">
		<!-- 
		<form action="#" method="post">
			<div class="row">
				<div class="small-12 columns">
					<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">CERCA ACCESSORI</div>
				</div>
			</div>
			<?php
			
			foreach ($this->frontend->accessori as $accessorio) { ?>
			<div class="row">
				<div class="small-12 columns">
					<input type="checkbox"  name="accessori_group[]" value="<?php echo $accessorio['id'];?>">
					<?php echo $accessorio['titolo'];?>
				</div>
			</div>
			<? } ?>
			<div class="row">
				<div class="small-12 columns">
					<button class="button fullWidth" type="submit">FILTRA RICERCA</button>
				</div>
			</div>
		</form>
		-->
	</div>
	<div class="large-9 columns">
		<div class="row collapse">
			<div class="large-12 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-left margin-bottom-10 show-for-large-up">
				<div class="padding-left-10">RISULTATO RICERCA</div>
			</div>
			<!--<div class="large-6 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-right margin-bottom-10 padding-left-10">
				<i class="fa fa-sort-desc floatright margin-right-10"></i>
				 ORDINA PER:
				<select class="oderbyselect font-bold">
					<option value="orderbyprice" class="no-bold">PREZZO DECRESCENTE</option>
					<option value="orderbyprice" class="no-bold">PREZZO CRESCENTE</option>
				</select> 
				&nbsp;
				
			</div>-->
		</div>
		<?php 
		//print_r($_SESSION);
		?>
		<div class="row">
			<div class="small-12 columns show-form-medium-down padding-bottom-10 show-for-medium-down">
			La ricerca è filtrata per:
			</div>
			<div class="small-12 columns">
				<ul class="category-list">
					<form method="post" class="research">
	  					<li>
						<label>
							<?php 
							if($_SESSION['filtri']['neopatentati']=='on'){
								$checkneopatentati = "checked";
							}else{
								$checkneopatentati = "";
							}
							?>
							<input type="checkbox" class="searchfield" name="f_neopatentati" <?=$checkneopatentati; ?> /> Neopatentati
						</label>
						</li>
						<li>
						<label>
							<?php 
							if($_SESSION['filtri']['iva_esposta']=='on'){
								$checkiva = "checked";
							}else{
								$checkiva = "";
							}
							?>
							<input type="checkbox" class="searchfield" name="f_iva_esposta" <?=$checkiva; ?> /> Iva Esposta
						</label>
						</li>
						<li>
						<label>
							<?php 
							if($_SESSION['filtri']['kmzero']=='on'){
								$checkkm0 = "checked";
							}else{
								$checkkm0 = "";
							}
							?>
							<input type="checkbox" class="searchfield" name="f_kmzero" <?=$checkkm0; ?> /> Aziendali Km0
						</label>
						</li>
						<li>
						<label>
							<?php 
							if($_SESSION['filtri']['cambioautomatico']=='on'){
								$checkcambio = "checked";
							}else{
								$checkcambio = "";
							}
							?>
							<input type="checkbox" class="searchfield" name="f_cambioautomatico" <?=$checkcambio; ?> /> Cambio Automatico
						</label>
						</li>

						<li>
						<label>
							<?php 
							if($_SESSION['filtri']['sportive']=='on'){
								$checksport = "checked";
							}else{
								$checksport = "";
							}
							?>
							<input type="checkbox" class="searchfield" name="f_sportive" <?=$checksport; ?> /> Sportive
						</label>
						</li>

						<li>
						<label>
							<?php 
							if($_SESSION['filtri']['suv']=='on'){
								$checksuv = "checked";
							}else{
								$checksuv = "";
							}
							?>
							<input type="checkbox" class="searchfield" name="f_suv" <?=$checksuv; ?> /> SUV & Fuori Strada
						</label>
						</li>
						
					</form>
				</ul>
			</div>
		</div>
		
		<div class="row">
			<div class="small-12 columns">

				<?php
					$clausolacerca = "";

					if(strlen($_SESSION['marchiosel'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND UPPER(descmarca) = '".$_SESSION['marchiosel']."' ";
						}else{
							$clausolacerca = " WHERE UPPER(descmarca) = '".$_SESSION['marchiosel']."' ";
						}
					}

					if(strlen($_SESSION['alimsel'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND alim = '".$_SESSION['alimsel']."' ";
						}else{
							$clausolacerca = " WHERE alim = '".$_SESSION['alimsel']."' ";
						}
					}

					if(strlen($_SESSION['modellosel'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND descallestimento LIKE '%".$_SESSION['modellosel']."%' ";
						}else{
							$clausolacerca = " WHERE descallestimento LIKE '%".$_SESSION['modellosel']."%' ";
						}
					}

					if(strlen($_SESSION['coloresel'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND colore LIKE '%".$_SESSION['coloresel']."%' ";
						}else{
							$clausolacerca = " WHERE colore LIKE '%".$_SESSION['coloresel']."%' ";
						}
					}

					if(strlen($_SESSION['dakmpercosel'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND kmperco > '".$_SESSION['dakmpercosel']."' ";
						}else{
							$clausolacerca = " WHERE kmperco > '".$_SESSION['dakmpercosel']."' ";
						}
					}

					if(strlen($_SESSION['akmpercosel'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND kmperco <= '".$_SESSION['akmpercosel']."' ";
						}else{
							$clausolacerca = " WHERE kmperco <= '".$_SESSION['akmpercosel']."' ";
						}
					}


					if(strlen($_SESSION['daimmatricsel'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND ((passpro >= ".$_SESSION['daimmatricsel']." AND deposito = 'SEDE L\'AUTOMOBILE') OR (immatricolaz >= ".$_SESSION['daimmatricsel']." AND deposito <> 'SEDE L\'AUTOMOBILE')) ";
						}else{
							$clausolacerca = " WHERE ((passpro >= ".$_SESSION['daimmatricsel']." AND deposito = 'SEDE L\'AUTOMOBILE') OR (immatricolaz >= ".$_SESSION['daimmatricsel']." AND deposito <> 'SEDE L\'AUTOMOBILE')) ";
						}
					}

					if(strlen($_SESSION['aimmatricsel'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND ((passpro <= ".$_SESSION['aimmatricsel']." AND deposito = 'SEDE L\'AUTOMOBILE') OR (immatricolaz <= ".$_SESSION['aimmatricsel']." AND deposito <> 'SEDE L\'AUTOMOBILE')) ";
						}else{
							$clausolacerca = " WHERE ((passpro <= ".$_SESSION['aimmatricsel']." AND deposito = 'SEDE L\'AUTOMOBILE') OR (immatricolaz <= ".$_SESSION['aimmatricsel']." AND deposito <> 'SEDE L\'AUTOMOBILE')) ";
						}
					}


					if(strlen($_SESSION['daprezzosel'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND ((stimato >= ".$_SESSION['daprezzosel']." AND deposito = 'SEDE L\'AUTOMOBILE') OR (prezzointernet >= ".$_SESSION['daprezzosel']." AND deposito <> 'SEDE L\'AUTOMOBILE')) ";
						}else{
							$clausolacerca = " WHERE ((stimato >= ".$_SESSION['daprezzosel']." AND deposito = 'SEDE L\'AUTOMOBILE') OR (prezzointernet >= ".$_SESSION['daprezzosel']." AND deposito <> 'SEDE L\'AUTOMOBILE')) ";
						}
					}

					if(strlen($_SESSION['aprezzosel'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND ((stimato <= ".$_SESSION['aprezzosel']." AND deposito = 'SEDE L\'AUTOMOBILE') OR (prezzointernet <= ".$_SESSION['aprezzosel']." AND deposito <> 'SEDE L\'AUTOMOBILE')) ";
						}else{
							$clausolacerca = " WHERE ((stimato <= ".$_SESSION['aprezzosel']." AND deposito = 'SEDE L\'AUTOMOBILE') OR (prezzointernet <= ".$_SESSION['aprezzosel']." AND deposito <> 'SEDE L\'AUTOMOBILE')) ";
						}
					}




					if(strlen($_SESSION['filtri']['neopatentati'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND (`tipologiainterna` =  'NEOPATENTATI' OR  `targa` =  'NEOPATENTATI') ";
						}else{
							$clausolacerca = " WHERE (`tipologiainterna` =  'NEOPATENTATI' OR  `targa` =  'NEOPATENTATI') ";
						}
					}


					if(strlen($_SESSION['filtri']['iva_esposta'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND `fa` =  'SI' ";
						}else{
							$clausolacerca = " WHERE `fa` =  'SI' ";
						}
					}

					if(strlen($_SESSION['filtri']['kmzero'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND (`tipologiainterna` LIKE '%km zero%' OR `tipologiaveicolo` LIKE '%km zero%') ";
						}else{
							$clausolacerca = " WHERE (`tipologiainterna` LIKE '%km zero%' OR `tipologiaveicolo` LIKE '%km zero%') ";
						}
					}

					if(strlen($_SESSION['filtri']['cambioautomatico'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." AND (`caa` <> '' OR `cambio` <> 'meccanico') ";
						}else{
							$clausolacerca = " WHERE (`caa` <> '' OR `cambio` <> 'meccanico') ";
						}
					}

					if(strlen($_SESSION['filtri']['sportive'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca." ";
						}else{
							$clausolacerca = " WHERE 1 ";
						}
					}

					if(strlen($_SESSION['filtri']['suv'])>0){
						if(strlen($clausolacerca)>0){
							$clausolacerca = $clausolacerca."  ";
						}else{
							$clausolacerca = " WHERE 1 ";
						}
					}


					/** inizio parte per paginazione **/
					if(strlen($_SESSION['perpage'])>0){
				        $perPage = $_SESSION['perpage'];
				      }else{
				        $perPage = 25;
				      }

				      if(strlen($_GET["pag"])>0){
				        $startItem = ($_GET["pag"]-1)*$_SESSION['perpage'];
				        $numpag = $_GET["pag"];
				      }else{
				        $startItem = 0;
				        $numpag = 1;
				      }

				      //echo '<br>$startItem = '.$startItem;
				       //echo '<br>$perPage = '.$perPage.'<br>';
				      
					/** fine parte per paginazione **/



					if(strlen($clausolacerca)>0){

						//echo '<br>'.$clausolacerca.'<br>';

						$listusatosenzapaginazione = LoadClass('usato', 1)->filter(array('LIMIT', 0, 10000), $clausolacerca." ORDER BY descmarca ASC, descmodello ASC ");


						$listusato = LoadClass('usato', 1)->filter(array('LIMIT', $startItem, $perPage), $clausolacerca." ORDER BY descmarca ASC, descmodello ASC ");

					}else{
						$clausolacerca = " ORDER BY descmarca ASC, descmodello ASC ";
						$listusatosenzapaginazione = LoadClass('usato', 1)->filter(array('LIMIT', 0, 10000), ' '.$clausolacerca.' ');
						$listusato = LoadClass('usato', 1)->filter(array('LIMIT', $startItem, $perPage), ' '.$clausolacerca.' ');
					}

					$numoff = count($listusatosenzapaginazione);
				?>
					
					<?php if(!$listusato) {?> 
				<div class="row">
					<div class="small-12 columns text-center padding-top-10 padding-bottom-10">
						<p>Nessun elemento trovato</p>
					</div>
				</div>
				<?php } ?>



				<?php if($listusato) { ?> 

			<div class="large-12 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-left margin-top-10 margin-bottom-10 show-for-medium-down">
				<div class="padding-left-10">RISULTATO RICERCA</div>
			</div>

			<div class="row collapse">
				<div class="small-12 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-left margin-bottom-10">
					<div class="padding-left-10">
						RISULTATI VISIBILI:
						<?php 
						if(strlen($_SESSION['perpage'])>0){
							if($_SESSION['perpage']==25){
								$colore10 = "#ccc";
								$colore20 = "white";
								$colore50 = "white";
								$coloretutte = "white";
							}elseif($_SESSION['perpage']==50){
								$colore10 = "white";
								$colore20 = "#ccc";
								$colore50 = "white";
								$coloretutte = "white";
							}elseif($_SESSION['perpage']==5000){
								$colore10 = "white";
								$colore20 = "white";
								$colore50 = "white";
								$coloretutte = "#ccc";
							}else{
								$colore10 = "white";
								$colore20 = "white";
								$colore50 = "#ccc";
								$coloretutte = "white";
							}
							
						}else{
							$colore10 = "#ccc";
							$colore20 = "white";
							$colore50 = "white";
							$coloretutte = "white";
						}
						?>
						<a href="/frontend/usato/perpage/num:25" style="color:<?=$colore10;?>; text-decoration:underline; font-weight:bold;">25</a>&nbsp;
						<a href="/frontend/usato/perpage/num:50" style="color:<?=$colore20;?>; text-decoration:underline; font-weight:bold;">50</a>&nbsp;
						<a href="/frontend/usato/perpage/num:100" style="color:<?=$colore50;?>; text-decoration:underline; font-weight:bold;">100</a>
						<a href="/frontend/usato/perpage/num:5000" style="color:<?=$coloretutte;?>; text-decoration:underline; font-weight:bold;">Tutte</a>
					</div>
				</div>
			</div>
			<?php } ?>

		<?php foreach ($listusato as $monousato) { ?>

				<?php 
					if(strlen(trim($monousato->foto1))>0){
				
						$url = 'http://www.ocholding.com/infocar/infocar/'.$monousato->foto1.'';
						
						$mioarrayurl = get_headers($url, 1);
						if($mioarrayurl[0]=="HTTP/1.1 200 OK" || $mioarrayurl[0]=="HTTP/1.0 200 OK"){
							$sorgente1 = "http://www.ocholding.com/infocar/infocar/".$monousato->foto1."";
							$lauto = 0;
						}else{
							//print_r(get_headers($url, 1));
							//die();
							$sorgente1 = "http://www.ocholding.com/infocar/infocar2/".$monousato->foto1."";
							$lauto = 1;
						}
					}else{
						
						$sorgente1 = "/cdn/images/nofoto.jpg";
						
					}


					$mioprezzook = "";

					/** **/
					if($lauto == 0){
							
							if(strlen($monousato->prezzointernet)>0 && $monousato->prezzointernet > 0){
								$mioprezzook = number_format($monousato->prezzointernet, 0, ",", ".");
								$mioprezzookstampa = number_format($monousato->prezzointernet, 0, ",", ".");
							}else{
								$mioprezzook = number_format($monousato->stimato, 0, ",", ".").'';
								$mioprezzookstampa = number_format($monousato->stimato, 0, ",", ".").'';
							}

							$mioprezveurook = ''.$monousato->ppneuro.'';

							$mioprezvaltok = ''.$monousato->prezzoveuro.'';

							
							 
						}else{
							if(strlen($monousato->prezzointernet)>0 && $monousato->prezzointernet > 0){
								$mioprezzook = number_format($monousato->prezzointernet, 0, ",", ".");
								$mioprezzookstampa = number_format($monousato->prezzointernet, 0, ",", ".");
							}else{
								$mioprezzook = $monousato->prezzoveuro;
								$mioprezzookstampa = $monousato->prezzoveuro;
							}



							$mioprezveurook = ''.$monousato->prezzoveuro.'';

							$mioprezvaltok = ''.$monousato->prezzovalt.'';

							
							 
						}

						if($mioprezzook==0){
								$mioprezzook = number_format($monousato->stimato, 0, ",", ".");
							}

						if($mioprezzook==0){
								$mioprezzook = number_format($monousato->prezzoveuro, 0, ",", ".");
							}

						if($mioprezzook==0){
								$mioprezzook = number_format($monousato->prezzointernet, 0, ",", ".");
							}

						if($mioprezzook==0){
								$mioprezzook = number_format($monousato->ppneuro, 0, ",", ".");
							}
					/** 
					
					if($lauto == 0){
									
									if(strlen($monousato->prezzointernet)>0 && $monousato->prezzointernet > 0){
										$mioprezzook = '&euro; '.number_format($monousato->prezzointernet, 0, ",", ".");
									}else{
										$mioprezzook = '&euro; '.number_format($monousato->stimato, 0, ",", ".").'';
									}
									
									 
					}else{
									if(strlen($monousato->prezzointernet)>0 && $monousato->prezzointernet > 0){
										$mioprezzook = '&euro; '.number_format($monousato->prezzointernet, 0, ",", ".");
									}else{
										$mioprezzook = ''.$monousato->prezzoveuro.'';
									}
									 
					}

					**/

					
					$miocav = "";
	                $miokw = "";

					if($monousato->kw > 0){
								
								$miokw = $monousato->kw;
								$miocav = $monousato->cav;
					}else{
								
								$miokw = $monousato->cav;
								$miocav = $monousato->immatricolaz;
					}

					$alimentazionemia = "";

					if($monousato->alim == B){
								$alimentazionemia = "BENZINA";
					}elseif($monousato->alim == D){
								$alimentazionemia = "DIESEL";
					}elseif($monousato->alim == G){
								$alimentazionemia = "GAS";
					}else{
								$alimentazionemia = "IBRIDA";
					}

					
				?>

				<div class="row car-schedule">
					<div class="medium-4 columns padding-top-10">
						<a href="/frontend/dettaglio/cod:<?= $monousato->cod; ?>" style="border:0px; text-decoration:none; outline:0px;">
						<img
						data-interchange="
						[/timthumb.php?src=<?php echo $sorgente1;?>&w=600&h=300&zc=1&a=c, (small)],
						[/timthumb.php?src=<?php echo $sorgente1;?>&w=600&h=300&zc=1&a=c, (medium)],
						[/timthumb.php?src=<?php echo $sorgente1;?>&w=300&h=300&zc=1&a=c, (large)]"
						src="/timthumb.php?src=<?php echo $sorgente1;?>&w=300px&h=300&zc=1" style="border:0px; text-decoration:none; outline:0px;" >
						</a>
					</div>
					<div class="medium-8 columns  padding-top-10">
						<div class="row primary-informations collapse">

							<div class="small-6 large-3 columns text-center padding-top-05 padding-bottom-05">
							<?php 
							//echo callFunction('frontend','price',$auto['importo'],'€');
							if($mioprezzook==0){
								echo 'Tratt.Riserv.';
							}else{
								echo '&euro; '.$mioprezzook;	
							}
							
							?>
							</div>
							<div class="small-6 large-3 columns text-center padding-top-05 padding-bottom-05"><?php echo 'km '.$monousato->kmperco;?></div>
							<div class="small-6 large-4 columns text-center padding-top-05 padding-bottom-05"><?php echo 'Imm. '.$monousato->immatric;?></div>
							<div class="small-6 large-2 columns text-center padding-top-05 padding-bottom-05"><?php echo $miocav;?>cv </div>
						</div>
						<div class="row">
							<div class="medium-8 columns padding-top-10">
								<span class="font-bold"><?php echo strtoupper($monousato->descmarca).' '.strtoupper($monousato->descallestimento);?></span><br>
								<?php 
								if(strlen($monousato->descmarca." ".$monousato->descallestimento)>30){
									$stiledesc = "min-height: 64px !important;";
								}else{
									$stiledesc = "min-height: 88px !important;";
								}
								?>
								<div class="description" style="<?=$stiledesc; ?>">
									<span class="font-size-09">
										<?php
											echo "Colore: ".$monousato->colore." / Cilindrata: ".$monousato->cil." <br>Cod: ".$monousato->cod."";
										?>
									</span>
								</div>
								
							</div>
							<div class="medium-4 columns">
								<ul class="font-size-09 padding-top-10">
									<li>Usato garantito</li>
									<li>Alim. <?php echo $alimentazionemia;?></li> 
								</ul>
							</div>
						</div>

						<div class="row">
							<div id="area-<?php echo $monousato->cod;?>" class="small-12 hidden-for-large-up contattami-area">
							<a  data-cod="<?php echo $monousato->cod;?>" class="button fullWidth font-size-08 contattami margin-bottom-0">CONTATTAMI</a>
								<form ID="" method="post" action="/frontend/invio/usato" class="hide">
									<input type="text" name="gyu_ctrl" value="" style="opacity: 0; height: 0px; width: 0px; margin: 0px; padding: 0px;" />
									<div class="row ">
										<div class="large-12 columns padding-top-10">
											<input type="hidden" 				name="cod" value="<?=$monousato->cod ?>"/>
											<input type="text"  	class="big" name="name" placeholder="NOME E COGNOME"  	tabindex="1" required/>
											<input type="email" 	class="big" name="email" placeholder="EMAIL"  			tabindex="2" required/>
											<input type="number" 	class="big" name="name" placeholder="CELLULARE"  		tabindex="3" required/>
										</div>
									</div>
									<div class="row">
										<div class="small-12 columns">
											<textarea rows="4" cols="30" class="form-control" name="message" 				tabindex="4" placeholder="" required>VORREI MAGGIORI INFORMAZIONI SULLA VETTURA <?php echo strtoupper($monousato->descmarca).' '.strtoupper($monousato->descallestimento);?> (cod.<?=$monousato->cod; ?>).</textarea>
										</div>
									</div>
									<div><p>Tutti i campi sono obbligatori</p></div>
									<div class="row">
										<div class="large-12 columns margin-top-1">
											<input class="top-6" id="authorize-action" tabindex="6" type="checkbox" name="privacy" value="Si" required><label class="authorize-action-label" for="authorize-action">Autorizzo il trattamento dei dati personali del D.Lgs 196/2003</label>
										</div>
										<div class="large-12 columns text-right padding-top-20">
											<input type="submit" tabindex="7" class="fullWidth button" value="INVIA" >
										</div>
									</div>
								</form>
							</div>
						</div>


						<div class="row padding-top-08 collapse">
							<div class="small-12 medium-12 columns padding-right-02">
								<a href="/frontend/dettaglio/cod:<?= $monousato->cod; ?>" class="button fullWidth font-size-08 ">DETTAGLIO ANNUNCIO</a>
							</div>
							<div class="small-2 medium-1 large-2  columns text-center">
								<a class="social-icon button button-gray" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/' . $_GET['cat'] .'/'. $auto['id'] )[1]; ?>"><i class="fa fa-facebook"></i></a>
							</div>
							<div class="small-2 medium-1 large-2  columns text-center">
								<a class="social-icon button button-gray" target="_blank" href="https://plus.google.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/' . $_GET['cat'] .'/'. $auto['id'] )[1];  ?>"><i class="fa fa-google-plus"></i></a>
							</div>
							<div class="small-2 medium-1 large-2  columns text-center">
								<a class="social-icon button button-gray" target="_blank" href="https://twitter.com/home?status= <?php echo $auto['descrizione']; ?> <?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/' . $_GET['cat'] .'/'. $auto['id'] )[1];  ?>"><i class="fa fa-twitter"></i></a>
							</div>
							<div class="small-6 medium-5 large-6 columns text-center padding-left-02 ">
								<a href="#" data-car="<?php echo $monousato->cod;?>" class="preferiti-btn button fullWidth button-gray font-size-08"><i class="fa fa-car"></i> 
								<?php 
								if( in_array($monousato->cod, $_SESSION['wishlist']['products']) ){
									echo "Riumuovi dai";
								}
								else{
									echo "Aggiungi ai";	
								}
								?> preferiti
								</a>
							</div>
						</div>

						
					</div>
				</div>
				<hr class="padding-bottom-10">
		<?php } ?>
			</div>
			<?php if($listusato) { ?> 
			<div class="row collapse padding-10">
				<div class="small-12 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-left margin-bottom-10">
					<div class="padding-left-10">
						RISULTATI VISIBILI:
						<?php 
						if(strlen($_SESSION['perpage'])>0){
							if($_SESSION['perpage']==25){
								$colore10 = "#ccc";
								$colore20 = "white";
								$colore50 = "white";
								$coloretutte = "white";
							}elseif($_SESSION['perpage']==50){
								$colore10 = "white";
								$colore20 = "#ccc";
								$colore50 = "white";
								$coloretutte = "white";
							}elseif($_SESSION['perpage']==5000){
								$colore10 = "white";
								$colore20 = "white";
								$colore50 = "white";
								$coloretutte = "#ccc";
							}else{
								$colore10 = "white";
								$colore20 = "white";
								$colore50 = "#ccc";
								$coloretutte = "white";
							}
							
						}else{
							$colore10 = "#ccc";
							$colore20 = "white";
							$colore50 = "white";
							$coloretutte = "white";
						}
						?>
						<a href="/frontend/usato/perpage/num:25" style="color:<?=$colore10;?>; text-decoration:underline; font-weight:bold;">25</a>&nbsp;
						<a href="/frontend/usato/perpage/num:50" style="color:<?=$colore20;?>; text-decoration:underline; font-weight:bold;">50</a>&nbsp;
						<a href="/frontend/usato/perpage/num:100" style="color:<?=$colore50;?>; text-decoration:underline; font-weight:bold;">100</a>
						<a href="/frontend/usato/perpage/num:5000" style="color:<?=$coloretutte;?>; text-decoration:underline; font-weight:bold;">Tutte</a>
					</div>
				</div>
				
				<div class="small-12 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-right margin-bottom-10">
					<?php 
					$paginetot = ceil($numoff/$perPage);
					//echo 'p:'.$numoff.'/'.$perPage.' = '.$paginetot;
					?>
					
					<span  class="total-pages">PAGINA <span class="font-bold">1</span> DI <span class="font-bold"><?=$paginetot; ?></span></span>

					<span class="font-bold margin-right-10 number-pages"><i class="fa fa-caret-left"></i>
					&nbsp;&nbsp;
					<?php 
					for ($p=1; $p <= $paginetot; $p++) { 
						if($numpag==$p){
							$colorpage = "#ccc";
						}else{
							$colorpage = "white";
						}
					?>
						<a href="/frontend/usato/pag:<?=$p; ?>" style="color:<?=$colorpage; ?>; text-decoration:underline;"><?=$p;?></a>&nbsp;
					<?php 
					}
					?>
					&nbsp;&nbsp;
					<i class="fa fa-caret-right"></i> </span>
				</div>
				
			</div> 
			<?php } ?> 

			

		</div>
	</div>

	
</div>