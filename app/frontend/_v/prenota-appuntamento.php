<form action="/frontend/invio/prenotazione" method="post" class="padding-bottom-10">
			
			<input type="hidden" name="gyu_return" value="<?= $_SERVER['REQUEST_URI']; ?>" />
			<input type="text" name="gyu_ctrl" value="" style="opacity: 0; height: 0px; width: 0px; margin: 0px; padding: 0px;" />

	<div id="form-prenota" class="row">
		<div class="large-6 columns padding-top-20">
			<div class="font-bold padding-bottom-10">DATI PERSONALI</div>
			<label>
				Nome e Cognome*
				<input type="text" name="nomecognome" required>
			</label>
			<label>
				Indirizzo di Residenza
				<input type="text" name="indirizzoresidenza" >
			</label>
			<label>
				Citt&agrave; di Residenza
				<input type="text" name="cittaresidenza" >
			</label>
			<label>
				Telefono
				<input type="text" name="telefono" >
			</label>
			
		</div>
		<div class="large-6 columns padding-top-20">
		<div class="font-bold padding-bottom-10">&nbsp;</div>
		<label>
				Cellulare
				<input type="text" name="cellulare" >
			</label>
			<label>
				Fax
				<input type="text" name="fax" >
			</label>
			<label>
				Email*
				<input type="email" name="email" required>
			</label>
			<label>
				Tipo Appuntamento* (tagliando, carrozzeria, meccanico, ...)
				<input type="text" name="tipoappuntamento" required>
			</label>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<label>
				Messaggio*
				<textarea name="msg" rows="10" required></textarea>
			</label>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns text-left padding-bottom-10">
			* Campi Obbligatori
		</div>
	</div>
	<div class="row">
		<div class="medium-12 columns margin-top-1">
			<input id="authorize-action" name="privacy" value="Si" style="margin-top:5px;" tabindex="6" type="checkbox" required><label class="authorize-action-label" for="authorize-action">Acconsento al trattamento dei dati ai sensi e per gli effetti di cui all'articolo 13, D.Lgs 30/06/2003.</label>
		</div>
	</div>

	<div class="row padding-top-10">
		<div class="medium-6 columns text-right hide-for-small-only">
			&nbsp;
		</div>
		<div class="medium-6 columns text-right">
			<input class="button fullWidth font-bold" type="submit" value="Invia">
		</div>
	</div>

</form>