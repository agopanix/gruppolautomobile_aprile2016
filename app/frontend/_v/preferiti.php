<?
$preferiti = LoadApp("wishlists", 1)->apiLists();
?>

<div class="row margin-top-20">
	<div class="large-12 columns">
		<div class="row collapse">
			<div class="large-12 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-left margin-bottom-10">
				<div class="padding-left-10">RISULTATO RICERCA</div>
			</div>
			<!-- <div class="large-6 columns bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-right margin-bottom-10 padding-left-10">
				<i class="fa fa-sort-desc floatright margin-right-10"></i>
				ORDINA PER:
				<select class="oderbyselect">
					<option value="orderbyprice">PREZZO DECRESCENTE</option>
					<option value="orderbyprice">PREZZO CRESCENTE</option>
				</select>
				&nbsp;
			</div>-->
		</div>
		<!-- <div class="row">
			<div class="small-12 columns">
				<ul class="category-list">
					<?php //foreach ($this->frontend->categorie as $categoria) { ?>
					<?php foreach ($preferiti['auto'] as $i=>$auto) { ?>
					<li>
						<input type="checkbox"  name="ctagorie_group[]" value="<?php echo $categoria['id'];?>">
						<?php echo $categoria['name'];?>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div> -->
		
		<div class="row">
			<?php if($preferiti['auto']){?>
			<div class="small-12 columns">
				<?php foreach ($preferiti['auto'] as $i=>$auto) { 
					//var_dump($preferiti['auto']);
					?>

				<div class="row car-schedule">
					<div class="medium-4 columns padding-top-10">
						<img 
						data-interchange="
						[/timthumb.php?src=<?php echo $auto['img'];?>&h=300&w=640&zc=1, (small)],
						[/timthumb.php?src=<?php echo $auto['img'];?>&w=300&h=260&zc=1, (medium)],
						[/timthumb.php?src=<?php echo $auto['img'];?>&w=300&h=220&zc=1, (large)]"
						src="" >
					</div>
					<div class="medium-8 columns  padding-top-10">
						<div class="row primary-informations collapse">
							<div class="small-6 large-3 columns text-center padding-top-05 padding-bottom-05"><?php echo callFunction('frontend','price',$auto['prezzostimato'],'€');?></div>
							<div class="small-6 large-3 columns text-center padding-top-05 padding-bottom-05"><?php echo $auto['cat'];?> </div>
							<div class="small-6 large-3 columns text-center padding-top-05 padding-bottom-05"><?php echo $auto['immatric'];?></div>
							<div class="small-6 large-3 columns text-center padding-top-05 padding-bottom-05"><?php echo $auto['cav'];?> cv</div>
						</div>
						<div class="row">
							<div class="medium-8 columns padding-top-10">
								<span class="font-bold color-7"><?php echo $auto['descrizione'];?></span><br>
								<div class="description padding-top-10">
									<span class="font-size-09 color-7">
										<?php
											echo $auto['equip']; 
										?>
									</span>
								</div>
							</div>
							<div class="medium-4 columns">
								<ul class="font-size-09 padding-top-10">
									<li class="color-7"><?php echo $auto['tipint']; ?></li>
								</ul>
							</div>
						</div>
						<div class="row padding-top-10 collapse">
							<div class="small-6 medium-12 columns">
								<a href="<? echo createUrl('frontend/dettaglio/cod:' . $auto['cod'] )[1];  ?>" class="button fullWidth font-size-08 ">VEDI ANNUNCIO</a>
							</div>
							<div class="small-2 medium-2 columns text-center">
								<a class="social-icon button button-gray" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/cod:' . $auto['cod'] )[1];  ?>"><i class="fa fa-facebook"></i></a>
							</div>
							<div class="small-2 medium-2 columns text-center">
								<a class="social-icon button button-gray" target="_blank" href="https://plus.google.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/cod:' . $auto['cod'] )[1]; ?>"><i class="fa fa-google-plus"></i></a>
							</div>
							<div class="small-2 medium-2 columns text-center">
								<a class="social-icon button button-gray" target="_blank" 
								href="https://twitter.com/home?status= <?php echo $auto['descrizione']; ?> <?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('frontend/dettaglio/cod:' . $auto['cod'] )[1];  ?>"><i class="fa fa-twitter"></i></a>
							</div>
							<div class="medium-6 columns text-center">
								<a class="button fullWidth button-gray font-size-08 preferiti-btn" data-car="<? echo $auto["cod"];?>" ><i class="fa fa-car"></i> RIMUOVI DAI PREFERITI</a>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php } else {?>
				<div class="small-12 columns text-center padding-top-20 padding-bottom-20">
					<p class="font-bold font-size-13">La tua lista dei preferiti è vuota.</p>
				</div>
			<?php } ?>
		</div>
	</div>
	
</div>
</div>