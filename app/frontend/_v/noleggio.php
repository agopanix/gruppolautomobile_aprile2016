
<?php 
	$items      = LoadClass('servizi', 1)->filter( ' WHERE servizio_id = 1 ' );
		foreach ($items as $item) {
?>
<div class="row">
	<div class="small-12 columns padding-top-10 font-size-15 font-bold">
		<?php echo $item->getTitle();?>
	</div>
</div>
<div class="row">
	<div class="small-12 columns font-size-12  padding-top-10 color-7 font-bold">
		<?php echo $item->getExcerpt();?>
	</div>
</div>
<div class="row">
	<div class="medium-12 columns padding-top-10 color-7">
		<div class="row">
			<div class="medium-12 columns">
				<img class="fullWidth" src="<?php echo $item->getAttr('img');?>" title="">
			</div>
			<div class="medium-12 columns padding-top-10">
				<?php echo $item->getAttr('content');?>
			</div>
		</div>
	</div>
</div>
<div class="bkg-color-0">
	<div class="row">
		<div class="small-12 columns padding-bottom-10">
				<ul class="concessionari mobilita ">
					<li>
						<a href="http://autorent-ald.com/" target="_bloank">
							<div class="relative area-logo">
								<img class="centered-text" src="/cdn/images/automobile-ald-automotive.png">
							</div>
						</a>	
					</li>
					<li>
						<a href="http://automobile.nltauto.it/" target="_blank">
							<div class="relative area-logo">
								<img class="centered-text" src="/cdn/images/automobile-arval.png">
							</div>
						</a>
					</li>
						
				</ul>
		</div>
	</div>
</div>

<?php 
		}
?>
<div class="row">
	<div class="small-12 columns padding-top-10">
		<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">COMPILA IL MODULO PER RICHIEDERE INFORMAZIONI SUL NOLEGGIO</div>
	</div>
</div>
<?
/*
<form action="/frontend/invio/noleggioLungo" method="POST">
	
	<input type="hidden" name="gyu_return" value="<?= $_SERVER['REQUEST_URI']; ?>" />
	<input type="text" name="gyu_ctrl" value="" style="opacity: 0; height: 0px; width: 0px; margin: 0px; padding: 0px;" />

<div class="row">
	<div class="medium-12 columns">
		<div class="row">
			<div class="medium-12 columns">
				<input type="text"  class="big" placeholder="NOME E COGNOME*"  tabindex="1" required/>
			</div>
			<div class="medium-6 columns">
				<input type="number"  class="big" placeholder="CELLULARE O TELEFONO*"  tabindex="2" required/>
			</div>
			<div class="medium-6 columns">
				<input type="email"  class="big" placeholder="EMAIL*"  tabindex="2" required/>
			</div>
			<div class="medium-6 columns">
				<input type="text"  name="marca" class="big" placeholder="MARCA DI INTERESSE*"  tabindex="3" required/>
			</div>
			<div class="medium-6 columns">
				<input type="text" name="modello"  class="big" placeholder="MODELLO DI INTERESSE*"  tabindex="4" required/>
			</div>
			<div class="medium-6 columns">
				<input type="text" name="limitekm"  class="big" placeholder="MAX KM/ANNO (Limite Km in un anno)*"  tabindex="5" required/>
			</div>
			<div class="medium-6 columns">
				<input type="number" name="duratanoleggio"  class="big" placeholder="DURATA DEL NOLEGGIO IN ANNI*"  tabindex="6" required/>
			</div>
			<div class="medium-6 columns">
				<input type="number" name="partitaiva"  class="big" placeholder="PARTITA IVA*"  onKeyDown="if(this.value.length==11) this.value = this.value.slice(0, - 1);"  tabindex="7" required />
			</div>
			<div class="medium-6 columns">
				<input type="text" name="ragionesociale"  class="big" placeholder="RAGIONE SOCIALE*"  tabindex="8" required />
			</div>
			<div class="medium-12 columns">
				<textarea rows="4" cols="30"  name="messaggio" tabindex="5" placeholder="MESSAGGIO" tabindex="9" ></textarea>
			</div>
		</div>

		<div class="row">
			<div class="small-12 columns padding-bottom-10">
				Tutti i campi contrassegnati con * sono obbligatori.
			</div>
		</div>

		<div class="row">
			<div class="small-12 padding-top-10 columns">
				Preferenza sulla modalità di contatto.  <input type="checkbox" name="ricontattotel" value="1"> Cellulare - <input type="checkbox" name="ricontattoemail" value="1"> Email
			</div>
		</div>
	
		<div class="row">
			<div class="medium-12 columns margin-top-1">
				<input id="authorize-action" style="margin-top:5px;" tabindex="6" type="checkbox" required><label class="authorize-action-label" for="authorize-action">Acconsento al trattamento dei dati ai sensi e per gli effetti di cui all'articolo 13, D.Lgs 30/06/2003.</label>
			</div>
		</div>
		<div class="row padding-top-20 padding-bottom-20">
			<div class="medium-12 columns text-right ">
				<button type="submit" tabindex="7" >Invia</button>
			</div>
		</div>
	</div>
</div>
</form>
*/

#Application("frontend/_v/commons/form-noleggio", null, Array("noleggioBreve"=>0, "noleggioLungo"=>1));
Application("frontend/_v/commons/form-noleggio", null, Array("type"=>"noleggioLungo"));
?>