<?php

//var_dump($this->frontend);

?>

<div class="row">

	<div class="small-12 columns">

		<div class="row">

			<div class="small-12 columns text-center padding-top-20 hidden-for-small-only">

				<?php 

				  $items = LoadClass('paginahome', 1)->filter( ' ORDER BY paginahome_id DESC ' );
				?>
				<?php 
				  	foreach ($items as $item) {
				?>

				<!-- <h1 class="h1 titolo-sezione font-bold color-6"><?= $item->getAttr('title') ?></h1>-->
				<p>

					<span class="content color-7">

					<?= $item->getAttr('content') ?>

					</span>

					<!--<a href="">Visualizza Altro <i class="fa fa-sort-desc"></i> </a>-->

				</p>

				<?php 

					}

				?>

			</div>
		</div>
		<div class="row show-for-small-only padding-top-10">
		
			<div class="small-12 columns">
				<div class="titolo-sezione tfooter font-bold color-6 text-center">
					Ricerca
				</div>
				<form id="app" action="/frontend/usato/cercatop" method="post" class="padding-top-10">
					<div class="small-12 columns">
						<div class="row collapse">
							<div class="medium-2 columns">
								<select name="marchio">
					                <option value="TUTTI">Marca</option>
					                <?php 
					                $listamarchi = LoadApp('usato',1)->apiMarchiusato();

					                foreach ($listamarchi as $marchio) {
					                  if($_SESSION["marchiosel"]==strtoupper($marchio->descmarca)){
					                    $selectedmarchio = "selected";
					                  }else{
					                    $selectedmarchio = "";
					                  }
					                  echo '<option value="'.strtoupper($marchio->descmarca).'" '.$selectedmarchio.'>'.strtoupper($marchio->descmarca).'</option>';
					                }
					                ?>
					            </select>
							</div>
							<div class="medium-2 columns">
								<?php 
								if(strlen($_SESSION["modellosel"])>0){
									$miomodello = $_SESSION["modellosel"];
								}else{
									$miomodello = "";
								}
								?>
								<input type="text" name="modello" placeholder="Modello" value="<?=$miomodello; ?>">
							</div>
							<div class="medium-2 columns">
								<select name="aprezzo">
									<option value="">Prezzo fino a</option>
									<option value="5000">    5.000 €</option>
									<option value="10000">  10.000 €</option>
									<option value="15000">  15.000 €</option>
									<option value="20000">  20.000 €</option>
									<option value="30000">  30.000 €</option>
									<option value="50000">  50.000 €</option>
								</select>
							</div>
							<div class="medium-2 columns">
								<select name="akmperco">
									<option value="">	   KM fino a</option>
									<option value="5000">    5.000 KM</option>
									<option value="10000">  10.000 KM</option>
									<option value="15000">  15.000 KM</option>
									<option value="20000">  20.000 KM</option>
									<option value="30000">  30.000 KM</option>
									<option value="50000">  50.000 KM</option>
									<option value="100000">100.000 KM</option>
									<option value="200000">200.000 KM</option>
								</select>
							</div>
							<div class="medium-2 columns">
								<select name="daimmatricolaz">
									<option value="">Anno da</option>
									<option value="2014">2014</option>
									<option value="2013">2013</option>
									<option value="2012">2012</option>
									<option value="2010">2011</option>
									<option value="2009">2010</option>
									<option value="2008">2009</option>
									<option value="2007">2008</option>
									<option value="2006">2007</option>
									<option value="2005">2005</option>
									<option value="2004">2004</option>
								</select>
							</div>
							<div class="medium-2 columns small text-left">
								<button class="button submit width100 heightHowInput" type="submit">Ricerca</button>
							</div>
						</div>
					</div>
					</form>
			</div>
		</div>
		<div class="row">

			<?php 
			$link = array('neopatentati','ivaesposta','kmzero','cambioautomatico','sportive','suv');
			$i = 0;
			foreach ($this->frontend->categorie as $cat) { 
			?>

			<div class=" medium-4 card columns text-center relative padding-top-10">

				<a class="category-link" href="/api/frontend.usato.<? echo $link[$i]; ?>">

					<div class="box-background relative">

						<img class="categoryimg" src="/timthumb.php?src=<?php echo $cat['img'];?>&w=900" >

						<!-- <div class="titolo-categoria centered-text color-0 font-bold font-size-20"><?php echo $cat['name'];?></div>-->

						<div class="description-2 padding-top-05 padding-bottom-05"><?php echo $cat['name'];?></div>

					</div>

				</a>

			</div>

			<?php 
				$i++;
			} 
			?>

		</div>

		<!-- AUTO IN OFFERTA -->

		<div class="row padding-top-20 padding-bottom-20">

			<div class="columns small-12 text-center">

				<div class="titolo-sezione font-bold color-6">Scopri le offerte della settimana</div>

				<div>

					<a href="/frontend/usato" class="color-6" >Visualizza tutti i migliori modelli di usato garantito »</a>

				</div>

			</div>

		</div>

		<div class="row padding-bottom-40">

			

			<?php foreach ($this->inofferta as $miousato) { ?>

			<?php 
                if(strlen(trim($miousato->foto1))>0){
                	$url = 'http://www.ocholding.com/infocar/infocar/'.$miousato->foto1.'';
				
					$mioarrayurl = get_headers($url, 1);
					if($mioarrayurl[0]=="HTTP/1.1 200 OK" || $mioarrayurl[0]=="HTTP/1.0 200 OK"){
	                	$foto1 = "http://www.ocholding.com/infocar/infocar/".$miousato->foto1."";
						$lauto = 0;
					}else{
						$foto1 = "http://www.ocholding.com/infocar/infocar2/".$miousato->foto1."";
						$lauto = 1;
					}
                }else{
                	$foto1 = "/cdn/images/nofoto.jpg";
                }

                $mioprezzook = "";
                $mioprezzookstampa = "";
                $mioprezveurook = "";
                $mioprezvaltok = "";

                
                $miocav = "";
                $miokw = "";


						if($lauto == 0){
							
							if(strlen($miousato->prezzointernet)>0 && $miousato->prezzointernet > 0){
								$mioprezzook = number_format($miousato->prezzointernet, 0, ",", ".");
								$mioprezzookstampa = number_format($miousato->prezzointernet, 0, ",", ".");
							}else{
								$mioprezzook = number_format($miousato->stimato, 0, ",", ".");
								$mioprezzookstampa = number_format($miousato->stimato, 0, ",", ".");
							}

							$mioprezveurook = $miousato->ppneuro;

							$mioprezvaltok = $miousato->prezzoveuro;

							
							 
						}else{
							if(strlen($miousato->prezzointernet)>0 && $miousato->prezzointernet > 0){
								$mioprezzook = number_format($miousato->prezzointernet, 0, ",", ".");
								$mioprezzookstampa = number_format($miousato->prezzointernet, 0, ",", ".");
							}else{
								$mioprezzook = $miousato->prezzoveuro;
								$mioprezzookstampa = $miousato->prezzoveuro;
							}

							$mioprezveurook = $miousato->prezzoveuro;

							$mioprezvaltok = $miousato->prezzovalt;

							
							 
						}

						if($miousato->kw > 0){
								
								$miokw = $miousato->kw;
								$miocav = $miousato->cav;
						}else{
								
								$miokw = $miousato->cav;
								$miocav = $miousato->immatricolaz;
						}
						?>

			<div class="medium-6 columns text-center relative padding-top-10">

				<div class="card relative home-offers">

					<a href="/frontend/dettaglio/cod:<?=$miousato->cod; ?>">

						<img data-interchange="

						[/timthumb.php?src=<?php echo $foto1;?>&h=700&w=900&zc=1, (small)],

						[/timthumb.php?src=<?php echo $foto1;?>&h=455&zc=1, (medium)],

						[/timthumb.php?src=<?php echo $foto1;?>&h=455&zc=1, (large)]"

						src="/timthumb.php?src=<?php echo $foto1;?>&h=455&zc=1" >

						<div class="description padding-top-05 padding-bottom-05 text-left font-bold">

							<div class="small-4 medium-8 columns truncate">

								<?=strtoupper($miousato->descmarca) ?> <?=strtoupper($miousato->descallestimento) ?>

							</div>

							<div class="small-8 medium-4 columns color-8 text-right">

								<?php
								if($mioprezzook>0){
									echo '&euro; '.$mioprezzook;
								}else{
									echo 'Trattativa Riservata';
								}
								?>

							</div>

						</div>

					</a>

				</div>

			</div>

					

			<?php } ?>

		</div>

	</div>

</div>



<div class="fullWidth padding-for-small bkg-color-0 padding-bottom-20">

	<div class="row">

		<div class="small-12 columns text-center">

				<div class="titolo-sezione font-bold color-6 padding-top-10">Servizi Aggiuntivi</div>

				<a href="" class="color-6" >Solo il meglio per i nostri clienti</a>

		</div>

	</div>

	<div class="row">

		<?php 

		  	$items      = LoadClass('servizi', 1)->filter( ' ORDER BY servizio_id ASC ' );

		  	foreach ($items as $item) {

		  		//echo $item->getAttr('servizio_id');

		  			if($item->getAttr('servizio_id')==1){

						// link pagina Noleggio

						$linkserv = "/frontend/noleggio/lungotermine";

					}elseif($item->getAttr('servizio_id')==2){

						// link pagina Carrozzeria

						$linkserv = "/frontend/noleggio/brevetermine";

					}else{

						// link accesso Area Convenzioni

						$linkserv = "/frontend/convenzioni/";

					}



					

					if(strlen($item->getAttr('img'))>0){

						$imgserv = $item->getAttr('img');

					}else{

						$imgserv = "/upl/servizio-1.png";

					}

					

		?>

			<div class="medium-4 columns relative padding-top-10">

				<div class="servizi-aggiuntivi card relative">

				

					

					<a href="<?php echo $linkserv;?>">



						<!-- <img data-interchange="

						[/timthumb.php?src=<?php echo $auto['img'];?>&h=700&w=900&zc=1, (small)],

						[/timthumb.php?src=<?php echo $auto['img'];?>&h=455&zc=1, (medium)],

						[/timthumb.php?src=<?php echo $auto['img'];?>&h=455&zc=1, (large)]"

						src="/timthumb.php?src=<?php echo $auto['img'];?>&h=455&zc=1" > -->

						

						<img src="<?= CallFunction('servizi', 'resize', $imgserv, 640, 740); ?>" >

						

						<div class="description-2 padding-top-05 padding-bottom-05">

							<div class="text  text-center centered-text">

								<span class="titolo titolo-txt line-height-10 font-bold"><?php echo $item->getAttr('title');?></span><br>

								<span class="descrizione descrizione-txt"><?php echo $item->getExcerpt();?></span>

							</div>

						</div>

					</a>

					

				</div>

			</div>

		<?php 

			}

		?>

		<?php 

		/*

		<?php 

		  $items      = LoadClass('servizi', 1)->filter( ' ORDER BY servizio_id ASC ' );

		?>

		<?php foreach ($items as $item) { ?>

			

			<div class="medium-4 columns relative padding-top-10">

				<div class="servizi-aggiuntivi card relative">

					<?php 

					if($item->getAttr('servizio_id')==1){

						// link pagina Noleggio

						$linkserv = "/frontend/noleggio/";

					}elseif($item->getAttr('servizio_id')==2){

						// link pagina Carrozzeria

						$linkserv = "/frontend/carrozzeria/";

					}else{

						// link accesso Area Convenzioni

						$linkserv = "/frontend/convenzioni/";

					}



					if(strlen($item->getAttr('img'))>0){

						$imgserv = $item->getAttr('img'):

					}else{

						$imgserv = "/upl/servizio-1.png";

					}

					?>

					<a href="<?php echo $linkserv;?>">

						<img

						data-interchange="

						[/timthumb.php?src=<?php echo $imgserv;?>&h=700&w=900&zc=1, (small)],

						[/timthumb.php?src=<?php echo $imgserv;?>&h=455&zc=1, (medium)],

						[/timthumb.php?src=<?php echo $imgserv;?>&h=900&zc=1, (large)]"

						src="/timthumb.php?src=<?php echo $imgserv;?>&h=908&zc=1" >

						<div class="description-2 padding-top-05 padding-bottom-05">

							<div class="text  text-center centered-text">

								<span class="titolo titolo-txt line-height-10 font-bold"><?php echo $item->getAttr('title');?></span><br>

								<span class="descrizione descrizione-txt"><?php echo $item->getExcerpt();?></span>

							</div>

						</div>

					</a>

				</div>

			</div>

			

			<?php } ?>

			**/

			?>

	</div>

</div>

