<?
$preferiti = LoadApp("wishlists", 1)->apiLists();
?>
<div class="listapreferiti useraction">
	<div id="listapreferiti-btn"  class="label-text relative vieniatrovarci-btn">
		<img class="centered-text" src="/cdn/images/label-lista-preferiti.png">
	</div>
	<div id="listapreferitiarea" class="closed area font-size-13 animate">
		<div class="row title">
			<div class="small-12 columns  padding-bottom-05">
				<a class="preferiti-link" href="<? echo createUrl('frontend/preferiti/' )[1]; ?>">
					<i class="fa fa-car"></i> <span class="font-bold">LISTA PREFERITI</span>
				</a>
			</div>
		</div>
		<div  class="lista-preferiti">
		<?php foreach ($preferiti['auto'] as $i=>$auto) { ?>
			<div class="row padding-top-05">
				<div class="small-2 columns">
					<img class="preview" src="/timthumb.php?src=<?php echo $auto['img'];?>&w=58px&h=44px" >
				</div>
				<div class="small-10 columns font-size-07">
					<span class="font-bold"><?php echo $auto['descrizione'];?></span><br>
					<a href="<? echo $auto['url'] . '/frontend/dettaglio/cod:' .  $auto['cod'] ; ?>">Visualizza »</a>
				</div>
			</div>
		<?php }?>
		</div>
	</div>
</div>
