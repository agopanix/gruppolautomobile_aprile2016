<?
$frontend = $app_data->config;

?>

<!doctype html>

<html class="no-js" lang="en">

	<head>

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><? echo $frontend["title"]; ?></title>
		<meta name="description" value="<?php echo $frontend["description"]; ?>" />
		<!-- <script src="//fast.eager.io/6iWfr1UhwG.js"></script>  -->

		<link rel="stylesheet" href="/cdn/css/foundation.css" />

		<script src="/cdn/js/vendor/modernizr.js"></script>

		<script src="/cdn/js/vendor/vue.js"></script>

		<script src="https://maps.googleapis.com/maps/api/js"></script>

		<link rel="stylesheet" href="/cdn/css/gruppolautomobile.css" />

		<link rel="stylesheet" href="/cdn/css/font-awesome/css/font-awesome.min.css" />

		

		<link rel="apple-touch-icon" sizes="57x57" href="/cdn/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/cdn/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/cdn/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/cdn/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/cdn/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/cdn/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/cdn/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/cdn/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/cdn/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/cdn/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/cdn/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/cdn/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/cdn/favicon/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/cdn/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!-- socials -->

		<meta property="og:title" content="<?php echo $frontend["title"]; ?>" />
		<meta property="og:url" content="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" />
		<meta property="og:description" content="<?php echo $frontend["description"]; ?>"/>
		<meta property="og:site_name" content="<? echo siteName; ?>">
		<meta property="og:type" content="website" />
		<meta property="og:updated_time" content="<? echo time(); ?>" />

		<meta name="twitter:card" content="photo">
		<meta name="twitter:site" content="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
		<meta name="twitter:creator" content="@Gruppolautomobile">
		<meta name="twitter:url" content="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
		<meta name="twitter:description" content="<?php echo $frontend["description"]; ?>">
		<?
		# echo '<!--';
		# 	print_r($frontend['miousato'][0]);
		# echo '!-->';
		?>
		<?php if(isset($app_data->dettaglio_veicolo)){?>

		<meta name="twitter:title" content="<?php echo $frontend["title"]; ?>">

		<?
		#if(file_exists(upload.$frontend['miousato'][0]->foto1)){
			#http://www.gruppolautomobile.it/timthumb.php?src=http://www.ocholding.com/infocar/infocar2/1399453.JPG&w=600&h=420&zc=1&a=c
			#$http://www.gruppolautomobile.it/timthumb.php?src=http://www.ocholding.com/infocar/infocar2/1399453.JPG&w=600&h=420&zc=1&a=c
			$url = 'http://www.gruppolautomobile.it/timthumb.php?src=http://www.ocholding.com/infocar/infocar2/'.$frontend['miousato'][0]->foto1.'&w=470&h=245&zc=1&a=c';
			?>
			<meta property="og:image" itemprop="image" content="<?php echo $url;?>" />

			<meta name="twitter:image" content="<?php echo $url; ?>" />
			<?
		#}
		?>

		<?php } ?>


		<script>
		 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		 ga('create', 'UA-63409501-1', 'auto');
		 ga('send', 'pageview');

		</script>

	</head>

	<body class="bkg-color-2">

		<div class="row bkg-color-0 text-center fullWidth relative hidden-for-small">

			<a href="/">

				<img class="logo margin-bottom-10 margin-top-10" src="/cdn/images/main-logo.png">

			</a>

			<?php 
			if(($_REQUEST['send']==1)||($_REQUEST['sendFooter']==1)||($_REQUEST['sendInfo']==1)||($_REQUEST['sendPrenotazione']==1)||($_REQUEST['sendUsato']==1)
				||($_REQUEST['sendNoleggioBreve']==1)

				){
			?>
			<div class="email-message ok">
				Grazie per aver inviato il Suo messaggio. <br>
				La Sua richiesta verrà trattata nel minor tempo possibile ed 
				un Nostro Responsabile La contatterà per rispondere alle Sue esigenze.
			</div>
			<?php 
			}
			else if(($_REQUEST['sendFooter']==2)||($_REQUEST['sendInfo']==2)||($_REQUEST['sendPrenotazione']==2)||($_REQUEST['sendUsato']==2)
				||($_REQUEST['sendNoleggioBreve']==2)
				){
				?>
				<div class="email-message ko">
					Errore nell'invio
				</div>
				<?
			}
			?>

		</div>

		<div class="show-for-small-down text-center bkg-color-0">
				<a href="/">
				<img style="max-width:320px;" class="logo" src="/cdn/images/main-logo.png">
				</a>
		</div>

		<div class="header fullWidth relative <?php if(isset($frontend["little-header"])){ echo 'little-header'; } ?>">
			<nav class="mainmenu top-bar" data-topbar data-options="back_text: <i class='fa fa-chevron-left'></i> Chiudi" role="navigation">
				<ul class="title-area">
					<li class="name show-for-small-down">
						<h1><!-- <a href="/" class="underline"><span class="italic">Gruppo</span> L'Automobile</a>--></h1>
					</li>
					<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
				</ul>
				<section class="top-bar-section">
					<ul class="left">
						<?php if(isset($frontend["showHomebtn"])){?>

						<li><a class="shadow" href="/"> <i class="fa fa-home"></i> Torna al Sito</a></li>

						<?php } ?>

						<li class="has-dropdown <? echo $frontend["voceMenu"] == 'categoria' ? 'active' : ''; ?>">
							<a class="shadow" href="/frontend/usato"> <i class="fa fa-car"></i> Scegli la tua auto</a>
							<ul class="dropdown">
							<!--
								<?php foreach ($app_data->categorie as $cat) { ?>
								<li><a href="<? echo createUrl('frontend/categoria/' . $cat['id'] )[1]; ?>"><?php echo $cat['name'];?></a></li>
								<?php } ?>
							-->
								<li class="sub-v"><a href="/api/frontend.usato.neopatentati">Neopatentati</a></li>
								<li class="sub-v"><a href="/api/frontend.usato.ivaesposta">Iva Esposta</a></li>
								<li class="sub-v"><a href="/api/frontend.usato.kmzero">Aziendali Km0</a></li>
								<li class="sub-v"><a href="/api/frontend.usato.cambioautomatico">Cambio Automatico</a></li>
								<li class="sub-v"><a href="/api/frontend.usato.sportive">Sportive</a></li>
								<li class="sub-v"><a href="/api/frontend.usato.suv">Suv &amp; Fuori Strada</a></li>
							</ul>
						</li>
						
						
						<li <? echo $frontend["voceMenu"] == 'preferiti' ? 'class="active"' : ''; ?>><a  class="shadow" href="<? echo createUrl('frontend/preferiti/GetIndex')[1]; ?>"><i class="fa fa-car"></i> Lista dei preferiti</a></li>
						<li <? echo $frontend["voceMenu"] == 'chisiamo' ? 'class="active"' : ''; ?>><a class="shadow" href="<? echo createUrl('frontend/chisiamo/GetIndex')[1]; ?>"> <i class="fa fa-heart"></i> Chi Siamo</a></li>
						<li <? echo $frontend["voceMenu"] == 'dovesiamo' ? 'class="active"' : ''; ?>><a class="shadow" href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>"> <i class="fa fa-map-marker"></i> Dove Siamo</a></li>
						<li <? echo $frontend["voceMenu"] == 'contatti' ? 'class="active"' : ''; ?>><a class="shadow" href="<? echo createUrl('frontend/contatti/GetIndex')[1]; ?>"> <i class="fa fa-envelope"></i> Contatti</a></li>
						<li class="socials-header show-for-medium-up">
							<a class="link" href="https://www.facebook.com/pages/Auto365/398395653621974?ref=hl" target="_blank" ><i class="fa fa-facebook-official"></i></a>
							<a class="link" href="https://plus.google.com/111367938131787325252" target="_blank"><i class="fa fa-google-plus-square" ></i></a>
							<a class="link"  href="https://twitter.com/_auto365" target="_blank"><i class="fa fa-twitter-square" ></i></a>
						</li>
						<!--<li><a class="shadow hide-for-small-only" href="#" id="vieniatrovarci-btn-menu"> <i class="fa fa-calendar"></i> Vieni a tovarci</a></li>
						<li><a class="shadow" href="/frontend/preferiti/index"> <i class="fa fa-car"></i> Lista dei preferiti</a></li>
						<li><a class="shadow" href="/frontend/chisiamo/index"> <i class="fa fa-heart"></i> Chi Siamo</a></li>
						<li><a class="shadow" href="/frontend/dovesiamo/index"> <i class="fa fa-map-marker"></i> Dove Siamo</a></li>
						<li><a class="shadow" href="/frontend/contatti/index"> <i class="fa fa-envelope"></i> Contatti</a></li>-->
					</ul>
				</section>
			</nav>

			<?php if($frontend["viewscroller"]){ ?>

				<ul class="orbitheader relative"

				data-orbit

				data-options="animation:slide;

				timer_speed: 4000;

				slide_number: false;

				animation_speed:600;

				pause_on_hover: true;

				resume_on_mouseout: true,

				timer: true;

				circular:true;

				navigation_arrows:true;

				bullets:true;

				timer: true;">

				<li class="reltive" style="background-image:url(/cdn/images/slider_gruppo_lautomobile1.jpg)">
					<img src="/cdn/images/transparent.png" />
					<!-- <iframe class="youtubechannel" src="https://www.youtube.com/embed/6Viyt2aIOG8" frameborder="0" allowfullscreen></iframe> -->
					<div class="title-page centerer text-center color-0">
						<span class="text1 font-bold shadow"><?php echo $frontend["titleHeader"]; ?></span><br>
						<span class="text2 shadow"><?php echo $frontend["subTitle"]; ?></span>
					</div>
				</li>

				<li class="reltive" style="background-image:url(/cdn/images/header-bkg-11.jpg)">
					<img src="/cdn/images/transparent.png" />
					<!-- <iframe class="youtubechannel" src="https://www.youtube.com/embed/6Viyt2aIOG8" frameborder="0" allowfullscreen></iframe> -->
					<div class="title-page centerer text-center color-0">
						<span class="text1 font-bold shadow"><?php echo $frontend["titleHeader2"]; ?></span><br>
						<span class="text2 shadow"><?php echo $frontend["subTitle"]; ?></span>
					</div>
				</li>

				<li class="reltive" style="background-image:url(/cdn/images/<? echo $frontend["header-bkg"]; ?>)">
					<img src="/cdn/images/transparent.png"  />
					<div class="title-page centerer text-center color-0">
						<span class="text1 font-bold shadow"><?php echo $frontend["titleHeader3"]; ?></span><br>
						<span class="text2 shadow"><?php echo $frontend["subTitle"]; ?></span>
					</div>
				</li>

				<li class="reltive" style="background-image:url(/cdn/images/slider_gruppolautomobile3.jpg)">
					<img src="/cdn/images/transparent.png" />
					<div class="title-page centerer text-center color-0">
						<span class="text1 font-bold shadow"><?php echo $frontend["titleHeader2"]; ?></span><br>
						<span class="text2 shadow"><?php echo $frontend["subTitle"]; ?></span>
					</div>
				</li>

				</ul>

			<?php } else {?>

				<img class="header-image fullWidthImg relative <?php if(isset($frontend["isUsato"])){ echo 'usato-page'; }?>" src="/cdn/images/<? echo $frontend["header-bkg"]; ?>">
				<div class="title-page centerer text-center color-0 <?php if(isset($frontend["isUsato"])){ echo 'usato-page'; }?>">
						<span class="text1 font-bold shadow"><?php echo $frontend["titleHeader"]; ?></span><br>
						<span class="text2 shadow"><?php echo $frontend["subTitle"]; ?></span>
					</div>
			<?php }?>

			

			<?php if(!isset($frontend["header-opaque"])) { ?>

			<div class="area-selector text-center padding-top-14 hide-for-small-only" >

				<div class="row">

				<form id="app" action="/frontend/usato/cercatop" method="post">
					<div class="small-12 columns">
						<div class="row collapse">
							<div class="medium-2 columns">
								<select name="marchio">
					                <option value="TUTTI">Marca</option>
					                <?php 
					                $listamarchi = LoadApp('usato',1)->apiMarchiusato();

					                foreach ($listamarchi as $marchio) {
					                  if($_SESSION["marchiosel"]==strtoupper($marchio->descmarca)){
					                    $selectedmarchio = "selected";
					                  }else{
					                    $selectedmarchio = "";
					                  }
					                  echo '<option value="'.strtoupper($marchio->descmarca).'" '.$selectedmarchio.'>'.strtoupper($marchio->descmarca).'</option>';
					                }
					                ?>
					            </select>
							</div>
							<div class="medium-2 columns">
								<?php 
								if(strlen($_SESSION["modellosel"])>0){
									$miomodello = $_SESSION["modellosel"];
								}else{
									$miomodello = "";
								}
								?>
								<input type="text" name="modello" placeholder="Modello" value="<?=$miomodello; ?>">
							</div>
							<div class="medium-2 columns">
								<select name="aprezzo">
									<option value="">Prezzo fino a</option>
									<option value="5000">    5.000 €</option>
									<option value="10000">  10.000 €</option>
									<option value="15000">  15.000 €</option>
									<option value="20000">  20.000 €</option>
									<option value="30000">  30.000 €</option>
									<option value="50000">  50.000 €</option>
								</select>
							</div>
							<div class="medium-2 columns">
								<select name="akmperco">
									<option value="">	   KM fino a</option>
									<option value="5000">    5.000 KM</option>
									<option value="10000">  10.000 KM</option>
									<option value="15000">  15.000 KM</option>
									<option value="20000">  20.000 KM</option>
									<option value="30000">  30.000 KM</option>
									<option value="50000">  50.000 KM</option>
									<option value="100000">100.000 KM</option>
									<option value="200000">200.000 KM</option>
								</select>
							</div>
							<div class="medium-2 columns">
								<select name="daimmatricolaz">
									<option value="">Anno da</option>
									<option value="2014">2014</option>
									<option value="2013">2013</option>
									<option value="2012">2012</option>
									<option value="2010">2011</option>
									<option value="2009">2010</option>
									<option value="2008">2009</option>
									<option value="2007">2008</option>
									<option value="2006">2007</option>
									<option value="2005">2005</option>
									<option value="2004">2004</option>
								</select>
							</div>
							<div class="medium-2 columns small text-left">
								<button class="button submit width100 heightHowInput" type="submit">Ricerca</button>
							</div>
						</div>
					</div>
					</form>
				</div>

			</div>

			<?php } ?>

			<?php if(isset($frontend["header-opaque"])) { ?>

			<div class="header-opaque"></div>

			<?php } ?>

			<?php if(isset($frontend["landingOfferta"])) { ?>

			<div class="area-selector text-center padding-top-14 hidden-for-medium" style="">

				<div class="row button-action-landing" style="max-width:600px;">

					<div class="medium-6 columns text-left">

						<a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>"class="button fullWidth font-size-08 font-bold"><i class="fa fa-calendar"></i> VIENI A TROVARCI</a>

					</div>

					<div class="medium-6 columns text-left">

						<a href="<? echo createUrl('frontend/contatti/GetIndex')[1]; ?>"class="button fullWidth button-gray-2 font-size-08 font-bold"><i class="fa fa-envelope"></i> RICHIEDI INFORMAZIONI</a>

					</div>

				</div>

			</div>

			<?php } ?>

		</div>

		<div class="breadcrumbs-row fullWidth">

			<div class="row">

				<div class="small-12 columns padding-top-05 padding-bottom-05">

					<ul class="breadcrumbs font-bold font-size-10">

						<!--<li><a href="#">Home</a></li>

						<li><a href="#">Features</a></li>

						<li class="unavailable"><a href="#">Gene Splicing</a></li>

						<li class="current"><a href="#">Cloning</a></li>-->

						<?php

						foreach ($frontend["breadcrumbs"] as $breadcrumb) {

							echo '<li class="' . $breadcrumb['current']  . '"><a href="' . $breadcrumb['url'] .'">' . $breadcrumb['name'] . '</a></li>';

						}

						?>

					</ul>

					

				</div>

			</div>

		</div>

		<div id="endheader"> </div>