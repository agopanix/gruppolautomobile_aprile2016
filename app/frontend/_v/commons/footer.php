<?
$frontend = $app_data;
?>
<div class="fullWindow bkg-color-0 ">
	<div class="row padding-bottom-10 padding-top-10">
		<div class="small-12 columns text-center">
			<div class="titolo-sezione tfooter font-bold color-6"><span>&nbsp;&nbsp;Concessionarie Ufficiali&nbsp;&nbsp;</span></div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns padding-bottom-20">
			<ul class="concessionari">
					<li>
					<a href="http://concessionaria.bmw.it/automobile/it_IT/home.html" target="_blank">
						<div class="relative area-logo margin-top-10">
							<img class="centered-text" src="/cdn/images/bmw-logo.png"></div><!-- <span class="marca">BMW</span><br>-->
							<span class="concessionaria show hidden-for-large-up">L'Automobile S.r.l.</span>
						</a>
					</li>
					
					<li>
						<a href="http://www.lauto.mercedes-benz.it/" target="_blank">
							<div class="relative area-logo margin-top-10">
								<img class="centered-text" src="/cdn/images/mercedes-benz-logo.png"></div><!-- <span class="marca">Mercedes Benz</span><br>-->
								<span class="concessionaria hidden-for-large-up">L'Auto. S.r.l.</span>
							</a>
						</li>
						<li>
							<a href="http://www.concessionario.citroen.it/lautomotive" target="_blank">
								<div class="relative area-logo margin-top-10">
									<img class="centered-text" src="/cdn/images/citroen-logo.png"></div><!-- <span class="marca">Citroen</span><br>-->
									<span class="concessionaria hidden-for-large-up">L'Automotive S.r.l.</span>
								</a>
							</li>
							<li>
								<a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>" target="_blank">
									<div class="relative area-logo margin-top-10">
										<img class="centered-text" src="/cdn/images/suzuki-logo.png"></div><!-- <span class="marca">Suzuki</span><br>-->
										<span class="concessionaria hidden-for-large-up">L'Automotive S.r.l.</span>
									</a>
								</li>
								<li>
									<a href="http://www.dealers.it.porsche.com/latina/ita/" target="_blank">
										<div class="relative area-logo margin-top-10">
											<img class="centered-text" src="/cdn/images/porche-logo.png"></div><!--<span class="marca">Porsche</span><br>-->
											<span class="concessionaria">L'Autosport S.r.l.</span>
										</a>
									</li>
								</ul>
					
								<ul class="concessionari padding-top-10">
									<li>
										<a href="http://www.automobile.mini.it/dealer/automobile/index.html" target="_blank">
											<div class="relative area-logo margin-top-10">
												<img class="centered-text" src="/cdn/images/mini-logo.png"></div><!--<span class="marca">Mini</span><br>-->
												<span class="concessionaria">L'Automobile S.r.l.</span>
											</a>
										</li>
										<li>
											<a href="http://www.lauto.mercedes-benz.it/content/italy/retail-0/lauto/it/desktop/passenger-cars/offers-overview/offerte-smart.html" target="_blank">
												<div class="relative area-logo margin-top-10">
													<img class="centered-text" src="/cdn/images/smart-logo.png"></div><!-- <span class="marca">Smart</span><br>-->
													<span class="concessionaria">L'Auto. S.r.l.</span>
												</a>
											</li>
											
											<li>
												<a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>" target="_blank">
													<div class="relative area-logo ald-fix margin-top-10">
														<img class="centered-text" src="/cdn/images/diesse-logo.png"></div><!--<span class="marca">Autorent</span><br>-->
														<span class="concessionaria">L'Automotive S.r.l.</span>
													</a>
												</li>
												
												<li>
													<a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>" target="_blank">
														<div class="relative area-logo ald-fix margin-top-10">
															<img class="centered-text" src="/cdn/images/honda-logo.png"></div><!--<span class="marca">Autorent</span><br>-->
															<span class="concessionaria">L'Automotive S.r.l.</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
										<div class="row padding-bottom-10 padding-top-10">
											<div class="small-12 columns text-center">
												<div class="titolo-sezione tfooter font-bold color-6">Il nuovo mondo della Mobilità. Le nostre proposte nel Noleggio a Lungo Termine</div>
											</div>
										</div>
										<div class="row">
											<div class="small-12 columns padding-bottom-10">
												<ul class="concessionari mobilita ">
													<li>
														<a href="http://autorent-ald.com/" target="_bloank">
														<div class="relative area-logo">
															<img class="centered-text" src="/cdn/images/automobile-ald-automotive.png">
														</div>
														</a>	
														</li>
														<li>
															<a href="http://automobile.nltauto.it/" target="_blank">
																<div class="relative area-logo">
																	<img class="centered-text" src="/cdn/images/automobile-arval.png"></div>
																</a>
															</li>
															
														</ul>
													</div>
												</div>
											</div>
											<div class="fullWidth bkg-color-4 color-0  padding-top-25">
												<div class="footer row">
													<div class="medium-6 large-4 columns">
														<ul class="contacts">
															<li class="font-bold">Gruppo L’ Automobile</li>
															<li>Via Enrico Fermi, 27</li>
															<li>03100 Frosinone (FR)</li>
															<li>Tel. 800.709.990</li>
															<li>E-mail : info@gruppolautomobile.it</li>
															<li>Partita IVA 11356071008</li>
															<li><a href="<? echo createUrl('frontend/privacy/GetIndex')[1]; ?>">Privacy</a></li>
															<li>Credits: <a href="http://www.mandarinoadv.com" target="_blank" >http://www.mandarinoadv.com</a></li>
														</ul>
													</div>
													<div class="medium-6 large-4 columns">
														<ul class="contacts">
															<li><a href="/">Home</a></li>
															<li><a href="/frontend/usato">Scegli la tua Auto</a></li>
															<li><a href="<? echo createUrl('frontend/richiediappuntamento/GetIndex')[1]; ?>">Prendi un appuntamento</a></li>
															<li><a href="<? echo createUrl('frontend/preferiti/GetIndex')[1]; ?>">Lista dei Preferiti</a></li>
															<li><a href="<? echo createUrl('frontend/chisiamo/GetIndex')[1]; ?>">Chi siamo</a></li>
															<li><a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>">Dove siamo</a></li>
															<li><a href="<? echo createUrl('frontend/contatti/GetIndex')[1]; ?>">Contatti</a></li>
															<li class="socials">
																<a href="https://www.facebook.com/pages/Auto365/398395653621974?ref=hl" target="_blank" ><i class="fa fa-facebook-official"></i></a>
																<a href="https://plus.google.com/111367938131787325252" target="_blank"><i class="fa fa-google-plus-square" ></i></a>
																<a href="https://twitter.com/_auto365" target="_blank"><i class="fa fa-twitter-square" ></i></a>
															</li>
														</ul>
													</div>
													<div class="medium-12 large-4 columns">
														<p><b>Contattaci</b></p>
														<?
														/*
														if($_REQUEST['sendFooter'] == 1){
															?>
															<p>Richiesta inviata con successo</p>
															<?
														}
														else if($_REQUEST['sendFooter'] == 2){
															?>
															<p>Errore nell'invio</p>
															<?	
														}
														*/

														#if($_REQUEST['sendFooter'] != 1){
															?>
															<form class="contact-form" method="post" action="/frontend/invio/footer">
																<input type="hidden" name="gyu_return" value="<?= $_SERVER['REQUEST_URI']; ?>" />
																<input type="text" name="gyu_ctrl" value="" style="opacity: 0; height: 0px; width: 0px; margin: 0px; padding: 0px;" />
																<?
																if($_REQUEST['cod']){
																	?>
																	<input type="hidden" name="cod" value="<?= $_REQUEST['cod']; ?>" />
																	<?
																}
																?>
																<input type="text" class="form-control" name="name" placeholder="Nome e Cognome" required />
																<input type="email" class="form-control" name="email" placeholder="Email" required />
																<textarea rows="4" cols="30" class="form-control" name="message" placeholder="Messaggio" required></textarea>
																<div class="margin-top-1">
																	<input id="autorize-action-mail" type="checkbox" class="autorize-action-mail" required><label class="autorize-action-mail-label" for="autorize-action-mail">Acconsento al trattamento dei dati ai sensi e per gli effetti di cui all'articolo 13, D.Lgs 30/06/2003.</label>
																</div>
																<div class="text-right padding-top-10">
																	<button type="submit" class="submit-gray">Invia</button>
																</div>
															</form>
															<?
														#}
														?>
													</div>
												</div>
											</div>
											<!-- footer-->
											<script src="/cdn/js/vendor/jquery.js"></script>
											<script src="/cdn/js/vendor/jquery.fittext.js"></script>
											<script type="text/javascript" src="/cdn/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
											<!-- Add fancyBox main JS and CSS files -->
											<script type="text/javascript" src="/cdn/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
											<link rel="stylesheet" type="text/css" href="/cdn/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
											<!-- Add Button helper (this is optional) -->
											<link rel="stylesheet" type="text/css" href="/cdn/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
											<script type="text/javascript" src="/cdn/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
											<!-- Add Thumbnail helper (this is optional) -->
											<link rel="stylesheet" type="text/css" href="/cdn/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
											<script type="text/javascript" src="/cdn/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
											<!-- Add Media helper (this is optional) -->
											<script type="text/javascript" src="/cdn/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
											<script src="/cdn/js/app.js"></script>
											<script src="/cdn/js/foundation.min.js"></script>
											<script src="/cdn/js/foundation/foundation.clearing.js"></script>
											<script src="/cdn/js/searchbox.js"></script>
											<script>
												$(document).foundation();
											</script>
											<script type="text/javascript">
											$(document).ready(function() {
											if ($('#vat-map-2').length) {
												var concessionarie = <?php echo json_encode($frontend->concessionari); ?>;
												var map = new google.maps.Map(document.getElementById('vat-map-2'), {
												disableDefaultUI: false,
												navigationControl: true,
												mapTypeControl: true,
												scaleControl: true,
												draggable: true,
												scrollwheel: true
											});
											var markers = [];
											var infowindow = new google.maps.InfoWindow({
												content: ""
											});
											var bindInfoWindow = function(marker, map, infowindow, description) {
												google.maps.event.addListener(marker, 'click', function() {
												var txt = '<b>' + description.titolo + '</b><br>' +  description.concessionaria + '<br>' + description.indirizzo + '<br>' + description.cap + ' ' + description.citta;
												if (description.capoluogo != description.citta)
													txt += ' ' + description.capoluogo;
												txt += ' (' + description.sigla + ') <br>';
												if (description.telefono)
													txt += 'Telefono: ' + description.telefono + '<br>';
												//if (description.fax)
												//txt += 'Fax: ' + description.fax + '<br>';
												infowindow.setContent(txt);
												infowindow.open(map, marker);
												});
											}
											for (i = 0; i < concessionarie.length; i++) {
												var latLng = new google.maps.LatLng(concessionarie[i].lat, concessionarie[i].long);
												var marker = new google.maps.Marker({
													position: latLng,
													map: map, 
													title: concessionarie[i].concessionaria
												});
												markers.push(marker);
												bindInfoWindow(marker, map, infowindow, concessionarie[i]);
											}
											
											map.fitBounds(markers.reduce(function(bounds, marker) {
												return bounds.extend(marker.getPosition());
											}, new google.maps.LatLngBounds()));
											google.maps.event.addDomListener(window, 'resize', function() {
												map.setCenter(centerMap);
											});
											}
											});
											</script>
											<script type="text/javascript">
															// Questa sarà la parte che gestirà il submit
															$(function() {
																$(document).on('change', '.searchfield', function() {
																	
																	var fields = $('.research').serialize();
																	console.log(fields);
																	$.post('/api/frontend.usato.filter', fields, function(res) {
																		//console.log(res);
																		// Qui refresho la pagina
																		window.location.reload();
																	});
																	
																});
															});
											</script>
											<script type="text/javascript" 
												id="cookiebanner" 
												src="http://cookiebanner.eu/js/cookiebanner.min.js"
												data-message="Il presente sito web utilizza cookie tecnici per garantire il corretto funzionamento delle procedure e migliorare l'esperienza di uso delle applicazioni online. Il presente documento fornisce informazioni sull'uso dei cookie e di tecnologie similari, su come sono utilizzati dal sito e su come gestirli. Chiudendo questo messaggio si accetta automaticamente l'utilizzo dei cookie."
												data-linkmsg =""
												data-accept-on-click="true"
												></script>
										</body>
									</html>