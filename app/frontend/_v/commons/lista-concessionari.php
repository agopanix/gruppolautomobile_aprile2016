<div class="concessionari useraction">
	<div id="concessionari-btn"  class="label-text relative vieniatrovarci-btn">
		<img src="/cdn/images/label-concessionari.png">
	</div> 
	<div id="concessionariarea" class="closed area font-size-13 animate">
		<div class="row">
			<div class="small-12 columns">
				<span class="font-bold title">Concessionari Ufficiali</span>
				<div class="row">
					<div class="medium-4 columns">
						<a href="http://concessionaria.bmw.it/automobile/it_IT/home.html" target="_blank">						
							<div class="relative area-logo"><img class="centered-text" src="/cdn/images/bmw-logo.png"></div>
							<span class="marca">BMW - L'Automobile S.r.l.</span>
						</a>
					</div>
					<div class="medium-4 columns">
						<a href="http://www.automobile.mini.it/dealer/automobile/index.html" target="_blank">
							<div class="relative area-logo"><img class="centered-text" src="/cdn/images/mini-logo.png"></div>
							<span class="marca">Mini - L'Automobile S.r.l.</span>
						</a>
					</div>
					<div class="medium-4 columns">
						<a href="http://www.lauto.mercedes-benz.it/" target="_blank">
							<div class="relative area-logo"><img class="centered-text" src="/cdn/images/mercedes-benz-logo.png"></div>
							<span class="marca">Mercedes Benz - L'Auto</span>
						</a>
					</div>
					<div class="medium-4 columns">
						<a href="http://www.lauto.mercedes-benz.it/content/italy/retail-0/lauto/it/desktop/passenger-cars/offers-overview/offerte-smart.html" target="_blank">
							<div class="relative area-logo"><img class="centered-text" src="/cdn/images/smart-logo.png"></div>
							<span class="marca">Smart - L'Auto S.r.l.</span>
						</a>
					</div>
					<div class="medium-4 columns">
						<a href="http://www.concessionario.citroen.it/lautomotive" target="_blank">
							<div class="relative area-logo"><img class="centered-text" src="/cdn/images/citroen-logo.png"></div>
							<span class="marca">Citroen - L'Automotive S.r.l.</span>
						</a>
					</div>
					<div class="medium-4 columns">
						<a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>" >
							<div class="relative area-logo"><img class="centered-text" src="/cdn/images/diesse-logo.png"></div>
							<span class="marca">Diesse - L'Automotive S.r.l.</span>
						</a>
					</div>
					<div class="medium-4 columns">
						<a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>" >
							<div class="relative area-logo"><img class="centered-text" src="/cdn/images/suzuki-logo.png"></div>
							<span class="marca">Suzuki - L'Automotive S.r.l.</span>
						</a>
					</div>
					<div class="medium-4 columns">
						<a href="<? echo createUrl('frontend/dovesiamo/GetIndex')[1]; ?>" >
							<div class="relative area-logo"><img class="centered-text" src="/cdn/images/honda-logo.png"></div>
							<span class="marca">Honda - L'Automotive S.r.l.</span>
						</a>
					</div>
					<div class="medium-4 columns">
						<a href="http://www.dealers.it.porsche.com/latina/ita/" target="_blank">
							<div class="relative area-logo"><img class="centered-text" src="/cdn/images/porche-logo.png"></div>
							<span class="marca">Porsche - Centro Porsche Latina</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>