<div class="concessionari useraction ">
	<div id="concessionari-btn"  class="label-text relative vieniatrovarci-btn">
		<img src="/cdn/images/label-concessionari.png">
	</div> 
	<div id="conessionariarea" class="closed area font-size-13 animate">
		<div class="row">
			<div class="small-12 columns">
				<i class="fa fa-calendar"></i> <span class="font-bold">Concessionari</span>
				<div class="map" id="vat-map"></div>
				<div class="font-size-11"> <i class="color-9 fa fa-map-marker"></i> Via Enrico Fermi, 27 - 03100 - Frosinone</div>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<hr>
			</div>
			<div class="small-7 columns">
				<div class="font-size-08 color-10">Hai bisogno di un appuntamento specifico?</div>
				<div class="font-size-1 color-10 font-bold">Tel. 800.709.990</div>
			</div>
			<div class="small-5 columns text-right">
				<a class="button font-size-07" href="<? echo createUrl('frontend/richiediappuntamento/GetIndex')[1]; ?>">Prenota un appuntamento</a>
			</div>
		</div>
	</div>
</div>