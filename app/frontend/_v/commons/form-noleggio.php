<form action="/frontend/invio/noleggio" method="POST">
	<input type="hidden" name="gyu_type" value="<?= $app_data['type']; ?>" />
	<input type="hidden" name="gyu_return" value="<?= $_SERVER['REQUEST_URI']; ?>" />
	<input type="text" name="gyu_ctrl" value="" style="opacity: 0; height: 0px; width: 0px; margin: 0px; padding: 0px;" />

<div class="row">
	<div class="medium-12 columns">
		<div class="row">
			<div class="medium-12 columns">
				<input type="text" name="nome"  class="big" placeholder="NOME E COGNOME*"  tabindex="1" required/>
			</div>
			<div class="medium-6 columns">
				<input type="number" name="telefono"  class="big" placeholder="CELLULARE O TELEFONO*"  tabindex="2" required/>
			</div>
			<div class="medium-6 columns">
				<input type="email" name="email" class="big" placeholder="EMAIL*"  tabindex="2" required/>
			</div>
			<div class="medium-6 columns">
				<input type="text"  name="marca" class="big" placeholder="MARCA DI INTERESSE*"  tabindex="3" required/>
			</div>
			<div class="medium-6 columns">
				<input type="text" name="modello"  class="big" placeholder="MODELLO DI INTERESSE*"  tabindex="4" required/>
			</div>
			<div class="medium-6 columns">
				<input type="text" name="limitekm"  class="big" placeholder="MAX KM/ANNO (Limite Km in un anno)*"  tabindex="5" required/>
			</div>
			<div class="medium-6 columns">
				<input type="number" name="duratanoleggio"  class="big" placeholder="DURATA DEL NOLEGGIO IN ANNI*"  tabindex="6" required/>
			</div>
			<div class="medium-6 columns">
				<input type="number" name="partitaiva"  class="big" placeholder="PARTITA IVA*"   onKeyDown="if(this.value.length==11) this.value = this.value.slice(0, - 1);" maxlength="11" tabindex="7" required />
			</div>
			<div class="medium-6 columns">
				<input type="text" name="ragionesociale"  class="big" placeholder="RAGIONE SOCIALE*"  tabindex="8" required />
			</div>
			<div class="medium-12 columns">
				<textarea rows="4" cols="30" required name="messaggio" tabindex="5" placeholder="MESSAGGIO*" tabindex="9" ></textarea>
			</div>
		</div>

		<div class="row">
			<div class="small-12 columns padding-bottom-10">
				Tutti i campi contrassegnati con * sono obbligatori.
			</div>
		</div>

		<div class="row">
			<div class="small-12 padding-top-10 columns">
				Preferenza sulla modalità di contatto.  <input type="checkbox" name="ricontattotel" value="Si" > Cellulare - <input type="checkbox" name="ricontattoemail" value="Si" > Email
			</div>
		</div>
	
		<div class="row">
			<div class="medium-12 columns margin-top-1">
				<input id="authorize-action" name="privacy" value="Si" style="margin-top:5px;" tabindex="6" type="checkbox" required><label class="authorize-action-label" for="authorize-action">Acconsento al trattamento dei dati ai sensi e per gli effetti di cui all'articolo 13, D.Lgs 30/06/2003.</label>
			</div>
		</div>
		<div class="row padding-top-20 padding-bottom-20">
			<div class="medium-12 columns text-right ">
				<button type="submit" tabindex="7" >Invia</button>
			</div>
		</div>
	</div>
</div>
</form>