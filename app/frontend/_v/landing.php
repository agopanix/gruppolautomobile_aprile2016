<div class="fullWindow bkg-color-0">
	<div class="row">
		<div class="small-12 columns padding-top-10 font-bold font-size-22 text-center">Cerchi un’auto per la famiglia?</div>
		<div class="small-12 columns font-bold font-size-14 text-center color-7">Scopri l’offerta a te dedicata. <span class="color-14">20% di Sconto</span></div>
		<div class="small-6 columns text-right color-13 font-size-22"><del>€ 27.200</del></div>
		<div class="small-6 columns text-left color-14 font-size-22 font-bold">€ 23.000</div>
		<hr>
	</div>
	<div class="row">
		<div class="medium-2 columns large-text-left small-text-center">
			<img class="hidden-for-small" style="" src="/cdn/images/logo-bmw-landing.png">
		</div>
		<div class="medium-10 columns padding-top-10">Lorem ipsum dolor sit amet, falli tollit cetero te eos. Ea ullum liber aperiri mi, impetus ate philosophia
			ad duo, quem regione ne ius. Vis quis lobortis dissentias ex, in du aft philosophia, malis
			necessitatibus no mei. Volumus sensibus qui ex, eum duis doming ad. Modo liberavisse eu
			mel, no viris prompta sit. Pro labore sadipscing et. Ne peax egat usu te mel vivendo scriptorem.
			Pro labore sadipscing et. Ne pertinax egat usu te mel vivendo scriptorem.
		</div>
	</div>
	<div class="row margin-top-20">
		<div class="photo-scroller large-6 columns relative button-action-landing">
			<ul class="orbit" data-orbit  data-options="animation:slide; animation_speed:500;  timer:false; navigation_arrows:true; bullets:true; slide_number: false; resume_on_mouseout: false;">
				<li>
					<img
					data-interchange="
					[/timthumb.php?src=/cdn/images/photo-image.jpg&w=600&h=300&zc=1, (small)],
					[/timthumb.php?src=/cdn/images/photo-image.jpg&w=1200&h=620&zc=1, (medium)],
					[/timthumb.php?src=/cdn/images/photo-image.jpg&w=590&h=500&zc=1, (large)]"
					src="" >
				</li>
				<li>
					<img
					data-interchange="
					[/timthumb.php?src=/cdn/images/photo-image.jpg&w=600&h=300&zc=1, (small)],
					[/timthumb.php?src=/cdn/images/photo-image.jpg&w=1200&h=620&zc=1, (medium)],
					[/timthumb.php?src=/cdn/images/photo-image.jpg&w=590&h=500&zc=1, (large)]"
					src="" >
				</li>
				<li>
					<img
					data-interchange="
					[/timthumb.php?src=/cdn/images/photo-image.jpg&w=600&h=300&zc=1, (small)],
					[/timthumb.php?src=/cdn/images/photo-image.jpg&w=1200&h=620&zc=1, (medium)],
					[/timthumb.php?src=/cdn/images/photo-image.jpg&w=590&h=500&zc=1, (large)]"
					src="" >
				</li>
			</ul>
			<a href="#"class="button button-gray-2 font-size-1 font-bold aggiungi-preferiti-scroller"><i class="fa fa-car"></i> Aggiungi ai preferiti </a>
		</div>
		<div class="large-6 columns">
			<div class="row">
				<div class="small-12 columns">
					<span class="font-bold font-size-15">BMW Serie 3 Touring</span>
				</div>
			</div>
			<div class="row">
				<div class="small-6 columns text-center">
					<div class="row">
						<div class="small-12 columns">
							<span class="color-13 font-size-15"><del>€ 27.200</del></span><span class="color-14 font-size-15 font-bold">&nbsp;&nbsp;<del>€ 23.200</del></span>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-left">
							<hr>
							<span class="font-bold">1a Immatricolazione</span><br>
							30/05/2011
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-left">
							<hr>
							<span class="font-bold">Km percorsi</span><br>
							106.205
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-left">
							<hr>
							<span class="font-bold">Carburante</span><br>
							Diesel
						</div>
					</div>
				</div>
				<div class="small-6 columns text-left">
					<div class="row">
						<div class="small-12 columns font-bold">
							<span class="color-7 font-size-15">Sconto</span><span class="color-14 font-size-15">&nbsp;-20%</span>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-left">
							<hr>
							<span class="font-bold">Colore</span><br>
							Grigio
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-left">
							<hr>
							<span class="font-bold">Cilindrata</span><br>
							1.995 cc
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-left">
							<hr>
							<span class="font-bold">Potenza cv / kw </span><br>
							184 cv / 135 kw
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns padding-top-10">
					<i class="color-15 fa fa-map-marker font-size-15"></i> <span class="font-bold">Disponibile in sede: </span> Via Enrico Fermi, 27 - 03100 - Frosinone
				</div>
			</div>
		</div>
	</div>
	<div class="row button-action-landing">
		<div class="medium-3 columns padding-top-10">
			<a href="<? echo createUrl('dovesiamo/GetIndex')[1]; ?>"class="button fullWidth font-size-08 font-bold"><i class="fa fa-calendar"></i> VIENI A TROVARCI</a>
		</div>
		<div class="medium-3 columns padding-top-10">
			<a href="<? echo createUrl('contatti/GetIndex')[1]; ?>"class="button fullWidth button-gray-2 font-size-08 font-bold"><i class="fa fa-envelope"></i> RICHIEDI INFORMAZIONI</a>
		</div>
		<div class="medium-3 columns padding-top-10">
			<a href="#"class="button fullWidth button-gray-2 font-size-08 font-bold"><i class="fa fa-file-text-o"></i>SCARICA SCHEDA</a>
		</div>
		<div class="small-4 medium-1 columns padding-top-10">
			<a class="social-icon button button-gray font-bold" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><i class="fa fa-facebook"></i></a>
		</div>
		<div class="small-4 medium-1 columns padding-top-10">
			<a class="social-icon button button-gray font-bold" target="_blank" href="https://plus.google.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><i class="fa fa-google-plus"></i></a>
		</div>
		<div class="small-4 medium-1 columns padding-top-10">
			<a class="social-icon button button-gray font-bold" target="_blank" 
			href="https://twitter.com/home?status= <?php echo $auto['descrizione']; ?> <?php echo 'http://' . $_SERVER['HTTP_HOST'] . createUrl('dettaglio/' . $auto['id'] )[1];  ?>"><i class="fa fa-twitter"></i></a>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns padding-top-10">
			<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">DESCRIZIONE DEL VEICOLO // ULTERIORI EQUIPAGGIAMENTI </div>
		</div>
		<div class="small-12 columns padding-bottom-10">
			ABS; Airbag anteriori per guidatore e passeggero; Airbag laterali; Airbag per la testa; Appoggiatesta posteriori;
			Autoradio; Bluetooth; Cambio manuale; Cerchi in lega; Chiave con transponder; Chiusura centralizzata; Climatizzatore;
			Controllo automatico velocità; Controllo elettronico della stabilità; Controllo elettronico della trazione;
			Correttore assetto fari; Fendinebbia; Filtro anti particolato; Immobilizzatore; Indicatore temperatura esterna; Lavatergifari;
			Navigatore satellitare; Pretensionatore cinture; Retrovisori elettrici; Sedile guida regolabile in altezza;
			Sedili regolabili elettricamente; Servosterzo; Vetri elettrici anteriori; Vetri elettrici posteriori; Volante regolabile;
		</div>
	</div>
	<div class="row border-bottom-gray color-7" data-equalizer data-equalizer-mq="medium-up">

		<div class="large-3 columns padding-top-10" data-equalizer-watch>
			<span class="font-bold centered-text padding-top-10 fullWidth padding-left-10">Equipaggiamento del veicolo</span>
		</div>
		<div class="large-3 columns padding-bottom-10 padding-top-10" data-equalizer-watch>
			ABS,<br>
			Cambio automatico,<br>
			Cerchi in lega,<br>
			Climatizzatore<br>
			Servosterzo<br>
			Airbag guida e passeggero,<br>
			laterali e per la testa<br>
			Controllo elettronico<br>
			della trazione<br>
			Servosterzo<br>
		</div>
		<div class="large-3 columns padding-top-10 padding-bottom-10" data-equalizer-watch>
			<span class="font-bold centered-text fullWidth  padding-left-10">Ulteriori specifiche del veicolo</span>
		</div>
		<div class="large-3 columns padding-top-10 padding-bottom-10" data-equalizer-watch>
			<div class="centered-text ulteriori-specifiche">
				<span class="font-bold">Tipo di cambio:</span> meccanico<br>
				<span class="font-bold">Prossima revisione:</span> 03/03/2016<br>
				<span class="font-bold">Ultima revisione:</span> 03/03/2014<br>
			</div>
		</div>
	</div>
	<div class="row color-7 padding-bottom-20">
		<div class="small-6 columns padding-top-10">
			<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10 padding-left-10">INFO RIVENDITORE</div>
			<div class="row">
				<div class="small-12 columns">
					<div class="map-dettaglio margin-bottom-10" id="vat-map-2"></div>
					<span class="font-bold">GRUPPO L’AUTOMOBILE</span><br>
					Via Enrico Fermi, 27 - 03100 Frosinone<br>
					Tel.: +39 - 0775 - 88 50 222<br>
					Mob.: +39 - 345 - 39 70 670<br>
					Fax: +39 - 0775 - 88 50 235<br>
				</div>
			</div>
		</div>
		<div class="small-6 columns padding-top-10">
			<div class="bkg-color-6 padding-top-05 padding-bottom-05 color-0 text-center margin-bottom-10">CONTATTA IL RIVENDITORE</div>
			<div class="row">
				<div class="small-12 columns">
					<form action="#">
						<div class="row ">
							<div class="large-6 columns">
								<input type="text"  class="big" placeholder="NOME"  tabindex="1" required/>
								<input type="email" class="big" placeholder="EMAIL"  tabindex="3" required/>
							</div>
							<div class="large-6 columns">
								<input type="text"  class="big" placeholder="COGNOME"  tabindex="2" required/>
								<input type="number" class="big" placeholder="TELEFONO"  tabindex="4" required/>
							</div>
						</div>
						<div class="row">
							<div class="small-12 columns">
								<textarea rows="4" cols="30" class="form-control" name="message" tabindex="5" placeholder="MESSAGGIO" required=""></textarea>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns margin-top-1">
								<input id="authorize-action" tabindex="6" type="checkbox"><label class="authorize-action-label" for="authorize-action">Autorizzo il trattamento dei dati personali Dlgs 196/2003</label>
							</div>
							<div class="large-12 columns text-left padding-top-20">
								<button type="submit" tabindex="7" >RICHIEDI INFORMAZIONI</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>