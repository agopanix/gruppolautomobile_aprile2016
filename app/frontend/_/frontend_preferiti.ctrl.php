<?php



class frontend_preferitiCtrl extends standardController {



	function __construct() {

		$this->frontend = LoadClass('frontend', 1);

	}



	function GetIndex() {
		//var_dump($this->frontend->marche);
		$config = [

			'title' => 'Lista Auto preferite - Gruppolautomobile.it',

			'description' 	=> "Salva la tua lista di auto usate preferite e comparale per verificare l'offerta migliore. Vieni a trovarci a Frosinone in via Enrico Fermi, 27, Numero Verde 800 709 990",

			'titleHeader' 	=> 'LISTA PREFERITI',

			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',

			'voceMenu' 		=> 'preferiti',

			'little-header' => true,

			'header-bkg'	=> 'header-bkg-3.jpg',

			'breadcrumbs'	=> Array(

				['name'=>'Lista dei preferiti','current'=>'current'],

			)

		];



		

		$this->frontend->_config($config);

		$this->frontend->header();

		$this->view('frontend/_v/preferiti');

		$this->view('frontend/_v/commons/vieni-a-trovarci');

		$this->view('frontend/_v/commons/lista-preferiti');

		$this->view('frontend/_v/commons/lista-concessionari');

		

		$this->frontend->footer();



	}



	

}



