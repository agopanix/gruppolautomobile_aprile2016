<?php

class frontend_dettaglioCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);
		/**
		$this->auto = LoadClass('usato', 1)->getUsato($_REQUEST["cod"]);
		if(!is_object($this->auto)) {
			header('Location: /');
		}

		echo '<pre>' . print_r($this->auto, 1) . '</pre>';
		**/

		if(strlen($_REQUEST['cod'])==0){
			header('Location: /frontend/404');
		}

		$this->miousato = LoadApp('usato',1)->apiGetfromcod($_REQUEST['cod']);
		$this->altre = LoadApp('usato',1)->apiAltrointeresse($_REQUEST['cod']);
		$this->listamarchi = LoadApp('usato',1)->apiMarchiusato();
		$this->listaalim = LoadApp('usato',1)->apiAlimentazioneusato();
		$this->listacolori = LoadApp('usato',1)->apiColoreusato();

		foreach ($this->frontend->categorie as $categoria) {
			if($categoria["id"]==$_REQUEST["cat"]){
				$categoria_selezionata = $categoria["name"];
				$testo_categoria = $categoria["text"];
			}
		}


		if($this->miousato){
			$title = "Offerte Auto ".$this->miousato[0]->descmarca." ".$this->miousato[0]->descallestimento." Usata";

			$description = "Offerte vendita auto ".$this->miousato[0]->descmarca." ".$this->miousato[0]->descallestimento." usata su Gruppolautomobile.it il portale delle auto usate.";
		}
		else{
			$title = "Vendita Auto usate - Gruppolautomobile.it";

			$description = "Offerte vendita Auto usate sul sito Gruppolautomobile.it";
		}
		
		$config = [
			'title' => $title,
			'description' 	=> $description,
			'titleHeader' 	=> 'USATO GARANTITO',
			'subTitle'		=> 'Scegli la tua auto',
			'voceMenu' 		=> '',
			'little-header' => true,
			'header-opaque' => true,
			'header-bkg'	=> 'header-bkg-1.jpg',
			'breadcrumbs'	=> Array(
				['name'=>'Home','current'=>'', 'url'=>'/'],
				['name'=> 'Usato','current'=>'','url'=>'/categoria/' . $_REQUEST["cat"]],
				['name'=> strtoupper($this->miousato[0]->descmarca) .' '.strtoupper($this->miousato[0]->descallestimento) ,'current'=>'current']
			),
			"miousato"=>$this->miousato
		];

		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/dettaglio');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->view('frontend/_v/commons/lista-concessionari');
		$this->frontend->footer();

	}

	
}

