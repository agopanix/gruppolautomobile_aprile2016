<?php

class frontend_chisiamoCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Concessonari Ufficiali Auto Usate - Gruppolautomobile.it',
			'description' 	=> 'Le nostre 9 Concessionarie ufficiali, vere e proprie ambasciate dei brand BMW, MINI, Mercedes-Benz, smart, Porsche, Citroen, DS, Suzuki e Honda sapranno soddisfare la tua scelta.',
			'titleHeader' 	=> 'CHI SIAMO',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> 'chisiamo',
			'header-bkg'	=> 'header-bkg-1.jpg',
			'breadcrumbs'	=> Array(
				['name'=>'Chi Siamo','current'=>'current'],
			)
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/chisiamo');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->view('frontend/_v/commons/lista-concessionari');
		$this->frontend->footer();

	}

	
}

