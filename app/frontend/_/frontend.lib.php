<?php
class frontend {

	var $config = [
		'title' => siteName,
		'description' => '',
		'logo' => '//placehold.it/150x150',
		'showHero' => true
	];

	var $catsel=0;



	var $marche = array(
		['id'=>'1','marca'=>'marca 1'],
		['id'=>'2','marca'=>'marca 2']
	);

	var $categorie = array(
		['id'=>'1','name'=>'Neopatentati','img'=>'/upl/neopatentati.png', 'text' =>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'2','name'=>'Iva Esposta','img'=>'/upl/ivaesposta.png','text'=>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'3','name'=>'Aziendali & Km0','img'=>'/upl/aziendalikmzero.png','text'=>'Vendita auto km0 dai concessionari di Gruppo L&rsquo;Automobile per assicurarti l&rsquo;auto che hai sempre desiderato al prezzo inaspettato'],
		['id'=>'4','name'=>'Cambio Automatico','img'=>'/upl/cambio_automatico.jpg','text'=>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'5','name'=>'Sportive','img'=>'/upl/sportive.jpg','text'=>'L&rsquo;incanto sar&agrave; godersi la strada'],
		['id'=>'6','name'=>'Suv & Fuori Strada','img'=>'/upl/furiostrada.png','text'=>'L&rsquo;incanto sar&agrave; godersi la strada']
	);

	var $autoinofferta = array(
		['id'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
	);

	var $autopreferite = array(
		['id'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'3','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'4','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'5','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'6','marca'=>'BMW','modello'=>'316d', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
	);

	/*var $serviziaggiuntivi = array(
		['id'=>'1','titolo'=>'Noleggio a breve e lungo termine','descrizione'=>'Noleggia le auto che preferisci, scegliendo tra una vasta gamma di opportunità','img'=>'/upl/servizio-1.png','link'=>'/frontend/noleggio/'],
		['id'=>'2','titolo'=>'Carrozzeria','descrizione'=>'Per garantirti un&rsquo;esperienza di mobilità assolutamente sicura, al riparo da qualsiasi rischio ... ','img'=>'/upl/servizio-1.png','link'=>'/frontend/carrozzeria/'],
		['id'=>'3','titolo'=>'Convenzioni','descrizione'=>'Area Riservata','img'=>'/upl/servizio-1.png','link'=>'/frontend/convenzioni/'],
		//['id'=>'2','titolo'=>'Vieni a trovarci','descrizione'=>'Un nostro collaboratore sarà al tuo servizio per l’intera giornata.','img'=>'/upl/servizio-2.png', 'link'=>'contatti/'],
	);*/

	var $accessori = array(
		['id'=>'1','titolo'=>'ABS'],
		['id'=>'2','titolo'=>'Airbag passeggero'],
		['id'=>'3','titolo'=>'Cambio Automatico'],
		['id'=>'4','titolo'=>'Climatizzatore'],
		['id'=>'5','titolo'=>'Interni in materiale pregiato'],
		['id'=>'6','titolo'=>'Servosterzo'],
		['id'=>'7','titolo'=>'Airbag lato guida'],
		['id'=>'8','titolo'=>'Airbag laterali'],
		['id'=>'9','titolo'=>'Cerchi in lega'],
		['id'=>'10','titolo'=>'Controllo elettr. trazione'],
		['id'=>'11','titolo'=>'Navigatore satellitare'],
		['id'=>'12','titolo'=>'Sospensioni autoregolabili'],
	);

	var $risultato_ricerca = array(
		['id'=>'1','idcategoria'=>'1','marca'=>'Audi','modello'=>'A4', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4],'immatricolazione'=>'24/05/2007','descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','idcategoria'=>'1','marca'=>'BMW','modello'=>'316d', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9],'immatricolazione'=>'24/05/2007', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'3','idcategoria'=>'1','marca'=>'Audi','modello'=>'A4', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4],'immatricolazione'=>'24/05/2007','descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'4','idcategoria'=>'1','marca'=>'BMW','modello'=>'316d', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9],'immatricolazione'=>'24/05/2007', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'5','idcategoria'=>'1','marca'=>'Audi','modello'=>'A4', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4],'immatricolazione'=>'24/05/2007','descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'6','idcategoria'=>'1','marca'=>'BMW','modello'=>'316d', 'alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9],'immatricolazione'=>'24/05/2007', 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
	);

	var $dettaglio_veicolo = array(
		['id'=>'1','idcategoria'=>'1','codice'=>'801117','marca'=>'BMW','modello'=>'318d 2.0 143CV cat Touring Attiva', 'normativa'=>'Euro5 (715/2007-692/2008)','colore'=>'Blue','alimentazione'=>'diesel','importo'=>'16900' ,'categoria'=>'Km zero','accessori'=>[1,2,3,4,5,6,7,8,9,10,11,12],'luogo'=>'Via Enrico Fermi, 27 - 03100 Frosinone','immatricolazione'=>'24/05/2007','km'=>'106.205','descrizione'=>'', 'motore' =>'2.0 TDI 143CV F.AP. Advanced' ,'cilindrata'=>'1995','kw'=>'135','cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
	);

	var $auto_interesse = array(
		['id'=>'1','idcategoria'=>'1','marca'=>'Audi','modello'=>'A4', 'importo'=>'16900' ,'descrizione'=>'AUDI A4 Avant 2.0 TDI 143CV F.AP. Advanced', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/audi-a4.png'],
		['id'=>'2','idcategoria'=>'1','marca'=>'BMW','modello'=>'316d','importo'=>'22900' , 'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		['id'=>'2','idcategoria'=>'1','marca'=>'BMW','modello'=>'316d', 'importo'=>'17800' ,'descrizione'=>'BMW 316d 2.0', 'motore' =>'2.0 TDI' ,'cavalli'=>'143','' ,img=>'/upl/bmw-316d.png'],
		);

	var $concessionari = array(
		[
			'id'=>'1',
			'titolo' =>'BMW',
			'concessionaria'=>"L'Automobile S.r.l.",
			'indirizzo'=>'Via Enrico Fermi, 27', 
			'cap'=>'03100',
			'citta'=>'Frosinone',
			'capoluogo'=>'Frosinone',
			'sigla'=>'FR',
			'email'=>'',
			'telefono'=>'0775.88.50.200',
			'fax'=>'',
			'lat'=>'41.6213226',
			'long'=>'13.3111829',
			'img' => '/cdn/images/foto-sedi/automobile-bmw-frosinone.jpg',
			'orari'=>'dal Lunedì al Sabato 08.40-13.00/15.00-19.00'
		],

		[
			'id'=>'2',
			'titolo' =>'MINI',
			'concessionaria'=>"L'Automobile S.r.l.",
			'indirizzo'=>'Via Enrico Fermi, 27', 
			'cap'=>'03100',
			'citta'=>'Frosinone',
			'capoluogo'=>'Frosinone',
			'sigla'=>'FR',
			'email'=>'',
			'telefono'=>'0775.88.50,200',
			'fax'=>'',
			'lat'=>'41.621347',
			'long'=>'13.311044',
			'img' => '/cdn/images/foto-sedi/automobile-mini-fosinone.jpg',
			'orari'=>'dal Lunedì al Sabato 08.40-13.00/15.00-19.00'
		],

		[
			'id'=>'3',
			'titolo' => 'BMW - MINI',
			'concessionaria'=>"L'Automobile S.r.l.",
			'indirizzo'=>'S.R.630 Ausonia Formia-Cassino', 
			'cap'=>'04023',
			'citta'=>'Formia',
			'capoluogo'=>'Latina',
			'sigla'=>'LT',
			'email'=>'',
			'telefono'=>'0771.21.434',
			'fax'=>'',
			'lat'=>'41.3136847',
			'long'=>'13.6897846',
			'img' => '/cdn/images/foto-sedi/automobile-formia-bmw-2.jpg',
			'orari'=>'dal Lunedì al Venerdì 8.30- 13.00 / 15.00-18.30, Sabato: 8.30-13.00 15.30-19.00'
		],

		[
			'id'=>'4',
			'titolo' => 'Mercedes Benz',
			'concessionaria'=>"L'Auto. S.r.l.",
			'indirizzo'=>'Via Enrico Fermi, 25', 
			'cap'=>'04023',
			'citta'=>'Frosinone',
			'capoluogo'=>'Frosinone',
			'sigla'=>'FR',
			'email'=>'',
			'telefono'=>'0775.19.90.500',
			'fax'=>'',
			'lat'=>'41.621347',
			'long'=>'13.311044',
			'img' => '/cdn/images/foto-sedi/auto-mercedes-frosinone-1.jpg',
			'orari'=>'dal Lunedì al S1abato 08.40-13.00/15.00-19.00'
		],

		[
			'id'=>'5',
			'titolo' => 'Smart',
			'concessionaria'=>"L'Auto. S.r.l.",
			'indirizzo'=>'Via Enrico Fermi, 25', 
			'cap'=>'04023',
			'citta'=>'Frosinone',
			'capoluogo'=>'Frosinone',
			'sigla'=>'FR',
			'email'=>'',
			'telefono'=>'0775.19.90.500',
			'fax'=>null,
			'lat'=>'41.621604',
			'long'=>'13.310548',
			'img' => '/cdn/images/foto-sedi/auto-centro-smart-frosinone-2.jpg',
			'orari'=>'dal Lunedì al Sabato 08.40-13.00/15.00-19.00'
		],

		[
			'id'=>'6',
			'titolo' => 'Mercedes Benz - Smart',
			'concessionaria'=>"L'Auto. S.r.l.",
			'indirizzo'=>'Via Epitaffio 74/76', 
			'cap'=>'0100',
			'citta'=>'Latina',
			'capoluogo'=>"Latina",
			'sigla'=>'LT',
			'email'=>'',
			'telefono'=>'0773.19.90.500',
			'fax'=>null,
			'lat'=>'41.5256478',
			'long'=>'12.9408816',
			'img' => '/cdn/images/foto-sedi/auto-mercedes-smart-latina.jpg',
			'orari'=>'dal Lunedì al Sabato 08.40-13.00/15.00-19.00'
		],

		[
			'id'=>'7',
			'titolo' => 'Mercedes, Smart',
			'concessionaria'=>"L'Auto. S.r.l.",
			'indirizzo'=>'Via Sandro Pertini, 167', 
			'cap'=>' 67051',
			'citta'=>'Avezzano',
			'capoluogo'=>'Aquila',
			'sigla'=>'AQ',
			'email'=>'',
			'telefono'=>'0863.01.34.17',
			'fax'=>'',
			'lat'=>'42.014493',
			'long'=>'13.428692',
			'img' => '/cdn/images/foto-sedi/auto-mercedes-avezzano.jpg',
			'orari'=>'dal Lunedì al Sabato 08.40-13.00/15.00-19.00'
		],

		[
			'id'=>'8',
			'titolo' => 'Porsche Latina',
			'concessionaria'=>"L'Autosport S.r.l.",
			'indirizzo'=>'Via Epitaffio, 144', 
			'cap'=>'04100',
			'citta'=>'Latina',
			'capoluogo'=>'Latina',
			'sigla'=>'LT',
			'email'=>'',
			'telefono'=>'0773.19.03.109',
			'fax'=>'',
			'lat'=>'41.529332',
			'long'=>'12.945100',
			'img' => '/cdn/images/foto-sedi/autosport-porsche-latina.jpg',
			'orari'=>'dal Lunedì al Venerdì  9.00-13.00/15.00-19.00, Sabato: 9.00-13.00'
		],

		[
			'id'=>'9',
			'titolo' => 'Citroen',
			'concessionaria'=>"L'Automotive S.r.l.",
			'indirizzo'=>'Via Monti Lepini, Km.5,700 snc', 
			'cap'=>'	03023',
			'citta'=>'Ceccano',
			'capoluogo'=>'Frosinone',
			'sigla'=>'FR',
			'email'=>'',
			'telefono'=>'0775.64.06.84',
			'fax'=>'',
			'lat'=>'41.529332',
			'long'=>'12.945100',
			'img' => '/cdn/images/foto-sedi/automotive-citroen-ceccano.jpg',
			'orari'=>' dal Lunedì al Sabato 08.40-13.00/15.00-19.00'
		],

		[
			'id'=>'10',
			'titolo' => 'Citroen - Honda',
			'concessionaria'=>"L'Automotive S.r.l.",
			'indirizzo'=>'Via Carpinetana Nord', 
			'cap'=>'00034',
			'citta'=>'Colleferro',
			'capoluogo'=>'Roma',
			'sigla'=>'RM',
			'email'=>'',
			'telefono'=>'06.97.02.343',
			'fax'=>'',
			'lat'=>'41.722235',
			'long'=>'13.010587',
			'img' => '/cdn/images/foto-sedi/automotive-honda.jpg',
			'orari'=>'dal Lunedì al Sabato 09.00-13.00/16.00-19.30'
		],

		[
			'id'=>'11',
			'titolo' => 'Suzuki',
			'concessionaria'=>"L'Automotive S.r.l.",
			'indirizzo'=>'Via Monti Lepini, Km.5,700 snc', 
			'cap'=>'',
			'citta'=>'Ceccano',
			'capoluogo'=>'Frosinone',
			'sigla'=>'FR',
			'email'=>'',
			'telefono'=>'0775.64.06.84',
			'fax'=>'',
			'lat'=>'41.630077',
			'long'=>'13.325246',
			'img' => '/cdn/images/foto-sedi/automotive-suzuki-frosinone.jpg',
			'orari'=>'dal Lunedì al Sabato 08.40-13.00/15.00-19.00'
		],

		[
			'id'=>'12',
			'titolo' => 'ALD',
			'concessionaria'=>"L'Automobile S.r.l.",
			'indirizzo'=>'Via Enrico Fermi, 22', 
			'cap'=>'03100',
			'citta'=>'Froninone',
			'capoluogo'=>'Frosinone',
			'sigla'=>'FR',
			'email'=>'',
			'telefono'=>'0775.20.20.87',
			'fax'=>'',
			'lat'=>'41.62135',
			'long'=>'13.311359',
			'img' => '/cdn/images/foto-sedi/automobile-ald-2.jpg',
			'orari'=>'dal Lunedì al Sabato 08.40-13.00/15.00-19.00, Sabato: 8.40-13.00'
		],

		[
			'id'=>'14',
			'titolo' => 'ARVAL',
			'concessionaria'=>"L'Automobile S.r.l.",
			'indirizzo'=>'Via Epitaffio, 74', 
			'cap'=>'04100',
			'citta'=>'Latina',
			'capoluogo'=>'Latina',
			'sigla'=>'LT',
			'email'=>'',
			'telefono'=>'800.187.013',
			'fax'=>'',
			'lat'=>'41.5268713',
			'long'=>'12.9418649',
			'img' => '/cdn/images/foto-sedi/automobil-arval-latina.jpg',
			'orari'=>'dal Lunedì al Venerdì 08.40-13.00/15.00-19.00, Sabato 8.40-13.00']

	);

	
	function _config($config) {
		$config = array_merge($this->config, $config);
		$this->config = $config;
	}

		
	function header() {
		Application('frontend/_v/commons/header', null, $this);
	}
	
	function footer() {
		Application('frontend/_v/commons/footer', null, $this);
	}
	
	function menu() {
		Application('frontend/_v/commons/menu', null, $this);
	}

	function vieniatrovarci() {
		Application('frontend/_v/commons/lista-preferiti', null, $this);
	}

	function listapreferiti() {
		Application('frontend/_v/commons/vieni-a-trovarci', null, $this);
	}


 }