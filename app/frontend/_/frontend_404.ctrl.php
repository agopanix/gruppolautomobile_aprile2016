<?php

class frontend_404Ctrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> 'PAGINA NON TROVATA',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> 'chisiamo',
			'header-bkg'	=> 'header-bkg-1.jpg',
			'breadcrumbs'	=> Array(
				['name'=>'Pagina non trovata','current'=>'current'],
			)
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/404');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->view('frontend/_v/commons/lista-concessionari');
		$this->frontend->footer();

	}

	
}

