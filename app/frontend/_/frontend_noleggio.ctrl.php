<?php



class frontend_noleggioCtrl extends standardController {



	function __construct() {

		$this->frontend = LoadClass('frontend', 1);

	}



	function GetIndex() {



		//var_dump($this->frontend->marche);



		$config = [

			'title' => 'Gruppo L&lsquo;Automobile',

			'description' 	=> 'Gruppo L&lsquo;Automobile',

			'titleHeader' 	=> 'NOLEGGIO A LUNGO TERMINE',

			'subTitle'		=> '',

			'voceMenu' 		=> '',

			'header-bkg'	=> 'header-bkg-1.jpg',

			'breadcrumbs'	=> Array(

				['name'=>'NOLEGGIO A LUNGO TERMINE','current'=>'current'],

			)

		];



		

		$this->frontend->_config($config);

		$this->frontend->header();

		$this->view('frontend/_v/noleggio');

		$this->view('frontend/_v/commons/vieni-a-trovarci');

		$this->view('frontend/_v/commons/lista-preferiti');

		$this->view('frontend/_v/commons/lista-concessionari');

		

		$this->frontend->footer();



	}

	function GetLungotermine() {



		//var_dump($this->frontend->marche);



		$config = [

			'title' => 'Noleggio auto a lungo Termine su Gruppolautomobile.it',

			'description' 	=> 'Con la consulenza specializzata della nostra azienda potrai scegliere il modello più idoneo alle tue necessità. Per info contattaci al Numero Verde 800 709 990',

			'titleHeader' 	=> 'NOLEGGIO A LUNGO TERMINE',

			'subTitle'		=> '',

			'voceMenu' 		=> '',

			'header-bkg'	=> 'header-bkg-1.jpg',

			'breadcrumbs'	=> Array(

				['name'=>'NOLEGGIO A LUNGO TERMINE','current'=>'current'],

			)

		];



		

		$this->frontend->_config($config);

		$this->frontend->header();

		$this->view('frontend/_v/noleggio');

		$this->view('frontend/_v/commons/vieni-a-trovarci');

		$this->view('frontend/_v/commons/lista-preferiti');

		$this->view('frontend/_v/commons/lista-concessionari');

		

		$this->frontend->footer();



	}


function GetBrevetermine() {



		$config = [

			'title' => 'Noleggio auto a breve Termine su Gruppolautomobile.it',

			'description' 	=> "Con la consulenza specializzata sul noleggio a breve termine potrai scegliere l'auto più adatta alle tue esigenze. Contattaci al Numero Verde 800 709 990",

			'titleHeader' 	=> 'NOLEGGIO A BREVE TERMINE',

			'subTitle'		=> '',

			'voceMenu' 		=> '',

			'header-bkg'	=> 'header-bkg-1.jpg',

			'breadcrumbs'	=> Array(

				['name'=>'NOLEGGIO A BREVE TERMINE','current'=>'current'],

			)

		];



		

		$this->frontend->_config($config);

		$this->frontend->header();

		$this->view('frontend/_v/carrozzeria');

		$this->view('frontend/_v/commons/vieni-a-trovarci');

		$this->view('frontend/_v/commons/lista-preferiti');

			$this->view('frontend/_v/commons/lista-concessionari');

		$this->frontend->footer();



	}


	

}



