<?php

class  frontend_richiediappuntamentoCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> 'PRENOTA UN APPUNTAMENTO',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> '',
			'little-header' => true,
			'header-bkg'	=> 'header-bkg-1.jpg',
			'breadcrumbs'	=> Array(
				['name'=>'PRENOTA APPUNTAMENTO','current'=>'current'],
			)

		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/prenota-appuntamento');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->view('frontend/_v/commons/lista-concessionari');
		
		$this->frontend->footer();

	}

	
}

