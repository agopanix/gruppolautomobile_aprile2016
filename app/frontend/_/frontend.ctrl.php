<?php

class frontendCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		$config = [
			'title' => 'Gruppolautomobile.it il portale delle auto usate garantite',
			'description' 	=> 'Il più grande parco auto del centro Italia con decennale esperienza nel settore della vendita auto usate. Via Enrico Fermi, 27, Frosinone. Numero Verde 800 709 990',
			'titleHeader' 	=> 'LA NOSTRA PASSIONE<br>LE VOSTRE AUTOMOBILI',
			'titleHeader2' 	=> 'LA LIBERTA’ DI SCEGLIERE<br class="show-for-small-only"> NEL PIU’ GRANDE PARCO USATO<br class="show-for-small-only"> DEL CENTRO ITALIA',
			'titleHeader3' 	=> 'GRUPPO L’AUTOMOBILE<br>LA GARANZIA DI 9 CONCESSIONARIE UFFICIALI<br>2 CENTRI RENTING',
			'subTitle'		=> '',
			'voceMenu' 		=> 'home',
			'header-bkg'	=> 'header-bkg-1.jpg',
			'breadcrumbs'	=> 	Array(['name'=>'Home','current'=>'current']),
			'viewscroller'	=>	true
		];

		$this->inofferta = LoadApp('usato',1)->apiGetofferte();
		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/home');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->view('frontend/_v/commons/lista-concessionari');
		$this->view('frontend/_v/commons/promozione-modal');
		$this->frontend->footer();

		//$this->frontend->vieniatrovarci();
		//$this->frontend->listapreferiti();

	}


	function postContatto(){
		
		if(strlen($_POST['cod'])>0){
			$body = '
			Nome: 		'.$_POST['name'].'<br>
		    Email: 		'.$_POST['email'].'<br>
		    Vettura di interesse: cod: '.$_POST['cod'].'<br> 
		    Messaggio: 	'.$_POST['message'];
		}else{
			$body = '
			Nome: 		'.$_POST['name'].'<br>
		    Email: 		'.$_POST['email'].'<br>
		    Messaggio: 	'.$_POST['message'];
		}

		CallFunction('mail', 'save', 'mandarinoadv@gmail.com', 'Contatto dal sito web', $body);
		CallFunction('mail', 'save', 'a.bellamio@mandarinoadv.com', 'Contatto dal sito web', $body);
		CallFunction('mail', 'save', 'm.morelli@mandarinoadv.com', 'Contatto dal sito web', $body);
		
		
		if( CallFunction('mail', 'save', 'info@gruppolautomobile.it', 'Contatto dal sito web', $body) )
			header( 'location: /send:1' );
		
	}

	function postContattonew(){
		
		
			$body = '
			Nome: 		'.$_POST['name'].'<br>
			Cognome: 		'.$_POST['surname'].'<br>
		    Email: 		'.$_POST['email'].'<br>
		    Telefono:   '.$_POST['telefono'].'<br> 
		    Messaggio: 	'.$_POST['message'];
		
		
		
		CallFunction('mail', 'save', 'mandarinoadv@gmail.com', 'Contatto dal sito web', $body);
		CallFunction('mail', 'save', 'a.bellamio@mandarinoadv.com', 'Contatto dal sito web', $body);
		CallFunction('mail', 'save', 'm.morelli@mandarinoadv.com', 'Contatto dal sito web', $body);
		
		
		if( CallFunction('mail', 'save', 'info@gruppolautomobile.it', 'Contatto dal sito web', $body) )
			header( 'location: /send:1' );
		
	}

	

}

