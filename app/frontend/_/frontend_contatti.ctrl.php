<?php

class frontend_contattiCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => "Gruppo L'Automobile Contatti concessionaria auto",
			'description' 	=> 'GruppoLAutomobile.it. scopri i contatti della concessionaria: Via Enrico Fermi, 27 03100 - Frosinone Numero Verde 800 709 990',
			'titleHeader' 	=> 'CONTATTI',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> 'contatti',
			'little-header' => true,
			'header-bkg'	=> 'header-bkg-1.jpg',
			'breadcrumbs'	=> Array(
				['name'=>'Contatti','current'=>'current'],
			)
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/contatti');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->view('frontend/_v/commons/lista-concessionari');
		$this->frontend->footer();

	}

	
}

