<?php

class frontend_offerteCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile - offerte',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> 'LISTA OFFERTE',
			'subTitle'		=> 'Approfitta ora delle nostre promozioni',
			'voceMenu' 		=> 'preferiti',
			'little-header' => true,
			'header-bkg'	=> 'header-bkg-3.jpg',
			'breadcrumbs'	=> Array(
				['name'=>'Lista dei offerte','current'=>'current'],
			)
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/offerte');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-offerte');
		$this->view('frontend/_v/commons/lista-concessionari');
		
		$this->frontend->footer();

	}

	
}

