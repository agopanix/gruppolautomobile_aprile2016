<?php

class  frontend_appuntamentoCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		$config = [
			'title' => 'Gruppo L&lsquo;Automobile',
			'description' 	=> 'Gruppo L&lsquo;Automobile',
			'titleHeader' 	=> 'PRENOTA UN APPUNTAMENTO',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> '',
			'header-bkg'	=> 'header-bkg-1.jpg',
		];

		
		$this->frontend->_config($config);
		$this->frontend->header();
		$this->view('frontend/_v/prenota-appuntamento');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->frontend->footer();

	}

	
}

