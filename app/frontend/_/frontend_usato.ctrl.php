<?php

class frontend_usatoCtrl extends standardController {

	function __construct() {
		$this->frontend = LoadClass('frontend', 1);
	}

	function GetIndex() {

		//var_dump($this->frontend->marche);

		
		foreach ($this->frontend->categorie as $categoria) {
			if($categoria["id"]==$_REQUEST["cat"]){
				$categoria_selezionata = $categoria["name"];
				$testo_categoria = $categoria["text"];
			}
		}


		$config = [
			'title' => 'Vendita auto usate con listino prezzi aggiornato - Gruppolautomobile.it',
			'description' 	=> "Usato garantito su tutte le migliori marche, con ampia scelta e offerte di vendita a prezzi imbattibili. Via Enrico Fermi, 27 - Frosinone Numero Verde 800 709 990",
			'titleHeader' 	=> 'PARCO USATO',
			'subTitle'		=> 'L&lsquo;incanto sarà godersi la strada',
			'voceMenu' 		=> 'usato',
			'header-bkg'	=> 'header-bkg-1.jpg',
			'little-header' => true,
			'header-opaque' => true,
			'isUsato'  	    => true,
			'breadcrumbs'	=> Array(
				['name'=>'Home','current'=>'', 'url'=>'/'],
				['name'=> 'USATO','current'=>'current']
			)
		];

		$catsel = ['catsel'=>$_REQUEST["cat"]];

		$this->listusato = LoadApp('usato',1)->apiGets();
		$this->listamarchi = LoadApp('usato',1)->apiMarchiusato();
		$this->listaalim = LoadApp('usato',1)->apiAlimentazioneusato();
		$this->listacolori = LoadApp('usato',1)->apiColoreusato();
		
		$this->frontend->_config($config);
		$this->frontend->_config($catsel);
		$this->frontend->header();
		$this->view('frontend/_v/usato');
		$this->view('frontend/_v/commons/vieni-a-trovarci');
		$this->view('frontend/_v/commons/lista-preferiti');
		$this->view('frontend/_v/commons/lista-concessionari');
		$this->frontend->footer();

	}

	function postCercausato() {
		//echo 'dentro frontend/usato/cercausato con marchio = <br>';
		//print_r($_POST["marchio"]);
		//die();
		//$addurl = "";
		

		if($_POST["marchio"] != "TUTTI"){
			$_SESSION["marchiosel"] = $_POST["marchio"];
		}else{
			$_SESSION["marchiosel"] = "";
		}

		if($_POST["alimentazione"] != "TUTTE"){
			$_SESSION["alimsel"] = $_POST["alimentazione"];
		}else{
			$_SESSION["alimsel"] = "";
		}

		if(strlen($_POST["modello"])>0){
			$_SESSION["modellosel"] = $_POST["modello"];
		}else{
			$_SESSION["modellosel"] = "";
		}

		if(strlen($_POST["colore"])>0){
			$_SESSION["coloresel"] = $_POST["colore"];
		}else{
			$_SESSION["coloresel"] = "";
		}

		if(strlen($_POST["dakmperco"])>0){
			$_SESSION["dakmpercosel"] = $_POST["dakmperco"];
		}else{
			$_SESSION["dakmpercosel"] = "";
		}

		if(strlen($_POST["akmperco"])>0){
			$_SESSION["akmpercosel"] = $_POST["akmperco"];
		}else{
			$_SESSION["akmpercosel"] = "";
		}

		if(strlen($_POST["daimmatricolaz"])>0){
			$_SESSION["daimmatricsel"] = $_POST["daimmatricolaz"];
		}else{
			$_SESSION["daimmatricsel"] = "";
		}

		if(strlen($_POST["aimmatricolaz"])>0){
			$_SESSION["aimmatricsel"] = $_POST["aimmatricolaz"];
		}else{
			$_SESSION["aimmatricsel"] = "";
		}

		if(strlen($_POST["daprezzo"])>0){
			$_SESSION["daprezzosel"] = $_POST["daprezzo"];
		}else{
			$_SESSION["daprezzosel"] = "";
		}

		if(strlen($_POST["aprezzo"])>0){
			$_SESSION["aprezzosel"] = $_POST["aprezzo"];
		}else{
			$_SESSION["aprezzosel"] = "";
		}

		header('Location: /frontend/usato');
	}

	function postCercatop() {
		//echo 'dentro frontend/usato/cercausato con marchio = <br>';
		//print_r($_POST["marchio"]);
		//die();
		//$addurl = "";
		

		if($_POST["marchio"] != "TUTTI"){
			$_SESSION["marchiosel"] = $_POST["marchio"];
		}else{
			$_SESSION["marchiosel"] = "";
		}

		

		if(strlen($_POST["modello"])>0){
			$_SESSION["modellosel"] = $_POST["modello"];
		}else{
			$_SESSION["modellosel"] = "";
		}

		

		

		if(strlen($_POST["akmperco"])>0){
			$_SESSION["akmpercosel"] = $_POST["akmperco"];
		}else{
			$_SESSION["akmpercosel"] = "";
		}

		if(strlen($_POST["daimmatricolaz"])>0){
			$_SESSION["daimmatricsel"] = $_POST["daimmatricolaz"];
		}else{
			$_SESSION["daimmatricsel"] = "";
		}

		

		

		if(strlen($_POST["aprezzo"])>0){
			$_SESSION["aprezzosel"] = $_POST["aprezzo"];
		}else{
			$_SESSION["aprezzosel"] = "";
		}

		header('Location: /frontend/usato');
	}

	function CtrlPerpage(){

		if(strlen($_REQUEST["num"])>0){
			$num = $_REQUEST["num"];
		}else{
			$num = 25;
		}

		// echo 'dentro perpage con num: '.$num;
		// die();
		
		$_SESSION['perpage'] = $num;
		header('Location: /frontend/usato');
	}

	function ApiFilter() {
	#echo 'dentro api filter';
	#print_r($_REQUEST);
	#die();
		$_SESSION['filtri'] = [];
	  foreach($_REQUEST as $key => $value) {
	  	if(strstr($key, 'f_'))
	    	$_SESSION['filtri'][str_replace('f_', '', $key)] = $value;
	  }
	  return $_SESSION;
	  // print_r($_REQUEST);
	  // die();
	  
	  return $true;
	}

	function ApiNeopatentati(){
		$_SESSION['filtri'] = [];
		$_SESSION['filtri']['neopatentati'] = 'on';
		
			$_SESSION["marchiosel"] = "";
			$_SESSION["alimsel"] = "";
			$_SESSION["modellosel"] = "";
			$_SESSION["coloresel"] = "";
			$_SESSION["dakmpercosel"] = "";
			$_SESSION["akmpercosel"] = "";
			$_SESSION["daimmatricsel"] = "";
			$_SESSION["aimmatricsel"] = "";
			$_SESSION["daprezzosel"] = "";
			$_SESSION["aprezzosel"] = "";

		header('Location: /frontend/usato');
	}

	function ApiIvaesposta(){
		$_SESSION['filtri'] = [];
		$_SESSION['filtri']['iva_esposta'] = 'on';

			$_SESSION["marchiosel"] = "";
			$_SESSION["alimsel"] = "";
			$_SESSION["modellosel"] = "";
			$_SESSION["coloresel"] = "";
			$_SESSION["dakmpercosel"] = "";
			$_SESSION["akmpercosel"] = "";
			$_SESSION["daimmatricsel"] = "";
			$_SESSION["aimmatricsel"] = "";
			$_SESSION["daprezzosel"] = "";
			$_SESSION["aprezzosel"] = "";

		header('Location: /frontend/usato');
	}

	function ApiKmzero(){
		$_SESSION['filtri'] = [];
		$_SESSION['filtri']['kmzero'] = 'on';
			
			$_SESSION["marchiosel"] = "";
			$_SESSION["alimsel"] = "";
			$_SESSION["modellosel"] = "";
			$_SESSION["coloresel"] = "";
			$_SESSION["dakmpercosel"] = "";
			$_SESSION["akmpercosel"] = "";
			$_SESSION["daimmatricsel"] = "";
			$_SESSION["aimmatricsel"] = "";
			$_SESSION["daprezzosel"] = "";
			$_SESSION["aprezzosel"] = "";

		header('Location: /frontend/usato');
	}

	function ApiCambioautomatico(){
		$_SESSION['filtri'] = [];
		$_SESSION['filtri']['cambioautomatico'] = 'on';

			$_SESSION["marchiosel"] = "";
			$_SESSION["alimsel"] = "";
			$_SESSION["modellosel"] = "";
			$_SESSION["coloresel"] = "";
			$_SESSION["dakmpercosel"] = "";
			$_SESSION["akmpercosel"] = "";
			$_SESSION["daimmatricsel"] = "";
			$_SESSION["aimmatricsel"] = "";
			$_SESSION["daprezzosel"] = "";
			$_SESSION["aprezzosel"] = "";

		header('Location: /frontend/usato');
	}

	function ApiSportive() {

		$_SESSION['filtri'] = [];
		$_SESSION['filtri']['sportive'] = 'on';

			$_SESSION["marchiosel"] = "";
			$_SESSION["alimsel"] = "";
			$_SESSION["modellosel"] = "";
			$_SESSION["coloresel"] = "";
			$_SESSION["dakmpercosel"] = "";
			$_SESSION["akmpercosel"] = "";
			$_SESSION["daimmatricsel"] = "";
			$_SESSION["aimmatricsel"] = "";
			$_SESSION["daprezzosel"] = "";
			$_SESSION["aprezzosel"] = "";

		header('Location: /frontend/usato');

	}

	function ApiSuv() {

		$_SESSION['filtri'] = [];
		$_SESSION['filtri']['suv'] = 'on';

			$_SESSION["marchiosel"] = "";
			$_SESSION["alimsel"] = "";
			$_SESSION["modellosel"] = "";
			$_SESSION["coloresel"] = "";
			$_SESSION["dakmpercosel"] = "";
			$_SESSION["akmpercosel"] = "";
			$_SESSION["daimmatricsel"] = "";
			$_SESSION["aimmatricsel"] = "";
			$_SESSION["daprezzosel"] = "";
			$_SESSION["aprezzosel"] = "";

		header('Location: /frontend/usato');

	}

	function CtrlStampascheda() {
		
		$riga = array();
		// echo 'dentro stampascheda con ';
		// print_r($_SESSION["recordtutti"]);
		// die();
		
		foreach($_SESSION["recordtutti"] as $f){
			//echo '$f :'.$f;
			array_push($riga, $f);
		}
		
		
		
		$this->indirizzo = $riga;
		
		
		$this->view('frontend/_v/stampaschedausato');
		
	}

	
}

?>