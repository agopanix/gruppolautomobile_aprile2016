<?php

### GYURAL ###

/*

----------
chisiamo 
----------

Filename: /app/chisiamo/_/chisiamo.ctrl.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 12/11/2015
	
*/

class chisiamoCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}
	
	// api di richiamo di tutti gli oggetti dell'app
	function apiGets($args = null){
		
		return LoadClass('chisiamo', 1)->filter_array($args);
		
	}
	
	// api di richiamo del singolo oggetto dell'app
	function apiGet($id){
		
		if($_REQUEST['id']){
			$id = $_REQUEST['id'];
		}
		
		if($id){
			return LoadClass('chisiamo', 1)->get($id);
		}else{
			return false;
		}
		
	}
	
}

?>