<?php

### GYURAL ###

/*

----------
chisiamo 
----------

Filename: /app/chisiamo/_/chisiamo.lib.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 12/11/2015
	
*/

class chisiamo extends standardObject {
	
	var $gyu_table	= 'chisiamo';
	var $gyu_id 	= 'chisiamo_id';

	function __construct() {
			
		if( $this->chisiamo_id ){
		
			$this->gyu_updateTime = $this->updateTime;
			
			CallFunction('hooks', 'get', 'get.end', $this);
		
		}else{
		
			$this->creationTime = time();
		
		}
		
		$this->updateTime = time();
		
	}
	
	function get_permalink(){
		
		if( $this->chisiamo_id )
			return '/frontend/chisiamo/id:'.$this->chisiamo_id;
		else
			return false;
	}

	function getImg(){
		
		if( $this->has_img() )
			return '/'.uploadPath. $this->img;
		else
			return false;
		
	}

	function has_img(){
		
		if( $this->img )
			return true;
		else 
			return false;
			
	}

}


?>