<?php
	
	if( isset($_GET['id']) ){
		$item = LoadApp('chisiamo', 1)->ApiGet($_GET['id']);
		$title = 'Modifica';
	}else{
		$item = LoadClass('chisiamo', 1);
		$title = 'Aggiungi';
	}
?>

<h1><?=$title?></h1>

<form method="post" enctype="multipart/form-data">
	
	<div class="form-group">
		<label for="title">Titolo</label>
		<input class="form-control input-lg" type="text" required name="title" value="<?=$item->title?>"/>
	</div>
	
	<div class="form-group">
		<label for="content">Contenuto</label>
		<textarea class="form-control summernote" rows="4" name="content" id="summernote"><?=$item->content?></textarea>
	</div>

	<div class="well">
		<div class="form-group">
			<label for="img">Immagine in evidenza</label>
			<input type="file" class="form-control" name="img">
			<?php 
					
				if($item->img){
					echo 'Anteprima: <br><img src="/upl/'.$item->img.'" style="width:25%;">';
				}
			
			?>
		</div>
	</div>
	
	<? if( isset($item->chisiamo_id) ): ?>
	
	<div class="row">
		<div class="col-sm-3">
			
			<button type="submit" class="btn btn-default btn-block">Modifica</button>
			
		</div>
		<div class="col-sm-9 text-right">
			
			<input type="hidden" name="chisiamo_id" value="<?=$item->chisiamo_id?>" />
			<div class="checkbox">
				<label><input type="checkbox" name="delete" value="1" /> Elimina definitivamente l'inserimento</label>
			</div>
			
		</div>
	</div>
	
	<? else: ?>
	
	<div class="row">
		<div class="col-sm-3">
			
			<button type="submit" class="btn btn-default btn-block">Inserisci</button>
	
		</div>
	</div>
	
	<? endif; ?>
		
	<div class="clearfix margin-40"></div>
	
	<? CallFunction('hooks', 'get', 'form.end', $this); ?>
		
</form>