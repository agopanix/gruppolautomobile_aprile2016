<?php

### GYURAL ###

/*

----------
servizi 
----------

Filename: /app/servizi/_/servizi.lib.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 03/11/2015
	
*/

class servizi extends standardObject {
	
	var $gyu_table	= 'servizi';
	var $gyu_id 	= 'servizio_id';

	function __construct() {
			
		if( $this->servizio_id ){
		
			$this->gyu_updateTime = $this->updateTime;
			
			CallFunction('hooks', 'get', 'get.end', $this);
		
		}else{
		
			$this->creationTime = time();
		
		}
		
		$this->updateTime = time();
		
	}
	
	function get_permalink(){
		
		if( $this->servizio_id )
			return '/frontend/servizi/id:'.$this->servizio_id;
		else
			return false;
	}
	function getImg(){
		
		if( $this->has_img() )
			return '/'.uploadPath. $this->img;
		else
			return false;
		
	}

	function getTitle(){
		
			return $this->title;
		
	}


	function getContent(){
		
			return $this->content;
		
	}

	function has_img(){
		
		if( $this->img )
			return true;
		else 
			return false;
			
	}

	function getExcerpt($lenght = 100){
		
		if( $this->excerpt ){
			
				return CallFunction('strings', 'taglia', $this->excerpt, $lenght);
			
		}else{ 
			
				return CallFunction('strings', 'taglia', $this->content, $lenght);
			
		}	
	}

}


?>