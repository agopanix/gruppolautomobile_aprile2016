<?php

### GYURAL ###

/*

----------
servizi 
----------

Filename: /app/servizi/_/servizi.funcs.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 03/11/2015
	
*/

function servizi__sample() {

	// this is a sample funcs!
	return 'Hello!';

}


function servizi__resize($src, $w = null, $h = null, $q = null, $zc = null){

	//echo 'dentro servizi resize';
	//die();
	
	// if( !file_exists('/app/frontend/_assets/timthumb.php') ) return 'http://placehold.it/800x800&text=NoLibFound';
	
	if( $src == '' ) return 'http://placehold.it/'.$w.'x'.$h.'&text=' . siteName;
	
	$img = '/app/frontend/_assets/timthumb.php?src=' . $src;
	
	if( !is_null($w) ) 	$img .= '&w=' . $w;
	if( !is_null($h) ) 	$img .= '&h=' . $h;
	if( !is_null($q) ) 	$img .= '&q=' . $q;
	if( !is_null($zc) ) $img .= '&zc=' . $zc; 
	
	return $img;
	
}

?>