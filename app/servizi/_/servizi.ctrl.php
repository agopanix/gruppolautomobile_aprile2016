<?php

### GYURAL ###

/*

----------
servizi 
----------

Filename: /app/servizi/_/servizi.ctrl.php
 Version: 0.1
  Author: Agostino <web@mandarinoadv.com>
    Date: 03/11/2015
	
*/

class serviziCtrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}


	function CtrlIndex(){
		Header( "HTTP/1.1 301 Moved Permanently" );
		header("Location: /");
	}
	
	// api di richiamo di tutti gli oggetti dell'app
	function apiGets($args = null){
		
		return LoadClass('servizi', 1)->filter_array($args);
		
	}
	
	// api di richiamo del singolo oggetto dell'app
	function apiGet($id){
		
		if($_REQUEST['id']){
			$id = $_REQUEST['id'];
		}
		
		if($id){
			return LoadClass('servizi', 1)->get($id);
		}else{
			return false;
		}
		
	}
	
}

?>