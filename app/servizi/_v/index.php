<div class="page-header">
	<!--
	<ul class="list-inline pull-right">
		<li><a class="btn btn-default" href="/servizi/admin/form/"><i class="fa fa-cloud-upload"></i> Aggiungi</a></li>
	</ul>
	-->
	
	<h1>Servizi</h1>
</div>

<? 
					
$count 			= LoadClass('servizi', 1)->filter('COUNT');

$perPage 		= 40;
$maxPage 		= ceil($count / $perPage);
$currentPage 	= ($_GET['p']) ? $_GET['p'] : 1;
$startItem 		= $perPage*($currentPage-1);
	
$items 			= LoadClass('servizi', 1)->filter( array('LIMIT', $startItem, $perPage) );

if( $items ) : 

?>

	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Titolo</th>
				<th>Inserimento</th>
				<th>Visualizza</th>
			</tr>
		</thead>
		<tbody>	
	
			<?php foreach( $items as $item ) : ?>
			
			<tr>
				<td>
					<?=$item->servizio_id?>
				</td>
				<td>
					<a href="/servizi/admin/form/id:<?=$item->servizio_id?>"><?=$item->title?></a>
				</td>
				
				<td>
					<?=date('d/m/Y H:i', $item->creationTime)?>
				</td>
				<td>
					<a href="/servizi/admin/form/id:<?=$item->servizio_id?>">Visualizza</a>
				</td>
			</tr>
			
			<?php endforeach; ?>
			
		</tbody>
	</table>	
	
	<?php if( $count > 0 ) CallFunction('admin', 'pagination', $currentPage, $maxPage, '/servizi/admin'); ?>

<? else: ?>

	<div class="alert alert-info">
		Ancora nessun elemento inserito...
	</div>
	
<? endif; ?>				