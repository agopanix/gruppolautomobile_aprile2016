<?php

/**
 * {{name}}
 *
 *
 * @version {{version}}
 * @author {{author}}
 */

class {{ctrl}}Ctrl extends standardController {
	
	var $index_tollerant = true;
	
	function __construct() {

	}

	function GetIndex() {
		echo '{{ctrl}} created!';
	}
	
	function ApiIndex() {}
	
	function PostIndex() {}
	
	function CtrlIndex() {}

	function CliRun() {
		echo "I'm the running CLI method of {{ctrl}}";
	}

}